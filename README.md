# **Laboratorio virtual de programación C++ en línea bajo el paradigma de educación basada en Web.**

## Objetivo

Desarrollar una plataforma virtual educativa basada en el paradigma WBE donde alumnos y profesores puedan ser partícipes en la elaboración de programas codificados en el lenguaje de programación C++, los cuales puedan compilar, descargar, editar y ser retroalimentados y evaluados por los profesores para enriquecer el aprendizaje de este lenguaje de programación. Esta plataforma se puede implementar de diferentes formas para su enseñanza ya sea presencial, a distancia o semi-presencial según la forma de trabajo preferida.

Sistema con el cual se pretende apoyar en la educación de la carrera de ingeniería en sistemas computacionales o carreras afines.
Un laboratorio virtual es un sistema computacional que busca tener un ambiente parecido al de un laboratorio tradicional, visualizando los fenómenos mediante objetos dinámicos e imágenes.

### Educación Basada en Web
La Educación Basada en Web (Web Based Education, WBE por sus siglas en inglés) es un paradigma que engloba todos los aspectos y procesos de educación que usa la Gran Red Mundial (World Wide Web, WWW por sus siglas en inglés) como un medio de comunicación y tecnología de apoyo.
La WBE está caracterizado por:
* La separación de profesores y estudiantes (la cual se distingue de la educación presencial).
* La influencia de una nueva organización educativa (que se distingue del auto-estudio y tutorías privadas).
* El uso de las tecnologías Web para la presentación y/o distribución de contenido educativo. ● La provisión de una comunicación bidireccional vía Internet de manera que los estudiantes pueden beneficiarse de la comunicación entre ellos, los maestros y personal.

### Librerías
Las librerías usadas para el desarrollo de este proyeecto y que son necesarias para el correcto funcionamiento son:
* Librerías del framework Struts 2.3.15
* JavaMail API
* Librerías de Hibernate 4.3
* Conector MySQL 5.1.23
* Bootstrap 4.0
* JQuery 3.2
* CodeMirror
* iTextPDF 5.5.12
* JQuery UI 1.8

### Diagrama de funcionamiento
![image.png](./image.png)

### Vistas

![image-1.png](./image-1.png)

![image-2.png](./image-2.png)

![image-3.png](./image-3.png)
