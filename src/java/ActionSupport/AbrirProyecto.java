package ActionSupport;

import Plantillas.Archivo;
import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import Utilidades.Compilador;
import Utilidades.Proyectos;
import com.opensymphony.xwork2.ActionSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Session;

public class AbrirProyecto extends ActionSupport implements SessionAware{
    private String idP;//ID del proyecto que se va a abrir
    private String nombreProyecto;//nombre
    private List <Proyectos> proyectos;
    private List <Archivo> archs;
    private Map<String, Object> session = null;
    //private String codigo = "";
    //String extension = "";

    public List<Proyectos> getProyectos() {
        return proyectos;
    }

    public void setProyectos(List<Proyectos> proyectos) {
        this.proyectos = proyectos;
    }
    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public String getIdP() {
        return idP;
    }

    public void setIdP(String idP) {
        this.idP = idP;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }
    public int getProyectosSize(){
        return proyectos.size();
    }
    
    @Override
    public String execute() throws FileNotFoundException, IOException{
        proyectos = new ArrayList<>();
        
        Compilador c =  new Compilador();
        Usuario user = (Usuario)session.get("USER");
        
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        archs = hiberSession.createQuery("from Archivo where idProyecto = "+idP+" AND autor="+user.getId()).list();
        hiberSession.close();
        
        System.out.println("Abriendo el proyecto #"+idP+":"+nombreProyecto+" del usuario "+user.getUsername()+"...");
        String ruta = c.getRutaCompleta() + "\\" + user.getUsername() + "\\" + nombreProyecto;
        
        //String ruta = archs.get(0).getRuta()+ "/"+ user.getUsername() + "/" + nombreProyecto;
        File directorio = new File(ruta);
        String [] archivos = new String [archs.size()];
        
        for(int i=0; i<archs.size(); i++){
            archivos[i] = archs.get(i).getNombreArchivo();
        }
        
        if(directorio.exists()){
            //archivos = directorio.listFiles();
            System.out.println("Los archivos en el proyecto son:");
            for(int i = 0; i< archivos.length; i++){
                System.out.println(i + "-" + archivos[i]);
                BufferedReader br = new BufferedReader(new FileReader(ruta + "\\" + archivos[i]));
                String linea;
                String codigo = "";
                String extension = "";
                while((linea = br.readLine()) != null){
                    codigo = codigo+linea+"\n";
                }
                int a = archivos[i].indexOf(".");
                extension = archivos[i].substring(a, archivos[i].length());
                String nombre = archivos[i].substring(0, a);
                Proyectos p = new Proyectos(i, nombre, codigo, extension);
                proyectos.add(i, p);
                //System.out.println(session.get("cuenta"));
                session.put("cuenta", a);
            }
        }else{
            addActionError("No se puede abrir el proyecto, directorio no existe");
            return INPUT;
        }
        System.out.println("Abriendo el proyecto #"+idP+":"+nombreProyecto+" del usuario "+user.getUsername()+"...");
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    
}
