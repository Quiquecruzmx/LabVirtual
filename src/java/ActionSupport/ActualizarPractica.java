/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Practica;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Iterator;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Enrique
 */
public class ActualizarPractica extends ActionSupport implements SessionAware{
    private Map<String, Object> map = null;
    private String objetivoGen;
    private String tituloprac;
    private String objetivosEsp;
    private String instrucciones;


    public String getObjetivosEsp() {
        return objetivosEsp;
    }

    public void setObjetivosEsp(String objetivosEsp) {
        this.objetivosEsp = objetivosEsp;
    }

    public String getInstrucciones() {
        return instrucciones;
    }

    public void setInstrucciones(String instrucciones) {
        this.instrucciones = instrucciones;
    }

    public String getTituloprac() {
        return tituloprac;
    }

    public void setTituloprac(String tituloprac) {
        this.tituloprac = tituloprac;
    }
    

    public String getObjetivoGen() {
        return objetivoGen;
    }

    public void setObjetivoGen(String objetivoGen) {
        this.objetivoGen = objetivoGen;
    }
    
    
    @Override
    public String execute(){
        System.out.println("Actualizando práctica...");
        System.out.println("Título: " + tituloprac);
        System.out.println("Objetivo general: " + objetivoGen);
        System.out.println("Objetivos específicos: " + objetivosEsp);
        System.out.println("Instrucciones: " + instrucciones);

        Session hbs = HibernateUtil.getSessionFactory().openSession();
        Query select = hbs.createQuery("FROM Practica WHERE titulo='"+tituloprac+"'");
        Iterator it = select.iterate();
        Practica p = (Practica)it.next();
        Transaction t = hbs.beginTransaction();
        Practica p1 = (Practica) hbs.get(Practica.class, p.getIdPractica());
        System.out.println(p1.getTitulo());
        p1.setObjetivo(objetivoGen);
        p1.setObjetivosEsp(objetivosEsp);
        p1.setInstrucciones(instrucciones);
        hbs.saveOrUpdate(p1);
        t.commit();
        
        addActionMessage("Práctica guardada!");
        hbs.close();
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.map = map;
    }
    
}
