/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.MaterialPractica;
import Plantillas.Practica;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.hibernate.Query;
import org.hibernate.Session;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Enrique
 */
public class AlumnoMain extends ActionSupport implements SessionAware{
    private List listaProyectos; //Lista de proyectos de este usuario
    private List <Practica> listaPracticas;//Lista de las practicas que tiene asignado este alumno
    private Map<String,Object> sesionDatos = null; 

    public List getListaProyectos() {
        return listaProyectos;
    }

    public void setListaProyectos(List listaProyectos) {
        this.listaProyectos = listaProyectos;
    }

    public List <Practica> getListaPracticas() {
        return listaPracticas;
    }

    public void setListaPracticas(List<Practica> listaPracticas) {
        this.listaPracticas = listaPracticas;
    }
    
    @Override
    public String execute(){
        listaPracticas = new ArrayList<>();
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        Usuario user = (Usuario)sesionDatos.get("USER");
        
        listaProyectos = hbs.createQuery("from Proyecto where autor="+user.getId()).list();
        
        Query q = hbs.createQuery("SELECT idGrupo FROM Alumno WHERE idAlumno='"+user.getId()+"'");
        Iterator t = q.iterate();
        System.out.println("Recuperando los grupos a los cuales pertenece "+user.getUsername()+"...");

        while(t.hasNext()){
            Grupo grupo = (Grupo)t.next();
            System.out.println("Recuperando las prácticas del grupo: "+grupo.getNombreGrupo());
            Query query = hbs.createQuery("FROM MaterialPractica m WHERE m.grupo='"+grupo.getIdGrupo()+"'");
            Iterator it = query.iterate();
            ArrayList <Integer> practicasID = new ArrayList<>();

            while(it.hasNext()){
                MaterialPractica mp = (MaterialPractica)it.next();
                
                Practica p = mp.getPractica();
                int idP = p.getIdPractica();
                if(!practicasID.contains(idP)){
                    practicasID.add(idP);
                }
                        
            }
            
            
            for(Integer x: practicasID){
                System.out.println("Imprime x: "+x);
                Practica p = (Practica)hbs.load(Practica.class, x);
                System.out.println("ID de la práctica " + p.getIdPractica() + ", nombre: " + p.getTitulo());
                listaPracticas.add(p);
            }

        }
        //System.out.println("Practica "+listaPracticas.get(0).);
        setListaPracticas(listaPracticas);
        sesionDatos.put("PROYECTOS_ALU", listaProyectos);
        sesionDatos.put("PRACTICAS_ALU", listaPracticas);
        //hbs.close();
        return SUCCESS;
        
    }
    
    @Override
    public void setSession(Map<String, Object> sesionDatos) {
       this.sesionDatos = sesionDatos;
    }
}
