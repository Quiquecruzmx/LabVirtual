/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.MaterialDidactico;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Enrique
 */
public class BorrarMaterial extends ActionSupport{
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public String execute(){
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        Transaction t = hbs.beginTransaction();
        MaterialDidactico m = (MaterialDidactico) hbs.get(MaterialDidactico.class, id);
        hbs.delete(m);
        t.commit();
        SubirArchivoAction sa = new SubirArchivoAction();
        String ruta = sa.getRuta();
        File f = new File(ruta+m.getNombreMaterial());
        if(f.delete()){
            System.out.println("Material borrado");
        }else{
            addActionError("El material no puede ser borrado");
        }
        hbs.close();
        return SUCCESS;
    }
}
