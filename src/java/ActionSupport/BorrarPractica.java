package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Practica;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class BorrarPractica extends ActionSupport implements SessionAware{
    private Map<String, Object> sessionAtributes = null;
    private int idPractica;

    public int getIdPractica() {
        return idPractica;
    }

    public void setIdPractica(int idPractica) {
        this.idPractica = idPractica;
    }
    
    
    @Override
    public String execute()throws Exception{
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        Usuario user = (Usuario) sessionAtributes.get("USER");
        
        Transaction t = hbs.beginTransaction();
        Practica practica = (Practica) hbs.load(Practica.class, idPractica);
              
        System.out.println("Borrando la práctica "+practica.getTitulo()+"...");
        hbs.delete(practica); 
        
        t.commit();
        
        addActionMessage("Práctica borrada");
        hbs.close();
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAtributes = sessionAttributes;
    }
}
