/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Enrique
 */
public class CompartirPractica extends ActionSupport implements SessionAware{
    private Map <String, Object> sessionMap = null;
    private int idPractica;
    private String titulo;
    
    public CompartirPractica(){}

    public int getIdPractica() {
        return idPractica;
    }

    public void setIdPractica(int idPractica) {
        this.idPractica = idPractica;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        System.out.println("Hola desde setTitulo");
        this.titulo = titulo;
    }

    
    @Override
    public String execute() throws Exception{
        
        System.out.println("El id de la práctica es: " + idPractica);
        System.out.println("EEEEEEl titulo de la práctica es: " + titulo);
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }
    
}
