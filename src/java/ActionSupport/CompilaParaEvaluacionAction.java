package ActionSupport;

import com.opensymphony.xwork2.ActionSupport;
import Utilidades.Compilador;
import Utilidades.Proyectos;
import java.util.List;
import Plantillas.Usuario;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
 


public class CompilaParaEvaluacionAction extends ActionSupport implements SessionAware{
    
    private String nombreProyecto;
    private List <Proyectos> proyectos;
    private String accion;
    private int idP;
    private Map<String,Object> sesionDatos = null; 
    private String nombreAutor; 

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public List<Proyectos> getProyectos() {
        return proyectos;
    }

    public void setProyectos(List<Proyectos> proyectos) {
        this.proyectos = proyectos;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }
    
    
    
    @Override
    public String execute(){
        System.out.println("Accion es =  "+accion);
        
        Usuario u = (Usuario) sesionDatos.get("USER");
        System.out.println("Usuario recuperado "+ u.getUsername());
        int i;
        String resultArchivos = "";//Esta cadena nos va a arrojar el resultado durante el proceso de creacion de archivos
        String resultCompilacion= "";//Esta cadena nos va a arrojar el resultado durante el proceso de compilación
        
        
        String [] nombres = new String[proyectos.size()];
        String [] exts = new String[proyectos.size()];
        String [] codigos = new String[proyectos.size()];
        
        for(i = 0; i < proyectos.size(); i++){
            if(proyectos.get(i).getNombre().startsWith("@")){
                 nombres[i] = proyectos.get(i).getNombre().substring(1);
            }
            else{
                nombres[i] = proyectos.get(i).getNombre();
                
            }
            exts[i] = proyectos.get(i).getExtension();
            codigos[i] = proyectos.get(i).getCodigo();

        }
           
        Compilador c = new Compilador();
        
        resultArchivos = c.generarParaEvaluar(u.getUsername(), nombreAutor, nombres, exts, codigos, nombreProyecto);
        
        if(resultArchivos.compareTo("") != 0){
            addActionError(resultArchivos);
            return ERROR;
        }   
        if(accion.compareTo("compilar") == 0){
            System.out.println("Autor del proyecto a evaluar:"+nombreAutor );
            if((resultCompilacion = c.compilarParaEvaluar(u.getUsername(), nombreAutor,  nombres, exts, nombreProyecto))!=null){
                addActionError(resultCompilacion);}
            else{addActionMessage("El programa compiló exitosamente");}
        }
            
        return SUCCESS;
            

    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
    
}
