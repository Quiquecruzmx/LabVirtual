package ActionSupport;

import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CrearGrupoAction extends ActionSupport {
    
    private String nombreGrupo;
    private int userTitular; 
    
    private String mensaje; //Mensaje con los errores desplegados
    
    private int claveGrupo; //idTabla

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public int getUserTitular() {
        return userTitular;
    }

    public void setUserTitular(int userTitular) {
        this.userTitular = userTitular;
    }

    public int getClaveGrupo() {
        return claveGrupo;
    }

    public void setClaveGrupo(int claveGrupo) {
        this.claveGrupo = claveGrupo;
    }
    
    public String getMensaje(){
        return mensaje;
    }
    public void setMensaje(String mensaje){
        this.mensaje = mensaje;
    }
    
    
    @Override
    public String execute(){
        System.out.println("Datos: "+nombreGrupo+" , "+ userTitular);
        Session hiberSession;
        hiberSession=HibernateUtil.getSessionFactory().openSession(); 
        Transaction t=hiberSession.beginTransaction();
        Grupo gr = new Grupo(); //Grupo
        
        Usuario u = (Usuario) hiberSession.load(Usuario.class, userTitular);
        
        if(nombreGrupo.trim().compareTo("") == 0)//Valida si el nombre no es vacio
        {
            addActionMessage("No se puede quedar el grupo sin un nombre");
            return INPUT;
        }
        
     //Insert
        try{
            gr.setNombreGrupo(nombreGrupo);
//            gr.setPass(pass.trim());
            gr.setTitular(u);
            
            hiberSession.save(gr);
            t.commit();
            
            hiberSession.close();
            return SUCCESS;
        }
        catch(HibernateException e){
            System.err.println(e.getMessage());
            addActionError("No se pudo crear el grupo");
            return INPUT;
        }
    }
    
}
