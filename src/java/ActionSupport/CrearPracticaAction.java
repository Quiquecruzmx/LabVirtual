package ActionSupport;

import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.MaterialDidactico;
import Plantillas.MaterialPractica;
import Plantillas.Practica;
import Plantillas.Usuario;
import Utilidades.CreadorPDF;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CrearPracticaAction extends ActionSupport implements SessionAware{
    private Map<String, Object> sessionAttributes = null;
    private List<String> materiales;
    private List<String> listaMateriales;
    private List<String> grupos;
    private List<String> listaGrupos;
    private final String ruta = "C:\\MaterialSubido\\";//Aquí se guarda la imagen del resultado de la práctica
    //private final String ruta = "C:\\Users\\admin\\Desktop\\MaterialSubido\\";//Aquí se guarda la imagen del resultado de la práctica
    private String titulo;
    private String objetivo;
    private String objetivosEsp;
    private String instrucciones;
    private File resultado;
    private String resultadoContentType;
    private String resultadoFileName;
    private String nombreArchivo;
    private String rubrica;
    
    
    public CrearPracticaAction(){
        materiales = new ArrayList<String>();
        grupos = new ArrayList<String>();
        sessionAttributes = ActionContext.getContext().getSession();
        Usuario user = (Usuario)sessionAttributes.get("USER");
        Session hbs = HibernateUtil.getSessionFactory().openSession();            
        Query query = hbs.createQuery("FROM Grupo WHERE titular='"+user.getId()+"'");
        Iterator it = query.list().iterator();
        while(it.hasNext()){
            Grupo g = (Grupo) it.next();
            grupos.add(g.getNombreGrupo());
        }
        query = hbs.createQuery("FROM MaterialDidactico WHERE autor='"+user.getId()+"'");
        it = query.list().iterator();
        while(it.hasNext()){
            MaterialDidactico md = (MaterialDidactico)it.next();
            materiales.add(md.getNombreMaterial());
        }
        hbs.close();
    }

    public List<String> getMateriales() {
        return materiales;
    }

    public void setMateriales(List<String> materiales) {
        this.materiales = materiales;
    }

    public List<String> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<String> grupos) {
        this.grupos = grupos;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getInstrucciones() {
        return instrucciones;
    }

    public void setInstrucciones(String instrucciones) {
        this.instrucciones = instrucciones;
    }

    public File getResultado() {
        return resultado;
    }

    public void setResultado(File resultado) {
        this.resultado = resultado;
    }

    public String getResultadoContentType() {
        return resultadoContentType;
    }

    public void setResultadoContentType(String resultadoContentType) {
        this.resultadoContentType = resultadoContentType;
    }

    public String getResultadoFileName() {
        return resultadoFileName;
    }

    public void setResultadoFileName(String resultadoFileName) {
        this.resultadoFileName = resultadoFileName;
    }

    public List<String> getListaMateriales() {
        return listaMateriales;
    }

    public void setListaMateriales(List<String> listaMateriales) {
        this.listaMateriales = listaMateriales;
    }

    public List<String> getListaGrupos() {
        return listaGrupos;
    }

    public void setListaGrupos(List<String> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }

    public String getObjetivosEsp() {
        return objetivosEsp;
    }

    public void setObjetivosEsp(String objetivosEsp) {
        this.objetivosEsp = objetivosEsp;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getRubrica() {
        return rubrica;
    }

    public void setRubrica(String rubrica) {
        this.rubrica = rubrica;
    }
    
    

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }

    public Map<String, Object> getSessionAttributes() {
        return sessionAttributes;
    }
    
    public String display(){
        return NONE;
    }
    @Override
    public String execute() throws Exception{
        System.out.println("Rubrica: "+rubrica);
        Session hbs =  HibernateUtil.getSessionFactory().openSession();
        Transaction t = hbs.beginTransaction();
        Usuario user = (Usuario) sessionAttributes.get("USER");
        
        PrintWriter writer;
        String rutaRub = "C:\\Rubricas\\"+titulo+"-"+user.getUsername()+".txt";
        try {
            writer = new PrintWriter(rutaRub, "UTF-8");
            writer.print(rubrica);
            writer.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InsertProyectoCalifAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(InsertProyectoCalifAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        if(practicaExiste(hbs, titulo, user)){
            addActionError("Esta práctica ya existe, intente nuevamente...");
            sessionAttributes.put("tit", titulo);
            sessionAttributes.put("obj", objetivo);
            sessionAttributes.put("objEsp", objetivosEsp);
            sessionAttributes.put("ins", instrucciones);
            hbs.close();
            return INPUT;
        }
        sessionAttributes.remove("tit");
        sessionAttributes.remove("obj");
        sessionAttributes.remove("objEsp");
        sessionAttributes.remove("ins");
        Practica practica = new Practica(titulo);
        if(resultado != null){
            File archivo = new File(ruta+resultadoFileName);
            resultado.renameTo(archivo);
            practica.setResultado(ruta+resultadoFileName);
        }
        if(objetivo.compareTo("") != 0){
            practica.setObjetivo(objetivo);
        }
        if(objetivosEsp.compareTo("") != 0){
            practica.setObjetivosEsp(objetivosEsp);
        }
        if(instrucciones.compareTo("") != 0){
            practica.setInstrucciones(instrucciones);
        }
        practica.setPdf(ruta+titulo+".pdf");
        practica.setRubricaRuta(rutaRub);
        practica.setIdUsuario(user);
        hbs.save(practica);
        
        for(String grupo : listaGrupos){
            Query query = hbs.createQuery("FROM Grupo WHERE nombreGrupo='"+grupo+"'");
            Iterator it = query.iterate();
            Grupo g = null;
            while(it.hasNext()){
                g  = (Grupo) it.next();
            }
            if(!listaMateriales.isEmpty()){
                for(String material:listaMateriales){
                    Query query1 = hbs.createQuery("FROM MaterialDidactico WHERE nombreMaterial='"+material+"'");
                    Iterator it1 = query1.iterate();
                    while(it1.hasNext()){
                        MaterialDidactico md = (MaterialDidactico)it1.next();
                        MaterialPractica mp = new MaterialPractica();
                        mp.setMaterialdidactico(md);
                        mp.setPractica(practica);
                        mp.setGrupo(g);
                        hbs.save(mp);
                    }
                }
            }else{
                MaterialPractica mp = new MaterialPractica();
                mp.setPractica(practica);
                mp.setGrupo(g);
                hbs.save(mp);
            }
        }
        t.commit();
        
        InputStream inStream = null;
	OutputStream outStream = null;
        
        
        File archivo = new File(ruta+titulo+".pdf");
        CreadorPDF creador = new CreadorPDF();
        creador.crearDocumento(archivo, titulo, user.getUsername(), titulo, objetivo, objetivosEsp, instrucciones, ruta+resultadoFileName);
        addActionMessage("Practica creada exitosamente");
        System.out.println("Práctica guardada en la base de datos");
        hbs.close();
        
        
        System.out.println("Nombre: "+archivo.getName());
        if(resultado != null){
            File afile = new File(ruta+resultadoFileName);
            File bfile = new File("C:\\Rubricas\\"+afile.getName());
            nombreArchivo=afile.getName();
            inStream = new FileInputStream(afile);
            outStream = new FileOutputStream(bfile);

            byte[] buffer = new byte[1024];

            int length;
            System.out.println("Copiando");
            //copy the file content in bytes 
            while ((length = inStream.read(buffer)) > 0){

                outStream.write(buffer, 0, length);

            }

            inStream.close();
        outStream.close();
        }        
        return SUCCESS;
        
    }

    private boolean practicaExiste(Session hbs, String titulo, Usuario user) {
        Query query = hbs.createQuery("FROM Practica WHERE idUsuario='"+user.getId()+"'");
        if(query.list().isEmpty()){return false;}//Si está vacia, no existe una practica igual
        Iterator it = query.iterate();
        while(it.hasNext()){
            Practica p = (Practica) it.next();
            if(p.getTitulo().compareTo(titulo) == 0){//Si es igual al titulo
                return true;
            }
        }
       return false;
    }
    
}
