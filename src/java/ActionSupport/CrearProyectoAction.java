/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import Utilidades.Compilador;
import Utilidades.Proyectos;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Hibernate;
import org.hibernate.Session;

/**
 *
 * @author Enrique
 */
public class CrearProyectoAction extends ActionSupport implements SessionAware{
    public final String SUCCESS = "success";
    private List <Proyectos> proyectos;
    private String nombreProyecto;
    private Map<String, Object> sesionDatos = null;

     public List<Proyectos> getProyectos() {
        return proyectos;
    }

    public void setProyectos(List<Proyectos> proyectos) {
        this.proyectos = proyectos;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }
    
    
    
    @Override
    public String execute()throws Exception{
        Compilador c = new Compilador();
        Usuario u = (Usuario) sesionDatos.get("USER");
        System.err.println("Usuario logueado:"+u.getUsername());
        if(nombreProyecto.trim().compareTo("")==0 || proyectos.get(0).getNombre().trim().compareTo("") == 0){
            addActionError("No puede dejar vacio el nombre proyecto y/o el archivo principal");
            return INPUT;
        }
        
        Session hiberSesion = HibernateUtil.getSessionFactory().openSession();
        if(!c.proyectoExistente(hiberSesion, nombreProyecto, u.getId())){
            return SUCCESS;
        }
        return INPUT;
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
    
}
