/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Practica;
import Plantillas.Usuario;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Session;

public class DatosPracticaParaWeb extends ActionSupport implements SessionAware{
    
    private int nombrePractica;
    private Practica practica;
    private String nombreArchivo;
    
    private String rubrica;
   // HttpServletRequest request;
    //private byte[] imageBytes=null;
    private Map<String, Object> sessionAttributes = null;

    public int getNombrePractica() {
        return nombrePractica;
    }

    public void setNombrePractica(int nombrePractica) {
        this.nombrePractica = nombrePractica;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getRubrica() {
        return rubrica;
    }

    public void setRubrica(String rubrica) {
        this.rubrica = rubrica;
    }   
    
    @Override
    public String execute() throws Exception {
        System.out.println("Abriendo la práctica via web: "+nombrePractica);
        Usuario user = (Usuario)sessionAttributes.get("USER");
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        practica = (Practica) hbs.load(Practica.class, nombrePractica);
        System.out.println("Ruta: "+practica.getResultado());
        System.out.println("Objetivos: "+practica.getObjetivosEsp());
            
        BufferedReader br = null;
        FileReader fr = null;
        String res = "";

        if(practica.getRubricaRuta()==null){
            return SUCCESS;
        }
        
        try {
            br = new BufferedReader (new InputStreamReader (new FileInputStream (practica.getRubricaRuta()), "utf-8"));

            String sCurrentLine;                  

            while ((sCurrentLine = br.readLine()) != null) {
                    //System.out.println(sCurrentLine);
                    res+=sCurrentLine;
            }
            rubrica = res;
            
        } catch (IOException e) {
            System.out.println("Estás en otra excepción");
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                System.out.println("Caíste en una excepción");
                ex.printStackTrace();
            }
        }

        if(practica.getResultado() != null){
    	    File afile = new File(practica.getResultado());
    	    nombreArchivo=afile.getName();
        } 
        
        if(user.getRol().compareTo("profesor") == 0){
            System.out.println("Regresando al profesor");
            return SUCCESS;
        }
        else if(user.getRol().compareTo("alumno")==0)
            return SUCCESS+"alu";
        else
            return INPUT;
    }

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
}
