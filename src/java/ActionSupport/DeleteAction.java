package ActionSupport;

import Plantillas.BorrarUsuario;
import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import Utilidades.Compilador;
import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DeleteAction extends ActionSupport{
    private int toDelete;

    public int getToDelete() {
        return toDelete;
    }

    public void setToDelete(int toDelete) {
        this.toDelete = toDelete;
    }
    
    @Override
    public String execute(){
        
        if(toDelete == 1){
            return "prohibido";
        }
        else{
            Session hbmSesDelete;
            hbmSesDelete=HibernateUtil.getSessionFactory().openSession(); 
            Transaction t=hbmSesDelete.beginTransaction(); 

            BorrarUsuario aBorrar=(BorrarUsuario)hbmSesDelete.load(BorrarUsuario.class,toDelete);

            hbmSesDelete.delete(aBorrar);
            t.commit(); 
            Compilador comp =  new Compilador();
            comp.destruirCarpetaUsuario(aBorrar.getUsername());
            //Compilador.destruirCarpetaUsuario(aBorrar.getUsername());
            
            return SUCCESS;
        }
    }
    
}
