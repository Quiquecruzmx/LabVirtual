package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.MaterialDidactico;
import Plantillas.Practica;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;

public class DescargarMaterial extends ActionSupport implements SessionAware{
    private Map<String, Object> sessionAttributes = null;
    private InputStream fileInputStream;
    private String nombreMaterial;

    public InputStream getFileInputStream() {
        return fileInputStream;
    }
    public void setFileInputStream(InputStream fileInputStream){
        this.fileInputStream = fileInputStream;
    }

    public String getNombreMaterial() {
        return nombreMaterial;
    }

    public void setNombreMaterial(String nombreMaterial) {
        this.nombreMaterial = nombreMaterial;
    }
    @Override
    public String execute()throws Exception{
        
        Usuario user = (Usuario) sessionAttributes.get("USER");
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        System.out.println("bb: "+nombreMaterial);
        Query query = hbs.createQuery("FROM MaterialDidactico WHERE nombreMaterial='"+nombreMaterial+"'");
        Iterator it = query.iterate();
        System.out.println("pac: "+query.list());
        try{
            while(it.hasNext()){
                MaterialDidactico md = (MaterialDidactico)it.next();
                fileInputStream = new FileInputStream(new File(md.getUbicacion()));
                System.out.println(fileInputStream);
            }
        }catch(Exception ex){
            System.out.println(":Vv");
            ex.printStackTrace();
            System.out.println("No se encontró el archivo");
            addActionError("No se encontró el archivo");
            hbs.close();
            return INPUT;
        }
        hbs.close();
        return SUCCESS;
    }
    @Override
    public void setSession(Map <String, Object> sessionAttributes){
        this.sessionAttributes = sessionAttributes;
    }
    
}
