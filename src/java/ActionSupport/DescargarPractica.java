package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Practica;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;


public class DescargarPractica extends ActionSupport implements SessionAware{
    private Map<String, Object> sessionAttributes = null;
    private InputStream fileInputStream;
    private String nombrePractica;

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public String getNombrePractica() {
        return nombrePractica;
    }

    public void setNombrePractica(String nombrePractica) {
        this.nombrePractica = nombrePractica;
    }

    @Override
    public String execute()throws Exception{
        System.out.println("Abriendo la práctica: "+nombrePractica);
        Usuario user = (Usuario)sessionAttributes.get("USER");
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        Query query = hbs.createQuery("FROM Practica");
        Iterator it = query.iterate();
        try{
            while(it.hasNext()){
                Practica p =(Practica)it.next();
                if(nombrePractica.compareTo(p.getTitulo()) == 0){
                    System.out.println("practica: "+p.getPdf());
                    fileInputStream = new FileInputStream(new File(p.getPdf()));
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
            System.out.println("No hay");
            return INPUT;
        }
        hbs.close();
        return SUCCESS;
    }
    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
    
}
