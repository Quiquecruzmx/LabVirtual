/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.Usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import com.opensymphony.xwork2.ActionSupport;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.struts2.interceptor.SessionAware;

public class DownloadAction extends ActionSupport implements SessionAware{

	private InputStream fileInputStream;
        private String nombreProyecto;
        private Map <String,Object> sesionDatos = null; 
	
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }
        

        @Override
	public String execute(){
            Usuario u = (Usuario) sesionDatos.get("USER");
            try {
                fileInputStream = new FileInputStream(new File("C:\\Ficheros\\"+u.getUsername()+"\\"+nombreProyecto+"\\"+nombreProyecto+".exe"));
                return SUCCESS;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DownloadAction.class.getName()).log(Level.SEVERE, null, ex);
                return INPUT;
            }
	}

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
}

