/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.Usuario;
import Utilidades.CreadorZip;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import com.opensymphony.xwork2.ActionSupport;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.SessionAware;

public class DownloadSourceCode extends ActionSupport implements SessionAware{

	private InputStream fileInputStream;
        private String nombreProyecto;
        private String nombreAutor;
        private List archivosDescarga;
        private Map <String,Object> sesionDatos = null; 
	
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public List getArchivosDescarga() {
        return archivosDescarga;
    }

    public void setArchivosDescarga(List archivosDescarga) {
        this.archivosDescarga = archivosDescarga;
    }
    
    

    @Override
    public String execute() throws IOException{
        Usuario u = (Usuario) sesionDatos.get("USER");
        CreadorZip cz = new CreadorZip();
        
        System.out.println("Datos recibidos: Nombre: "+nombreAutor+"  Proyecto: "+nombreProyecto);
        
        if(archivosDescarga == null){
            System.out.println("No hay archivos seleccionados");
            addActionError("No seleccionó archivos para descargar, intente de nuevo");
            return INPUT;
        }
        
        for(int i=0; i<archivosDescarga.size(); i++){
            System.out.println("Archi["+i+"] : "+archivosDescarga.get(i).toString());
            cz.copiarTemporal(archivosDescarga.get(i).toString(), "C:\\Ficheros\\"+u.getUsername()+"\\@Recibidos\\"+nombreProyecto+"-"+nombreAutor);
        }
        
        File inputDir = new File("C:\\Temporal");
        File outputZipFile = new File("C:\\Ficheros\\"+u.getUsername()+"\\@Recibidos\\"+nombreProyecto+"-"+nombreAutor+".zip");
 
        cz.zipDirectory(inputDir, outputZipFile);
        
        try {
            System.out.println("Proyecto a descargar de la carpeta @Recibidos");
            fileInputStream = new FileInputStream(outputZipFile);
            //FileUtils.cleanDirectory(inputDir); 
            return SUCCESS;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DownloadAction.class.getName()).log(Level.SEVERE, null, ex);
            return INPUT;
        }
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
}

