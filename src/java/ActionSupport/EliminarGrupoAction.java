package ActionSupport;

import Plantillas.BorrarUsuario;
import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Utilidades.Compilador;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EliminarGrupoAction extends ActionSupport {
    
    private int grupoBorrar;

    public int getGrupoBorrar() {
        return grupoBorrar;
    }

    public void setGrupoBorrar(int grupoBorrar) {
        this.grupoBorrar = grupoBorrar;
    }
    
    @Override
    public String execute() throws Exception {
        Session hiberSession;
        hiberSession=HibernateUtil.getSessionFactory().openSession(); 
        Transaction t=hiberSession.beginTransaction(); 

        Grupo gBorrar=(Grupo)hiberSession.load(Grupo.class, grupoBorrar);

        hiberSession.delete(gBorrar);
        t.commit(); 
        hiberSession.close();

        return SUCCESS;
    }
    
}
