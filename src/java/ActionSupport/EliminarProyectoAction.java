package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Proyecto;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EliminarProyectoAction extends ActionSupport {
    
    private int idP;

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }
    
    @Override
    public String execute() throws Exception {
        Session hiberSession;
        hiberSession=HibernateUtil.getSessionFactory().openSession(); 
        Transaction t=hiberSession.beginTransaction(); 

        Proyecto proy =(Proyecto)hiberSession.load(Proyecto.class, idP);
        
        hiberSession.delete(proy);
        t.commit(); 
        
//        Proyecto p = (Proyecto) hiberSession.load(Proyecto.class, idP);
//        Compilador c = new Compilador().destruirFicheros(p.getNombreProyecto(), p.getAutor());
        hiberSession.close();

        return SUCCESS;
    }
    
}
