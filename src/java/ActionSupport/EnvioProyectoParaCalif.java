package ActionSupport;

import Plantillas.Calificacion;
import Plantillas.HibernateUtil;
import Plantillas.Proyecto;
import Plantillas.Usuario;
import Utilidades.Compilador;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EnvioProyectoParaCalif extends ActionSupport implements SessionAware{
    
    private int idProfe;
    private int idProy;
    private String nombrePracticaEnvio;
    private Map<String,Object> sesionDatos = null;  

    public int getIdProfe() {
        return idProfe;
    }

    public void setIdProfe(int idProfe) {
        this.idProfe = idProfe;
    }

    public int getIdProy() {
        return idProy;
    }

    public void setIdProy(int idProy) {
        this.idProy = idProy;
    }

    public String getNombrePracticaEnvio() {
        return nombrePracticaEnvio;
    }

    public void setNombrePracticaEnvio(String nombrePracticaEnvio) {
        this.nombrePracticaEnvio = nombrePracticaEnvio;
    }



    
    @Override
    public String execute(){
        System.out.println("Recibiendo profesorID: "+idProfe+" y nombre: "+nombrePracticaEnvio);
        
        if(idProfe<=0){
            addActionError("No se ha mandado este proyecto porque no se asignó a alguna práctica");
            return INPUT;
        }
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        Transaction t= hiberSession.beginTransaction();
        System.out.println(idProfe);
        Usuario u = (Usuario) sesionDatos.get("USER");
        Proyecto pr = (Proyecto) hiberSession.load(Proyecto.class, idProy);
        Usuario prof = (Usuario) hiberSession.load(Usuario.class, idProfe);
        
        Calificacion valida = (Calificacion) hiberSession.createQuery("from Calificacion where idProyecto = '"+idProy+"' AND idProfesor='"+idProfe+"' AND nombrePractica='"+nombrePracticaEnvio+"'").uniqueResult();
        
        Compilador c = new Compilador();
        c.copiarCarpetaProyecto(u.getUsername(), prof.getUsername(), pr.getNombreProyecto(), nombrePracticaEnvio);
        System.out.println("Carperta copiada con: "+nombrePracticaEnvio+" del alumno "+ u.getUsername());
        if(valida != null){
            Calificacion calif = (Calificacion) hiberSession.load(Calificacion.class, valida.getIdCal());
            calif.setIdProyecto(pr);
            calif.setIdProfesor(prof);
            calif.setIdAlumno(u);
            calif.setNombrePractica(nombrePracticaEnvio);
            hiberSession.update(calif);
            t.commit();
            addActionMessage("Proyecto reenviado al profesor: "+prof.getNombre()+" "+prof.getApePat());
            return SUCCESS;
            
        }
        
        Calificacion calif = new Calificacion();
        calif.setIdProyecto(pr);
        calif.setIdProfesor(prof);
        calif.setIdAlumno(u);
        calif.setNombrePractica(nombrePracticaEnvio);
        hiberSession.save(calif);
        t.commit();
  
        
        addActionMessage("Proyecto enviado al profesor: "+prof.getNombre()+" "+prof.getApePat()+". Esperando calificacion. Para mas detalles, revise \"Detalles de proyectos\"");
        return SUCCESS;
        
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
    
}
