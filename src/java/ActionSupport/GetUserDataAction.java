package ActionSupport;

import Plantillas.Usuario;
import Plantillas.HibernateUtil;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.apache.struts2.interceptor.SessionAware;


public class GetUserDataAction extends ActionSupport implements SessionAware{
    private int idToUpdate;
    Usuario datos;
    private Map<String,Object> sesionDatos = null;   

    public int getIdToUpdate() {
        return idToUpdate;
    }

    public void setIdToUpdate(int idToUpdate) {
        this.idToUpdate = idToUpdate;
    }
    
    public String getNombre(){
        return datos.getNombre();
    }
    
    public String getUsername(){
        return datos.getUsername();
    }
    
    public String getApePat(){
        return datos.getApePat();
    }
    public String getApeMat(){
        return datos.getApeMat();
    }
    public String getRol(){
        return datos.getRol();
    }
    public String getCorreo(){
        return datos.getCorreo();
    }
    
    
    
    @Override
    public String execute() throws Exception{
        
        if(idToUpdate == 1){
            return "prohibido";
        }
        else{
            Session hiberSession = HibernateUtil.getSessionFactory().openSession();
            Transaction t = hiberSession.beginTransaction();
            datos = (Usuario) hiberSession.createQuery("from Usuario where idUsuario='"+idToUpdate+"'").uniqueResult();
            t.commit();
            System.out.println("datos IDDD: "+getIdToUpdate());
            sesionDatos.put("RECUPERADO", datos);


            return SUCCESS;
        }
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
       this.sesionDatos =sesionDatos;
    }

}
