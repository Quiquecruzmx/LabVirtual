package ActionSupport;

import Plantillas.Alumno;
import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class InscribirAction extends ActionSupport implements SessionAware {
    private int idGrupo;
    private List alumno = null;
    private Map<String, Object> sessionAttributes = null;

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public List getAlumno() {
        return alumno;
    }

    public void setAlumno(List alumno) {
        this.alumno = alumno;
    }

    
    
    
    
    @Override
    public String execute() {
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        Transaction t=hiberSession.beginTransaction();
        Alumno alu;
        
        if(alumno == null){
           addActionError("No seleccionó inscritos");
           return ERROR; 
        }
        
        if(alumno.isEmpty() || alumno.get(0).toString() == "false"){
            addActionError("No seleccionó inscritos");
            return ERROR;
        }
        System.out.println("Alumno: "+alumno.get(0).toString());
        try{
            Usuario u;
            Grupo g = (Grupo) hiberSession.load(Grupo.class, idGrupo);
            for(int i = 0; i < alumno.size(); i++){
                alu = new Alumno();
                u = (Usuario) hiberSession.load(Usuario.class, Integer.parseInt(alumno.get(i).toString()));
                
                alu.setIdAlumno(u);
                alu.setIdGrupo(g);
                hiberSession.save(alu);
            }
        //Obtener los objetos correspondientes
        
              

        t.commit();
        hiberSession.close();
        return SUCCESS;
        }
        catch(HibernateException e){
            
            System.out.println(e.getMessage());
            System.out.println("Hubo un error procesando los datos proporcionados, verifíquelos");
            addActionError("Hubo un error procesando los datos proporcionados, verifíquelos ");
            return ERROR;
        }
    }

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
    
}
