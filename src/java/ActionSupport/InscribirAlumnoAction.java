package ActionSupport;

import Plantillas.Alumno;
import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class InscribirAlumnoAction extends ActionSupport {
    
    int idAlumno;
    String usernameAlumno;
    int idGrupo;

    public int getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(int idAlumno) {
        this.idAlumno = idAlumno;
    }

    public String getUsernameAlumno() {
        return usernameAlumno;
    }

    public void setUsernameAlumno(String usernameAlumno) {
        this.usernameAlumno = usernameAlumno;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }
    
    
    
    @Override
    public String execute() throws Exception {
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        Transaction t=hiberSession.beginTransaction();
        Alumno alu = new Alumno();
        System.out.println(idGrupo+" "+idAlumno+" "+usernameAlumno);
        Usuario u = (Usuario) hiberSession.load(Usuario.class, idAlumno);
        Grupo g = (Grupo) hiberSession.load(Grupo.class, idGrupo);
        
        if(u.getUsername().compareTo(usernameAlumno) != 0){
            addActionError("El ID o nombre de usuario son incorrectos");
            return INPUT;
        }
        
        try{
            if(!hiberSession.createQuery("from Alumno where idAlumno="+u.getId()+" AND idGrupo="+g.getIdGrupo()).list().isEmpty()){
                addActionError("Este alumno ya está inscrito en este grupo");
                //t.rollback();
                hiberSession.close();
                return INPUT;
            }
            alu.setIdAlumno(u);
            alu.setIdGrupo(g);
            hiberSession.save(alu);
            t.commit();
            hiberSession.close();
            return SUCCESS;
            
        }
        catch (Exception e){
            System.err.println(e.getMessage());
            System.out.println(idGrupo+" "+idAlumno+" "+usernameAlumno);
            t.rollback();
            hiberSession.close();
            return INPUT;
        }
    }
    
}
