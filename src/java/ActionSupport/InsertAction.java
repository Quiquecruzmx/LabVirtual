package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.InsertarUsuario;
import Plantillas.Usuario;
import Utilidades.Compilador;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class InsertAction extends ActionSupport {
    private String newUsername;
    private String newPass;
    private String newNombre;
    private String newApePat;
    private String newApeMat;
    private String newRol;
    private String newCorreo;

    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getNewNombre() {
        return newNombre;
    }

    public void setNewNombre(String newNombre) {
        this.newNombre = newNombre;
    }

    public String getNewApePat() {
        return newApePat;
    }

    public void setNewApePat(String newApePat) {
        this.newApePat = newApePat;
    }

    public String getNewApeMat() {
        return newApeMat;
    }

    public void setNewApeMat(String newApeMat) {
        this.newApeMat = newApeMat;
    }

    public String getNewRol() {
        return newRol;
    }

    public void setNewRol(String newRol) {
        this.newRol = newRol;
    }

    public String getNewCorreo() {
        return newCorreo;
    }

    public void setNewCorreo(String newCorreo) {
        this.newCorreo = newCorreo;
    }
    
        @Override
    public void validate(){ //Por default, este arroja un input
        if(StringUtils.isEmpty(getNewUsername())){
            addFieldError("newUsername","Se requiere este campo");
        }
        
        if(StringUtils.isEmpty(getNewPass())){
            addFieldError("newPass","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewNombre())){
            addFieldError("newNombre","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewApePat())){
            addFieldError("newApePat","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewApeMat())){
            addFieldError("newApeMat","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewCorreo())){
            addFieldError("newCorreo","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewRol())){
            addFieldError("newRol","Se requiere este campo");
            //return false;
        }
    }
    
    @Override
    public String execute(){
        Session hiberSession;
        hiberSession=HibernateUtil.getSessionFactory().openSession(); 
        Transaction t=hiberSession.beginTransaction();
        InsertarUsuario iu = new InsertarUsuario();
        Usuario u = (Usuario) hiberSession.createQuery("from Usuario where username='"+newUsername+"'").uniqueResult();
        if(u!=null){
            addActionError("Ya existe un usuario con este nombre");
            return ERROR;
        }
     //Insert
      try{
            iu.setUsername(newUsername);
            iu.setPassword(newPass);
            iu.setNombre(newNombre);
            iu.setApePat(newApePat);
            iu.setApeMat(newApeMat);
            iu.setCorreo(newCorreo);
            iu.setRol(newRol);
            hiberSession.save(iu);
            t.commit();
            Compilador compilador = new Compilador();
            compilador.generarCarpetaUsuario(newUsername);
            //Compilador.generarCarpetaUsuario(newUsername);
            
            return SUCCESS;
    }
        catch(HibernateException e){
            
            t.rollback();
            return ERROR;
    }
        
    }
    
    
}
