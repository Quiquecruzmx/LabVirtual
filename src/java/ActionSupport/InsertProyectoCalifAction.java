
package ActionSupport;

import Plantillas.Calificacion;
import Plantillas.HibernateUtil;
import Plantillas.Proyecto;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;


public class InsertProyectoCalifAction extends ActionSupport implements SessionAware {
    

    private int idProy;
    private String nombrePractica;
    private Map<String,Object> sesionDatos = null;  
    private String calificacion;
    private String observaciones;
    private String rubrica;

    public int getIdProy() {
        return idProy;
    }

    public void setIdProy(int idProy) {
        this.idProy = idProy;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getNombrePractica() {
        return nombrePractica;
    }

    public void setNombrePractica(String nombrePractica) {
        this.nombrePractica = nombrePractica;
    }

    public String getRubrica() {
        return rubrica;
    }

    public void setRubrica(String rubrica) {
        this.rubrica = rubrica;
    }
    
    
    
    
    public String calificacion(){
        System.out.println("Calificacion: "+calificacion+" Nombre practica:"+nombrePractica);
        System.out.println("Rubrica : "+rubrica);
     
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        Transaction t = hiberSession.beginTransaction();
        Usuario u = (Usuario) sesionDatos.get("USER");
        
        PrintWriter writer;
        String rutaRub = "C:\\Rubricas\\"+nombrePractica+"-"+u.getUsername()+"-"+idProy+".txt";
        try {
            writer = new PrintWriter(rutaRub, "UTF-8");
            writer.print(rubrica);
            writer.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InsertProyectoCalifAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(InsertProyectoCalifAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        Calificacion calif = (Calificacion) hiberSession.createQuery("from Calificacion where idProyecto = "+idProy+" AND"
                + " idProfesor="+u.getId()+" AND nombrePractica='"+nombrePractica+"'").uniqueResult();
        calif.setCalificacion(calificacion);
        calif.setObservaciones(observaciones);
        calif.setRubrica(rutaRub);
        hiberSession.update(calif);
        if (!t.wasCommitted())
            t.commit();
        addActionMessage("Proyecto calificado");
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
    
}
