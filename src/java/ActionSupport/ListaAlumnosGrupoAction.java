/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author admin
 */
public class ListaAlumnosGrupoAction extends ActionSupport implements SessionAware{
    private int grupo;
    private List alumnos; 
    private String nombreGrupo;
    private Map<String, Object> sessionAttributes = null;

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public List getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(List alumnos) {
        this.alumnos = alumnos;
    }
  
   
 
    @Override
    public String execute(){
        
        
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        Usuario u = (Usuario) sessionAttributes.get("USER");
        String cadena = "from Alumno where idGrupo="+grupo;
        Query select = hiberSession.createQuery(cadena); //Consulta los grupos del usuario logueado aquí
        alumnos = select.list();
        String cad = "FROM Grupo WHERE idGrupo="+grupo;
        Query select1 = hiberSession.createQuery(cad);
        Iterator it = select1.iterate();
        Grupo g = (Grupo) it.next();
        System.out.println("AAA "+g.getNombreGrupo());
        nombreGrupo = g.getNombreGrupo();
        
        if(alumnos== null || alumnos.isEmpty()){ //En caso de no capturar nada
            addActionMessage("Este grupo aún no posee alumnos");
            return INPUT;
        }
        
        else {
            if(u.getRol().compareTo("profesor")==0)
                return SUCCESS;
            else if(u.getRol().compareTo("alumno")==0)
                return SUCCESS+"alu";
            else
                return ERROR;
        }
        
    }

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
}
