package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;

public class ListaGruposAction extends ActionSupport implements SessionAware{
    
    private List grupos;
    private List profesores;
    private Map<String,Object> sesionDatos = null;  

    public List getGrupos() {
        return grupos;
    }

    public void setGrupos(List grupos) {
        this.grupos = grupos;
    }

    public List getProfesores() {
        return profesores;
    }

    public void setProfesores(List profesores) {
        this.profesores = profesores;
    }
    
    
    
    @Override
    public String execute() throws Exception {
        Usuario u = (Usuario)sesionDatos.get("USER"); //Obtenemos el usuario logueado en esta sesión
        System.out.println("Nombre usuario:"+u.getUsername());
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        
        if(u.getRol().compareTo("admon")==0){
            grupos= hiberSession.createQuery("FROM Grupo").list();
            profesores = hiberSession.createQuery("FROM Usuario WHERE rol = 'profesor'").list();
            sesionDatos.put("PROFESORES", profesores);
            if(grupos  == null || grupos.isEmpty()){ //En caso de no capturar nada
                addActionMessage("No hay grupos creados aún");
                return INPUT;
            }
            else {
                sesionDatos.put("GRUPOS", grupos);
                
                return SUCCESS;
            }      
        }
        else{
            grupos = hiberSession.createQuery("from Grupo where titular="+ u.getId()).list(); //Consulta los grupos del usuario logueado aquí
            if(grupos  == null || grupos.isEmpty()){ //En caso de no capturar nada
                return INPUT;
            }

            else {
                //sesionDatos.put("GRUPOS_PROFE", grupos);
                return SUCCESS;
            }
        }
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
    
    
}
