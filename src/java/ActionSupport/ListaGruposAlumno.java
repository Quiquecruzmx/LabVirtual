package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Session;

public class ListaGruposAlumno extends ActionSupport implements SessionAware{
    
    private List gruposAlumno;
    private Map<String,Object> sesionDatos = null;  

    public List getGruposAlumno() {
        return gruposAlumno;
    }

    public void setGruposAlumno(List gruposAlumno) {
        this.gruposAlumno = gruposAlumno;
    }
    
    
    
    @Override
    public String execute() throws Exception {
        
        Usuario u = (Usuario)sesionDatos.get("USER"); //Obtenemos el usuario logueado en esta sesión
        System.out.println("Nombre usuario:"+u.getUsername());
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        gruposAlumno = hiberSession.createQuery("select distinct idGrupo from Alumno where idAlumno="+u.getId()).list();
        if(gruposAlumno  == null || gruposAlumno.isEmpty()){ //En caso de no capturar nada
            addActionMessage("No hay grupos aún");
            return INPUT;
        }

        else {
            //sesionDatos.put("GRUPOS_PROFE", grupos);
            return SUCCESS;
        }
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
    
}
