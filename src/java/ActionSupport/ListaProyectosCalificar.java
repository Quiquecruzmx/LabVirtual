package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author admin
 */
public class ListaProyectosCalificar extends ActionSupport implements SessionAware{
    
    private List proyectosEvaluacion;
    private Map<String,Object> sesionDatos = null;  

    public List getProyectosEvaluacion() {
        return proyectosEvaluacion;
    }

    public void setProyectosEvaluacion(List proyectosEvaluacion) {
        this.proyectosEvaluacion = proyectosEvaluacion;
    }    
    
    @Override
    public String execute() throws Exception {
        Usuario u = (Usuario)sesionDatos.get("USER"); //Obtenemos el usuario logueado en esta sesión
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        Query select = hiberSession.createQuery("from Calificacion where idProfesor="+ u.getId()); //Consulta los grupos del usuario logueado aquí
        if((proyectosEvaluacion = select.list()) == null || select.list().isEmpty()){ //En caso de no capturar nada
            addActionMessage("No tiene proyectos para evaluar aún");
            return INPUT;
        }
        
        else {
            //sesionDatos.put("GRUPOS_PROFE", grupos);
            return SUCCESS;
        }
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
}
