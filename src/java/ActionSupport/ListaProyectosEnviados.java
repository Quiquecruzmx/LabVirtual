/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.Calificacion;
import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author admin
 */
public class ListaProyectosEnviados extends ActionSupport implements SessionAware{
    private List proyectosEnviados;
    private List <String> rubricas = new ArrayList<String>();
    private Map<String,Object> sesionDatos = null;  

    public List getProyectosEnviados() {
        return proyectosEnviados;
    }

    public void setProyectosEnviados(List proyectosEnviados) {
        this.proyectosEnviados = proyectosEnviados;
    }

    public List <String> getRubricas() {
        return rubricas;
    }

    public void setRubricas(List <String> rubricas) {
        this.rubricas = rubricas;
    }

    
    
    @Override
    public String execute() throws Exception {
        Usuario u = (Usuario)sesionDatos.get("USER"); //Obtenemos el usuario logueado en esta sesión
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        System.out.println("Autor: "+u.getId());
        Query select = hiberSession.createQuery("from Calificacion where idAlumno="+ u.getId()); //Consulta los grupos del usuario logueado aquí
        
        if((proyectosEnviados = select.list()) == null || select.list().isEmpty()){ //En caso de no capturar nada
            addActionMessage("No tiene proyectos enviados");
            return INPUT;
        }
        
        else {      
            System.out.println("No proyectos:"+proyectosEnviados.size());
            BufferedReader br = null;
            FileReader fr = null;
            String res="";
            for(int i=0; i<proyectosEnviados.size(); i++){
                System.out.println("Iteracion No: "+i);
                Calificacion c = (Calificacion) proyectosEnviados.get(i);
                if(c.getRubrica()==null){
                    System.out.println("No hay rubrica para esto");
                    rubricas.add("");
                    //return SUCCESS;
                }
                else{
                try {

                    br = new BufferedReader (new InputStreamReader (new FileInputStream (c.getRubrica()), "utf-8"));

                    String sCurrentLine;                  

                    while ((sCurrentLine = br.readLine()) != null) {
                            System.out.println(sCurrentLine);
                            res+=sCurrentLine;
                    }
                    rubricas.add(res);
                    res="";

                } catch (IOException e) {

                    e.printStackTrace();

                } finally {

                    try {

                            if (br != null)
                                    br.close();

                            if (fr != null)
                                    fr.close();

                    } catch (IOException ex) {

                            ex.printStackTrace();

                    }

                }
                }
            }            
            
            return SUCCESS;
        }
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
    
}
