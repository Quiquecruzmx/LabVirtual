package ActionSupport;

import Plantillas.Alumno;
import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class LlenarGrupoAction extends ActionSupport {

    private List alumnos;
    private int grupo;
     private String nombreGrupo;

    public List getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(List alumnos) {
        this.alumnos = alumnos;
    }

    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    
    @Override
    public String execute() throws Exception {
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        ArrayList listaFinal;
        System.out.println(grupo);
        Grupo g = (Grupo) hiberSession.load(Grupo.class, grupo);
        nombreGrupo = g.getNombreGrupo();
        String queryUsuarios = "from Usuario where rol= 'alumno' ";
        String queryAlumnos = "from Alumno where idGrupo="+grupo;
        List <Usuario> alumnosTotales = hiberSession.createQuery(queryUsuarios).list(); //Consulta los usuarios
        List <Alumno> inscritos = hiberSession.createQuery(queryAlumnos).list(); //Consulta los usuarios
        //Query select = hiberSession.createQuery(usuarios);
        try{
            for(int  i=0; i<alumnosTotales.size(); i++){
                for(int j= 0;j<inscritos.size(); j++){
                    if(inscritos.get(j).getIdAlumno().getId() == alumnosTotales.get(i).getId()){
                        alumnosTotales.remove(alumnosTotales.get(i));

                    }
                }

            }
            alumnos = alumnosTotales;
            hiberSession.close();
            return SUCCESS;
        }catch(Exception e){
            addActionMessage("Todos los alumnos disponibles ya fueron inscritos en este grupo");
            return INPUT;
        }
        
        
        
    }
    
    
    private ArrayList diferencia(String A[], String B[]){
        //Transformando los arreglos en ArrayList
        ArrayList <String> listaFinal = new ArrayList <String>();
        List <String> lista1 = new ArrayList <>();
        List <String> lista2 = new ArrayList <>();
        lista1 = Arrays.asList(A);
        lista2 = Arrays.asList(B);

        for(String elemento: lista2){
            if(!lista1.contains(elemento)){
                listaFinal.add(elemento);
            }
        }
        return listaFinal;
    }
    
}
