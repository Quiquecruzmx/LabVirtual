package ActionSupport;

import Plantillas.Usuario;
import Plantillas.HibernateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;
import org.apache.struts2.interceptor.SessionAware;

public class LoginAction extends ActionSupport implements SessionAware{
private String username;    
private String password; 
private Session hibernateSession; //Variable que contendrá la sesion actual del usuario para realizar ocnsultas
private Usuario userLogin; //objeto de tipo Usuario que permitirá realizar las consultas
private Map<String, Object> sessionAttributes = null;

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

@Override
 public String execute() throws Exception {

    hibernateSession = HibernateUtil.getSessionFactory().openSession(); 
    Transaction t1 = hibernateSession.beginTransaction();
    Criteria crit = hibernateSession.createCriteria(Usuario.class);
    crit.add(Restrictions.sqlRestriction("username = ? collate utf8_bin", username,  new StringType())); //Criterio para que el username sea EXACTAMENTE igual al de la base
    crit.add(Restrictions.sqlRestriction("password = ? collate utf8_bin", password,  new StringType())); //Criterio para que el password sea EXACTAMENTE igual al de la base
    userLogin = (Usuario) crit.uniqueResult(); //mapeo
    hibernateSession.close();  
            //=(Usuario) hibernateSession.createQuery("from Usuario where username='"+username+"' AND password='"+password+"'").uniqueResult();
    t1.commit();
    
    if(username!=null && password!=null &&(!username.equals("")) &&(!password.equals("")))
        {
        if(userLogin!=null  && userLogin.getRol().equals("admon")){
            sessionAttributes.put("USER", userLogin);
            return LOGIN+"admon"; 
        }

        if(userLogin!=null  && userLogin.getRol().equals("profesor")){
            sessionAttributes.put("USER", userLogin);
            return LOGIN+"prof";
        }

        if(userLogin!=null  && userLogin.getRol().equals("alumno")){
            sessionAttributes.put("USER", userLogin);
            return LOGIN+"alumno";
        }
     }
    addActionError("Fallo al iniciar sesión");
    return ERROR;
    

    
    }

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
            this.sessionAttributes = sessionAttributes;
    }

 }