/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Enrique
 */
public class MaterialProfe extends ActionSupport implements SessionAware{
    private List lista;
    private Map<String, Object> sessionAttributes = null; 

    public List getLista() {
        return lista;
    }

    public void setLista(List lista) {
        this.lista = lista;
    }
    @Override
    public String execute() throws Exception{
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        Usuario user = (Usuario)sessionAttributes.get("USER");
        String query = "FROM MaterialDidactico WHERE autor="+user.getId();
        Query sentencia = hbs.createQuery(query);
        System.out.println("La lista: "+sentencia.list());
        if((lista = sentencia.list()) == null){ //En caso de no capturar nada
            return ERROR;
        }
        sessionAttributes.put("lista", lista);
        hbs.close();
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
}
