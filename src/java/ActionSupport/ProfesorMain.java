/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;

public class ProfesorMain extends ActionSupport implements SessionAware{
    private List listaProyectos;
    private Map<String,Object> sesionDatos = null; 

    public List getListaProyectos() {
        return listaProyectos;
    }

    public void setListaProyectos(List listaProyectos) {
        this.listaProyectos = listaProyectos;
    }
    
    @Override
    public String execute() throws Exception{
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        System.out.println(sesionDatos);
        Usuario user = (Usuario)sesionDatos.get("USER");
        System.out.println("Recuperando los proyectos de "+user.getUsername()+"...");
        Query query = hbs.createQuery("from Proyecto WHERE autor="+user.getId());
        listaProyectos = query.list();
        hbs.close();
        sesionDatos.put("PROYECTOS_PROF", listaProyectos);
        return SUCCESS;
    }
    
    @Override
    public void setSession(Map<String, Object> sesionDatos) {
       this.sesionDatos = sesionDatos;
    }
}
