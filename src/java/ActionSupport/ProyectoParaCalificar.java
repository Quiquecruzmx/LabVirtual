package ActionSupport;

import Plantillas.Archivo;
import Plantillas.Calificacion;
import Plantillas.HibernateUtil;
import Plantillas.Practica;
import Plantillas.Usuario;
import Utilidades.Compilador;
import Utilidades.Proyectos;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Session;

public class ProyectoParaCalificar extends ActionSupport implements SessionAware{
    private String idP;//ID del proyecto que se va a abrir
    private String nombreProyecto;//nombre
    private int idAutor;
    private String nombrePractica;//nombre
    private List <Proyectos> proyectos;
    private List <Archivo> archs;
    private Map<String, Object> session = null;
    private float calificacion;
    private String observaciones;
    private String nombreAutor;
    //String extension = "";
    
    private String rubrica;

    public List<Proyectos> getProyectos() {
        return proyectos;
    }

    public void setProyectos(List<Proyectos> proyectos) {
        this.proyectos = proyectos;
    }
    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public String getIdP() {
        return idP;
    }

    public void setIdP(String idP) {
        this.idP = idP;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }
    public int getProyectosSize(){
        return proyectos.size();
    }

    public int getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(int idAutor) {
        this.idAutor = idAutor;
    }

    public float getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(float calificacion) {
        this.calificacion = calificacion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public String getNombrePractica() {
        return nombrePractica;
    }

    public void setNombrePractica(String nombrePractica) {
        this.nombrePractica = nombrePractica;
    }

    public String getRubrica() {
        return rubrica;
    }

    public void setRubrica(String rubrica) {
        this.rubrica = rubrica;
    }
    
    
    
    
    
    @Override
    public String execute() throws Exception{
        proyectos = new ArrayList<>();
        
        
        
        Compilador c =  new Compilador();
       
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        Usuario u = (Usuario) session.get("USER"); //Usuario logueado como profesor
        System.out.println("Datos: "+u.getUsername()+", nombre practica: "+nombrePractica);
        
        Calificacion calif = (Calificacion) hiberSession.createQuery("from Calificacion where idProyecto = "+idP+" AND idProfesor="+u.getId()
        +" AND nombrePractica='"+nombrePractica+"'").uniqueResult();
        observaciones = calif.getObservaciones();
        System.out.println("Calificacion: "+calificacion);
        if(calif.getCalificacion()!=null)
            calificacion = Float.parseFloat(calif.getCalificacion());
        System.out.println("Calificacion: "+calificacion);
        Usuario user = (Usuario)hiberSession.load(Usuario.class, idAutor); //autor del proyecto
        setNombreAutor(user.getUsername());
        archs = hiberSession.createQuery("from Archivo where idProyecto = "+idP+" AND autor="+idAutor).list();
        
        
        System.out.println("Abriendo el proyecto #"+idP+":"+nombreProyecto+" del usuario "+user.getUsername()+"...");
        String ruta = c.getRutaCompleta() + "\\"+u.getUsername()+"\\@Recibidos\\"+nombreProyecto+"-"+user.getUsername()+"-"+nombrePractica;
       
        File directorio = new File(ruta);
        String [] archivos = new String [archs.size()];
        
        for(int i=0; i<archs.size(); i++){
            archivos[i] = archs.get(i).getNombreArchivo();
        }
        
        if(directorio.exists()){
            //archivos = directorio.listFiles();
            System.out.println("Los archivos en el proyecto son:");
            for(int i = 0; i< archivos.length; i++){
                System.out.println(i + "-" + archivos[i]);
                BufferedReader br = new BufferedReader(new FileReader(ruta.trim()+"\\" + archivos[i]));
                String linea;
                String codigo = "";
                String extension = "";
                while((linea = br.readLine()) != null){
                    codigo = codigo+linea+"\n";
                }
                int a = archivos[i].indexOf(".");
                extension = archivos[i].substring(a, archivos[i].length());
                String nombre = archivos[i].substring(0, a);
                Proyectos p = new Proyectos(i, nombre, codigo, extension);
                proyectos.add(i, p);
                //System.out.println(session.get("cuenta"));
                session.put("cuenta", a);
            }
        }else{
            addActionError("No se puede abrir el proyecto, directorio no existe");
            return INPUT;
        }
        
        BufferedReader br = null;
        FileReader fr = null;
        String res="";
        System.err.println("Nombre prac: "+nombrePractica+" autor: "+user.getId());
        Practica p = (Practica) hiberSession.createQuery("from Practica WHERE titulo ='"+nombrePractica+"' AND idUsuario='"+u.getId()+"'").uniqueResult();
        if(p.getRubricaRuta()==null){
            return SUCCESS;
        }
        try {

            br = new BufferedReader (new InputStreamReader (new FileInputStream (p.getRubricaRuta()), "utf-8"));

            String sCurrentLine;                  

            while ((sCurrentLine = br.readLine()) != null) {
                    System.out.println(sCurrentLine);
                    res+=sCurrentLine;
            }
            rubrica  = res;
            session.put("rubrica", rubrica);
        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
        hiberSession.close();
        
            
        session.put("nombrePractica", nombrePractica);
        session.put("calif", calificacion);
        session.put("observaciones", observaciones);
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    
}
