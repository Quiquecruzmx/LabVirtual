package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.Usuario;
import Utilidades.Mail;
//import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.Transaction;
import org.hibernate.Session;

public class RecoverAction extends ActionSupport {
    
    private String correo;
    private Usuario userRec;
    private Session hs;

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    @Override
     public String execute(){
        hs = HibernateUtil.getSessionFactory().openSession();
        Transaction t = hs.beginTransaction();
        userRec = (Usuario) hs.createQuery("from Usuario where correo ='"+correo+"'").uniqueResult();
        t.commit();
        
        if(userRec == null){
            addActionError("Su correo no fue encontrado");
            return ERROR;
        }
        else{
            Mail em = new Mail();
            int entero = em.envioEmail(correo, userRec.getNombre(), userRec.getPassword());
//Mejorar
            if(entero == 1){
                addActionMessage("Se han enviado sus datos a su correo");
                return SUCCESS;
            }
            else{
                addActionError("Problema al enviar el correo");
                return ERROR;
            }
        }
        
        
    }
    
}