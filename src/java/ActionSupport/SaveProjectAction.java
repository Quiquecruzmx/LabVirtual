package ActionSupport;

import Plantillas.Usuario;
import Utilidades.Compilador;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import java.util.Map;

public class SaveProjectAction {
    
    private String nombreProyecto;

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }
      
    public String execute(){
        //Map <String, Object> sesionMapa = ActionContext.getContext().getSession();
        Usuario u = (Usuario) ActionContext.getContext().getSession().get("USER");
        System.out.println("Project Name: "+nombreProyecto);
        //Compilador.guardarEnBase(u.getId(), nombreProyecto);
        return SUCCESS;
    }
}
