/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActionSupport;

import Plantillas.HibernateUtil;
import Plantillas.MaterialDidactico;
import Plantillas.Usuario;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Session;
import org.hibernate.Transaction;
/**
 *
 * @author Enrique
 */
public class SubirArchivoAction extends ActionSupport implements SessionAware{
    private File archivoSubido;
    private String archivoSubidoContentType;
    private String archivoSubidoFileName;
    private String nombreNuevo;
    private Map<String, Object> sessionAttributes = null;
    private String ruta = "C:\\MaterialSubido\\";//Ruta donde se van a guardar los archivos en el servidor
    //private String ruta ="C:\\Users\\admin\\Desktop\\MaterialSubido\\";
    //private 

    public String getNombreNuevo() {
        return nombreNuevo;
    }
    
    public File getArchivoSubido() {
        return archivoSubido;
    }

    public String getArchivoSubidoContentType() {
        return archivoSubidoContentType;
    }

    public String getArchivoSubidoFileName() {
        return archivoSubidoFileName;
    }

    public String getRuta() {
        return ruta;
    }

    public void setNombreNuevo(String nombreNuevo) {
        this.nombreNuevo = nombreNuevo;
    }

    public void setArchivoSubido(File archivoSubido) {
        this.archivoSubido = archivoSubido;
    }

    public void setArchivoSubidoContentType(String archivoSubidoContentType) {
        this.archivoSubidoContentType = archivoSubidoContentType;
    }

    public void setArchivoSubidoFileName(String archivoSubidoFileName) {
        this.archivoSubidoFileName = archivoSubidoFileName;
    }
    
    @Override
    public String execute() throws Exception{
        System.out.println("EL NOMBRE DEL ARCHIVO ES: "+archivoSubidoFileName);
        System.out.println("EL TIPO DEL ARCHIVO ES: "+archivoSubidoContentType);    
        String extension = archivoSubidoFileName.substring(archivoSubidoFileName.lastIndexOf("."), archivoSubidoFileName.length());
        String ubicacion;
        System.out.println("EL NOMBRE NUEVO ES: "+nombreNuevo);        
        System.out.println("LA EXTENSION DEL ARCHIVO ES: "+extension);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        Usuario user = (Usuario) sessionAttributes.get("USER");
        MaterialDidactico md;
        if(nombreNuevo.compareTo("") != 0){
            ubicacion = ruta+nombreNuevo+extension;
            md = new MaterialDidactico(nombreNuevo+extension, user.getId() , ubicacion);
        }else{
            ubicacion = ruta+archivoSubidoFileName;
            md = new MaterialDidactico(archivoSubidoFileName, user.getId(), ubicacion);
            
        }
        File nuevoArchivo = new File(ubicacion);
        archivoSubido.renameTo(nuevoArchivo);
        session.save(md);
        t.commit();
        System.out.println("ARCHIVO GUARDADO EN: "+ubicacion);
        session.close();
        return Action.SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }

}
