package ActionSupport;

import org.apache.commons.lang3.StringUtils;
import Plantillas.ActualizarUsuario;
import Plantillas.HibernateUtil;
import Utilidades.Compilador;
//import antlr.StringUtils;
import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UpdateAction extends ActionSupport{
    private String idActualiza;
    private String newUsername, newPass, newNombre, newApePat, newApeMat, newRol, newCorreo;

    public String getIdActualiza() {
        return idActualiza;
    }

    public void setIdActualiza(String idActualiza) {
        this.idActualiza = idActualiza;
    }


    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getNewNombre() {
        return newNombre;
    }

    public void setNewNombre(String newNombre) {
        this.newNombre = newNombre;
    }

    public String getNewApePat() {
        return newApePat;
    }

    public void setNewApePat(String newApePat) {
        this.newApePat = newApePat;
    }

    public String getNewApeMat() {
        return newApeMat;
    }

    public void setNewApeMat(String newApeMat) {
        this.newApeMat = newApeMat;
    }

    public String getNewRol() {
        return newRol;
    }

    public void setNewRol(String newRol) {
        this.newRol = newRol;
    }

    public String getNewCorreo() {
        return newCorreo;
    }

    public void setNewCorreo(String newCorreo) {
        this.newCorreo = newCorreo;
    }
    
    @Override
    public void validate(){ //Por default, este arroja un input
        if(StringUtils.isEmpty(getNewUsername())){
            addFieldError("newUsername","Se requiere este campo");
        }
        
        if(StringUtils.isEmpty(getNewPass())){
            addFieldError("newPass","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewNombre())){
            addFieldError("newNombre","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewApePat())){
            addFieldError("newApePat","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewApeMat())){
            addFieldError("newApeMat","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewCorreo())){
            addFieldError("newCorreo","Se requiere este campo");
            //return false;
        }
        if(StringUtils.isEmpty(getNewRol())){
            addFieldError("newRol","Se requiere este campo");
            //return false;
        }
    }

    @Override
    public String execute(){
        Session hibernateSession=HibernateUtil.getSessionFactory().openSession();           
        Transaction t = hibernateSession.beginTransaction();  
        int id = Integer.parseInt(idActualiza);
        ActualizarUsuario update=(ActualizarUsuario)hibernateSession.load(ActualizarUsuario.class,id); 
        String nombreViejo = update.getUsername();
        Compilador comp = new Compilador();
        try{
            //System.err.println("Nombre en try: "+update.getUsername());
            
            comp.modificarCarpetaUsuario(nombreViejo, newUsername);
            //Compilador.modificarCarpetaUsuario(nombreViejo, newUsername);
            
            if(!update.getUsername().equals(newUsername)){
            update.setUsername(newUsername);}
            if(!update.getPassword().equals(newPass)){
            update.setPassword(newPass);}
            if(!update.getNombre().equals(newNombre)){
            update.setNombre(newNombre);}
            if(!update.getApePat().equals(newApePat)){
            update.setApePat(newApePat);}
            if(!update.getApeMat().equals(newApeMat)){
            update.setApeMat(newApeMat);}
            if(!update.getCorreo().equals(newCorreo)){
            update.setCorreo(newCorreo);}
            if(!update.getRol().equals(newRol)){
            update.setRol(newRol);}

            hibernateSession.update(update);
            t.commit();
            
            comp.modificarCarpetaUsuario(nombreViejo, newUsername);
            
            return SUCCESS;
        }
        catch(NumberFormatException | HibernateException e){
            addActionError("No se pudo realizar la modificación");
            
            t.rollback();
            System.err.println("Nombre en catch: "+update.getUsername());
            comp = new Compilador();
            comp.modificarCarpetaUsuario(newUsername, nombreViejo);
            //Compilador.modificarCarpetaUsuario(newUsername, nombreViejo);
            return ERROR;
        }
        
        finally{
            hibernateSession.close();
        }
    }
    
}