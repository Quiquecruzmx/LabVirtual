package ActionSupport;

import Plantillas.HibernateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;


public class UsersTableAction extends ActionSupport{
    private List lista;

    public List getLista() {
        return lista;
    }

    public void setLista(List lista) {
        this.lista = lista;
    }
    
    
    @Override
    public String execute() throws Exception {
        Session hiberSession = HibernateUtil.getSessionFactory().openSession();
        Query select = hiberSession.createQuery("from Usuario");
        if((lista = select.list()) == null){ //En caso de no capturar nada
            return ERROR;
        }
        
        else {return SUCCESS;}
    }
}
