package ActionSupport;

import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.MaterialDidactico;
import Plantillas.MaterialPractica;
import Plantillas.Practica;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class VerPracticaAction extends ActionSupport implements SessionAware{
    private Map<String, Object> sessionAttributes = null;
    private List<Practica> PDF;
    private List<List<MaterialDidactico>> mat;
    private List<List<Grupo>>grupos;

    public List<List<Grupo>> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<List<Grupo>> grupos) {
        this.grupos = grupos;
    }

    public List<List<MaterialDidactico>> getMat() {
        return mat;
    }

    public void setMat(List<List<MaterialDidactico>> mat) {
        this.mat = mat;
    }

    public Map<String, Object> getSessionAttributes() {
        return sessionAttributes;
    }

    public void setSessionAttributes(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }

    public List<Practica> getPDF() {
        return PDF;
    }

    public void setPDF(List<Practica> PDF) {
        this.PDF = PDF;
    }


    public VerPracticaAction(){
        
    }
    public String display(){
        return NONE;
    }
    @Override
    public String execute() throws Exception{
        PDF = new ArrayList<Practica>();
        mat =  new ArrayList<List<MaterialDidactico>>();
        grupos = new ArrayList<List<Grupo>>();
        
        Usuario user = (Usuario)sessionAttributes.get("USER");
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        
        System.out.println("Recuperando los grupos a los cuales está inscrito " + user.getUsername() + "...");
        Query q = hbs.createQuery("SELECT idGrupo FROM Alumno WHERE idAlumno='"+user.getId()+"'");
        System.out.println("Grupos recuperados: " + q.list().size());
        
        Iterator t = q.iterate();
        while(t.hasNext()){
            ArrayList<MaterialDidactico> arrayM = new ArrayList<MaterialDidactico>();
            
            Grupo g = (Grupo)t.next();
            System.out.println("Buscando las practicas del grupo: " + g.getNombreGrupo());
            
            Query query = hbs.createQuery("FROM MaterialPractica m WHERE m.grupo='"+g.getIdGrupo()+"'");
            //System.out.println("Materiales  totales recuperados: " + query.list().size());
            
            Iterator it = query.iterate();
            while(it.hasNext()){
                ArrayList<Grupo>arrayGrupos = new ArrayList<Grupo>();
                
                MaterialPractica mp = (MaterialPractica)it.next();
                Practica practica = mp.getPractica();
                
                Grupo gru = mp.getGrupo();
                
                if(!PDF.contains(practica)){
                    PDF.add(practica);
                    
                    arrayGrupos.add(gru);
                    grupos.add(arrayGrupos);
                    System.out.println("Se recuperó la práctica: " + practica.getTitulo() + " del grupo " + g.getNombreGrupo());
                }else{
                    
                }
                
                if(mp.getMaterialdidactico() != null){
                    MaterialDidactico md = mp.getMaterialdidactico();
                    System.out.println("El material " + md.getNombreMaterial() + " está asociado con la práctica: " + practica.getTitulo());
                    if(!arrayM.contains(md)){
                        arrayM.add(md);
                    }
                }else{
                    System.out.println("La práctica: " + practica.getTitulo() + " no tiene materiales asociados");
                }
                System.out.println(arrayGrupos.size());
                
            }
            
        }
        System.out.println("AAAAA" + grupos.size());
        for(Practica p:PDF){
            ArrayList<MaterialDidactico> arr = new ArrayList<MaterialDidactico>();
            Query qu = hbs.createQuery("SELECT materialdidactico FROM MaterialPractica WHERE idPractica='"+p.getIdPractica()+"'");
            Iterator r = qu.iterate();
            while(r.hasNext()){
                MaterialDidactico md = (MaterialDidactico)r.next();
                System.out.println(md.getNombreMaterial()+" de "+p.getTitulo());
                if(!arr.contains(md)){
                    arr.add(md);
                }
            }
            mat.add(arr);
        }
        hbs.close();
        return SUCCESS;       
    }

    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
    
}
