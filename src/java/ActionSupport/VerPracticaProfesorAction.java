package ActionSupport;

import Plantillas.Grupo;
import Plantillas.HibernateUtil;
import Plantillas.MaterialDidactico;
import Plantillas.MaterialPractica;
import Plantillas.Practica;
import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Query;
import org.hibernate.Session;


public class VerPracticaProfesorAction extends ActionSupport implements SessionAware{
    private Map<String, Object> sesionDatos = null;
    private List<Practica> practicas;
    private List<List<Grupo>> grupos; //lista bidimensional
    private List<Integer> IDG;
    private List<List<MaterialDidactico>> materiales;

    public List<List<MaterialDidactico>> getMateriales() {
        return materiales;
    }

    public void setMateriales(List<List<MaterialDidactico>> materiales) {
        this.materiales = materiales;
    }

    public List<List<Grupo>> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<List<Grupo>> grupos) {
        this.grupos = grupos;
    }

    public List<Practica> getPracticas() {
        return practicas;
    }

    public void setPracticas(List<Practica> practicas) {
        this.practicas = practicas;
    }
    
    @Override
    public String execute() throws Exception{
        
        Session hbs = HibernateUtil.getSessionFactory().openSession();
        Usuario user = (Usuario) sesionDatos.get("USER");
        
        System.out.println("Recuperando las prácticas creadas por " + user.getUsername());
        Query query = hbs.createQuery("FROM Practica WHERE idUsuario='"+user.getId()+"'");
        System.out.println(query.list().size() + " práctica(s) recuperada(s)");
        practicas = query.list();
        grupos = new ArrayList<List<Grupo>>();
        IDG = new ArrayList<Integer>();
        materiales = new ArrayList<List<MaterialDidactico>>();
        for(Practica p : practicas){
            //System.out.println("Estás en la práctica: " + p.getTitulo());
            query = hbs.createQuery("SELECT grupo FROM MaterialPractica WHERE idPractica='"+p.getIdPractica()+"'");
            //System.out.println("Esta práctica tiene asociados " + query.list().size() + " grupos");
            Iterator it = query.iterate();
            ArrayList<Grupo> arrayG = new ArrayList<Grupo>();
            ArrayList<MaterialDidactico> mat = new ArrayList<MaterialDidactico>();
            while(it.hasNext()){
                Grupo g = (Grupo)it.next();
                Query qu = hbs.createQuery("SELECT materialdidactico FROM MaterialPractica WHERE idGrupo='"+g.getIdGrupo()+"' AND idPractica='"+p.getIdPractica()+"'");
                //System.out.println(qu.list());
                Iterator rr = qu.iterate();
                while(rr.hasNext()){
                    MaterialDidactico md = (MaterialDidactico)rr.next();
                    if(!mat.contains(md)){
                        mat.add(md);
                    }
                }
                if(!arrayG.contains(g)){
                    arrayG.add(g);
                }
            }
            grupos.add(arrayG);
            materiales.add(mat);
        }
        
        //hbs.close();
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> sesionDatos) {
        this.sesionDatos = sesionDatos;
    }
    
}
