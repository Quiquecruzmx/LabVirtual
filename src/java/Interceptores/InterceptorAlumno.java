
package Interceptores;

import Plantillas.Usuario;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import java.util.Map;



public class InterceptorAlumno implements Interceptor{

    @Override
    public void destroy() {
        
    }

    @Override
    public void init() {
        
    }

    @Override
    public String intercept(ActionInvocation ai) throws Exception {
        Map<String, Object> sessionAttributes = ai.getInvocationContext().getSession();
        Usuario user = (Usuario) sessionAttributes.get("USER");
        //System.out.println("EL USUARIO ES: "+user.getNombre());
        if(user == null){
            return Action.LOGIN;
	}else{
            System.out.println("EL USUARIO ES: "+user.getNombre());
            return ai.invoke();
        }
    }
  
}
