
package Interceptores;

import Plantillas.Usuario;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import java.util.Map;



public class InterceptorLogin implements Interceptor{

    @Override
    public void destroy() {
        
    }

    @Override
    public void init() {
        
    }

    @Override
    public String intercept(ActionInvocation ai) throws Exception {
        Map<String, Object> sessionAttributes = ai.getInvocationContext().getSession();
        Usuario user = (Usuario) sessionAttributes.get("USER");
        if(user == null){
            System.out.println("No hay usuario loggeado");           
            return Action.LOGIN;
	}else{
            //System.out.println("Estás en el interceptor de sesión con el usuario: "+user.getNombre());
            return ai.invoke();
        }
    }
  
}
