/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interceptores;

import Plantillas.Usuario;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Enrique
 */
public class InterceptorRol extends AbstractInterceptor{
    private List<String> RolesPermitidos = new ArrayList<String>();
    private List<String> RolesProhibidos = new ArrayList<String>();
    private final String PROHIBIDO = "prohibido";
    
    public void setRolesPermitidos(String roles){
        this.RolesPermitidos = stringToList(roles);
    }
    
    public void setRolesProhibidos(String roles){
        this.RolesProhibidos = stringToList(roles);
    }

    @Override
    public String intercept(ActionInvocation ai) throws Exception {
        String resultado = null;
        Map<String, Object> sessionAttributes = ai.getInvocationContext().getSession();
        Usuario user = (Usuario) sessionAttributes.get("USER");
        
        //System.out.println("Estas en el interceptor de rol con el usuario: "+user.getUsername()+" y su rol es: "+user.getRol());
//        System.out.print("Los roles permitidos aquí son: ");
//        for(String rol: RolesPermitidos){
//            System.out.print("  -"+rol);
//        }
        
        if(estaPermitido(user)){
            resultado = ai.invoke();
        }else{
            resultado = PROHIBIDO;
        }
        return resultado;
        
    }
    
    protected boolean estaPermitido(Usuario user){//Evalúa si el rol del usuario está en la lista de RolesPermitidos
        if(RolesPermitidos.size() > 0){
            boolean result = false;
            for(String rol: RolesPermitidos){
                if(user.getRol().compareTo(rol) == 0)
                    result = true;
            }
            return result;   
        } else if(RolesProhibidos.size() > 0){
            for(String rol : RolesProhibidos){
                if(user.getRol().compareTo(rol) == 0)
                    return false;
            }
        }
        return true;
    }
            
    
    protected List<String> stringToList(String val) {//Convierte la cadena de parametros en una lista
        if (val != null) {
            String[] list = val.split("[ ]*,[ ]*");
            return Arrays.asList(list);
        } else {
           return Collections.EMPTY_LIST;
       }
    }

    
}
