
package Plantillas;


public class Alumno {
    private int idTablaAlumno;
    private Usuario idAlumno;
    private Grupo idGrupo;
    //protected Set UsersSet = new HashSet(0);

    public int getIdTablaAlumno() {
        return idTablaAlumno;
    }

    public void setIdTablaAlumno(int idTablaAlumno) {
        this.idTablaAlumno = idTablaAlumno;
    }

    public Usuario getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(Usuario idAlumno) {
        this.idAlumno = idAlumno;
    }

    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }
    
    
}
