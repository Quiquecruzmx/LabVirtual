
package Plantillas;

import java.io.Serializable;

public class Archivo implements Serializable{
    private int idArchivo;
    private String nombreArchivo;
    private int autor;
    private String ruta;
    private int idProyecto;

    public Archivo() {
    }

    public Archivo(String nombreArchivo, int autor, String ruta, int idProyecto) {
        this.nombreArchivo = nombreArchivo;
        this.autor = autor;
        this.ruta = ruta;
        this.idProyecto = idProyecto;
    }

    public int getIdArchivo() {
        return idArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public int getAutor() {
        return autor;
    }

    public String getRuta() {
        return ruta;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public void setIdArchivo(int idArchivo) {
        this.idArchivo = idArchivo;
    }

    public void setAutor(int autor) {
        this.autor = autor;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }
    
    
    
}
