package Plantillas;

public class Archivos implements java.io.Serializable{
    private String idArchivo;
    private String nombreArch;
    private String creador;
    private String idProyecto;
    private String ubicacion;

    public Archivos() {
    }

    public Archivos(String idArchivo, String nombreArch, String creador, String idProyecto, String ubicacion) {
        this.idArchivo = idArchivo;
        this.nombreArch = nombreArch;
        this.creador = creador;
        this.ubicacion = ubicacion;
        this.idProyecto = idProyecto;
    }
    
    

    public String getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(String idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String getNombreArch() {
        return nombreArch;
    }

    public void setNombreArch(String nombreArch) {
        this.nombreArch = nombreArch;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    
}
