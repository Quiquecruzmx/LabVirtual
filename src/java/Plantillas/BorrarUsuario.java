package Plantillas;

public class BorrarUsuario implements java.io.Serializable {
    private int idUser;
    private String username;
    
    public BorrarUsuario(){}
    
    public BorrarUsuario(int idUser){
        this.idUser = idUser;
    }
 
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }   

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    

}
