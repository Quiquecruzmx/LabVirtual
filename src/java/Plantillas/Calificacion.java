package Plantillas;


public class Calificacion {
    
    private int idCal;
    private Proyecto idProyecto;
    private String calificacion;
    private String observaciones;
    private String rubrica;
    private Usuario idAlumno;
    private Usuario idProfesor;
    private String nombrePractica;

    public int getIdCal() {
        return idCal;
    }

    public void setIdCal(int idCal) {
        this.idCal = idCal;
    }

    public Proyecto getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Proyecto idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public Usuario getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(Usuario idAlumno) {
        this.idAlumno = idAlumno;
    }

    public Usuario getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(Usuario idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getNombrePractica() {
        return nombrePractica;
    }

    public void setNombrePractica(String nombrePractica) {
        this.nombrePractica = nombrePractica;
    }

    public String getRubrica() {
        return rubrica;
    }

    public void setRubrica(String rubrica) {
        this.rubrica = rubrica;
    }
    
    
    
}
