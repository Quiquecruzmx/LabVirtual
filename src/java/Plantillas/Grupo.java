package Plantillas;

import java.util.HashSet;
import java.util.Set;

public class Grupo implements java.io.Serializable {
    private int idGrupo;
    private String nombreGrupo;
    private Usuario titular;
    private Set UsuariosSet = new HashSet(0);

    
    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public Usuario getTitular() {
        return titular;
    }

    public void setTitular(Usuario titular) {
        this.titular = titular;
    }

    public Set getUsuariosSet() {
        return UsuariosSet;
    }

    public void setUsuariosSet(Set UsuariosSet) {
        this.UsuariosSet = UsuariosSet;
    }
    
    
}
