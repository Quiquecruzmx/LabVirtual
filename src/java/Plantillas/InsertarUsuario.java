package Plantillas;

public class InsertarUsuario /*extends Usuario*/ implements java.io.Serializable{
    private int idUser;
    private String username;
    private String password;
    private String nombre;
    private String apePat;
    private String apeMat;
    private String correo;
    private String rol;

    public InsertarUsuario(int idUser, String username, String password, String nombre, String apePat, String apeMat, String correo, String rol) {
        this.idUser = idUser;
        this.username = username;
        this.password = password;
        this.nombre = nombre;
        this.apePat = apePat;
        this.apeMat = apeMat;
        this.correo = correo;
        this.rol = rol;
    }
    
    public InsertarUsuario(){
    
    }
    
    
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    
    public String getUsername() {//Sirven para obtener desde fuera (otras clases p.ej.) los valores obtenidos del mapeo
        return username;
    }

    public void setUsername(String username) { //se colocan a partir del archivo de mapeo (*.hbm.xml)
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApePat() {
        return apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }

    public String getApeMat() {
        return apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    
}
