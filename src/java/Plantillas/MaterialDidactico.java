package Plantillas;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class MaterialDidactico implements Serializable{
    private int idMaterial;
    private String nombreMaterial;
    private int autor;
    private String ubicacion;
    private Set<MaterialPractica> materialPractica = new HashSet<MaterialPractica>();

    public MaterialDidactico(String nombreMaterial, int autor, String ubicacion) {
        this.nombreMaterial = nombreMaterial;
        this.autor = autor;
        this.ubicacion = ubicacion;
    }

    public MaterialDidactico() {
    }
    
    public void addMaterialPractica(MaterialPractica mp){
        this.materialPractica.add(mp);
    }

    public Set<MaterialPractica> getMaterialPractica() {
        return materialPractica;
    }

    public void setMaterialPractica(Set<MaterialPractica> materialPractica) {
        this.materialPractica = materialPractica;
    }
    
    public String getNombreMaterial() {
        return nombreMaterial;
    }

    public int getIdMaterial() {
        return idMaterial;
    }


    public int getAutor() {
        return autor;
    }


    public String getUbicacion() {
        return ubicacion;
    }

    public void setNombreMaterial(String nombreMaterial) {
        this.nombreMaterial = nombreMaterial;
    }


    public void setAutor(int autor) {
        this.autor = autor;
    }


    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    public void setIdMaterial(int idMaterial) {
        this.idMaterial = idMaterial;
    }

}
