package Plantillas;

import java.io.Serializable;

public class MaterialPractica implements Serializable{
    private int idTabla;
    private MaterialDidactico materialdidactico;
    private Practica practica;
    private Grupo grupo;

    public MaterialPractica() {
        
    }

    public int getIdTabla() {
        return idTabla;
    }

    public void setIdTabla(int idTabla) {
        this.idTabla = idTabla;
    }

    public MaterialDidactico getMaterialdidactico() {
        return materialdidactico;
    }

    public void setMaterialdidactico(MaterialDidactico materialdidactico) {
        this.materialdidactico = materialdidactico;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }
}
