package Plantillas;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Practica implements Serializable{
    private int idPractica;
    private String titulo;
    private String objetivo;
    private String objetivosEsp;
    private String instrucciones;
    private String resultado;
    private String pdf;
    private Usuario idUsuario;
    private String rubricaRuta;
    private Set<MaterialPractica> materialPractica = new HashSet<MaterialPractica>();

    public Practica(int idPractica, String titulo, String objetivo, String instrucciones, String resultado) {
        this.idPractica = idPractica;
        this.titulo = titulo;
        this.objetivo = objetivo;
        this.instrucciones = instrucciones;
        this.resultado = resultado;
    }
    
    public Practica(String titulo, String objetivo, String instrucciones){
        this.titulo = titulo;
        this.objetivo = objetivo;
        this.instrucciones = instrucciones;
    }
    
    public Practica(String titulo){
        this.titulo = titulo;
    }

    public Practica() {
    }

    public Practica(String titulo, String objetivo, String instrucciones, String resultado) {
        this.titulo = titulo;
        this.objetivo = objetivo;
        this.instrucciones = instrucciones;
        this.resultado = resultado;
    }
    
    public void addMaterialPractica(MaterialPractica mp){
        this.materialPractica.add(mp);
    }

    public Set<MaterialPractica> getMaterialPractica() {
        return materialPractica;
    }

    public void setMaterialPractica(Set<MaterialPractica> materialPractica) {
        this.materialPractica = materialPractica;
    }

    public int getIdPractica() {
        return idPractica;
    }

    public void setIdPractica(int idPractica) {
        this.idPractica = idPractica;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getInstrucciones() {
        return instrucciones;
    }

    public void setInstrucciones(String instrucciones) {
        this.instrucciones = instrucciones;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getObjetivosEsp() {
        return objetivosEsp;
    }

    public void setObjetivosEsp(String objetivosEsp) {
        this.objetivosEsp = objetivosEsp;
    }

    public String getRubricaRuta() {
        return rubricaRuta;
    }

    public void setRubricaRuta(String rubricaRuta) {
        this.rubricaRuta = rubricaRuta;
    }
    
    

    
}
