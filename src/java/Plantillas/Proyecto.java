/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Plantillas;

import java.io.Serializable;

/**
 *
 * @author Enrique
 */
public class Proyecto implements Serializable{
    private int idProyecto;
    private String nombreProyecto;
    private int autor;

    public Proyecto() {
    }

    public Proyecto(String nombreProyecto, int autor) {
        this.nombreProyecto = nombreProyecto;
        this.autor = autor;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public int getAutor() {
        return autor;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public void setAutor(int autor) {
        this.autor = autor;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }
    
}
