package Plantillas;

import java.util.HashSet;
import java.util.Set;

public class Usuario implements java.io.Serializable {
    protected int id;
    protected String username;
    protected String password;
    protected String nombre;
    protected String apePat;
    protected String apeMat;
    protected String correo;
    protected String rol;
    private Set GruposSet = new HashSet(0);
    //protected Set AlumnosSet = new HashSet(0);
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getUsername() {//Sirven para obtener desde fuera (otras clases p.ej.) los valores obtenidos del mapeo
        return username;
    }

    public void setUsername(String username) { //se colocan a partir del archivo de mapeo (*.hbm.xml)
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApePat() {
        return apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }

    public String getApeMat() {
        return apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Set getGruposSet() {
        return GruposSet;
    }

    public void setGruposSet(Set GruposSet) {
        this.GruposSet = GruposSet;
    }
    
    
    
}
