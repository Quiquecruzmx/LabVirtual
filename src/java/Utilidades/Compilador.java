package Utilidades;

import Plantillas.Archivo;
import Plantillas.HibernateUtil;
import Plantillas.Proyecto;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Compilador {
    private final String rutaGlobal = "C:\\Ficheros"; //Enrique
    //private String rutaGlobal = "C:\\Users\\admin\\Desktop\\Ficheros"; //Sergio
    private String rutaCompleta = null;//
    private String rutaRecibidos = null;

    public String getRutaCompleta() {
        return rutaGlobal;
    }

    public String crearFicheros(String nombreCarpetaUsuario, String [] nombres,
            String [] extensiones, String [] codigos, String nombreProyecto){
        
        String ruta = rutaGlobal+"\\"+nombreCarpetaUsuario+"\\"+nombreProyecto+"\\";
        rutaCompleta = ruta;
        File dir = new File(ruta), archivo;
        int i ;
        
        //make sure directory exists
    	if(!dir.exists()){
           dir.mkdir();

 
        }else{
 
           try{
        	   
               borrarDir(dir);
        	
           }catch(IOException e){
               e.printStackTrace();
               return "Error al borrar ficheros anteriores";
           }
        }        

        dir.mkdir();

            
         try{   
            for(i=0; i<nombres.length; i++){
                archivo = new File(ruta+nombres[i]+extensiones[i]);
                BufferedWriter bw;
            
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write(codigos[i]);
                bw.close();

            }
         }
          catch (IOException ex) {
              
                Logger.getLogger(Compilador.class.getName()).log(Level.SEVERE, null, ex);
                
                return "Error al generar los archivos";
          }
         
            return "";
        
        
    }
    
    public String compilar(String nombreCarpeta, String [] nombres,
            String [] extensiones, String nombreEjecutable){
        int i;
        String nombresProgramas = "";
        String salida = "";
        String cad;

        for(i = 0; i<nombres.length; i++){
            nombresProgramas+=" \""+(nombres[i]+extensiones[i])+"\" ";
        }
               
        String comando = "cmd /c cd "+rutaGlobal+"\\"
                +nombreCarpeta+"\\"+nombreEjecutable+"\\ & g++"+nombresProgramas+"-o \""+nombreEjecutable+"\"";
        try {
            Process proc = Runtime.getRuntime().exec(comando);
            InputStreamReader entrada = new InputStreamReader(proc.getErrorStream());
            BufferedReader stdInput = new BufferedReader(entrada);
            
            while((cad = stdInput.readLine())!= null){
                salida+=(cad+"<br>");
            }

            if(salida.compareTo("")==0){
                entrada.close();
                stdInput.close();
                return null;

            }else{
                entrada.close();
                stdInput.close();
                System.out.println("Los errores son: "+salida);
                return salida;
            }
            
            
        } catch (IOException ex) {
            Logger.getLogger(Compilador.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("No se pudo compilar");
            return "Problema interno, contactar al administrador";
        }

    }
    
    public String generarParaEvaluar(String profesor, String alumnoAutor, String [] nombres,
            String [] extensiones, String [] codigos, String nombreProyecto){
        
        String ruta = rutaGlobal+"\\"+profesor+"\\@Recibidos\\"+nombreProyecto+"-"+alumnoAutor+"\\";
        rutaCompleta = ruta;
        File dir = new File(ruta), archivo;
        int i ;
        
        //make sure directory exists
    	if(!dir.exists()){
            dir.mkdir();
 
        }else{
 
           try{
        	   
               borrarDir(dir);
        	
           }catch(IOException e){
               e.printStackTrace();
               return "Error al borrar ficheros anteriores";
           }
        }        

        dir.mkdir();

            
         try{   
            for(i=0; i<nombres.length; i++){
                archivo = new File(ruta+nombres[i]+extensiones[i]);
                BufferedWriter bw;
            
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write(codigos[i]);
                bw.close();

            }
         }
          catch (IOException ex) {
              
                Logger.getLogger(Compilador.class.getName()).log(Level.SEVERE, null, ex);
                
                return "Error al generar los archivos recibidos del alumno";
          }
         
            return "";
        
        
    }

    public String compilarParaEvaluar(String profesor, String alumnoAutor, String [] nombres,
            String [] extensiones, String nombreEjecutable){
        int i;
        String nombresProgramas = "";
        String salida = "";
        String cad;

        for(i = 0; i<nombres.length; i++){
            nombresProgramas+=" \""+(nombres[i]+extensiones[i])+"\" ";
        }      
        String comando = "cmd /c cd "+rutaGlobal+"/"
                +profesor+"/@Recibidos/"+nombreEjecutable+"-"+alumnoAutor+"/ & g++"+nombresProgramas+"-o \""+nombreEjecutable+"\"";
        try {
            Process proc = Runtime.getRuntime().exec(comando);
            InputStreamReader entrada = new InputStreamReader(proc.getErrorStream());
            BufferedReader stdInput = new BufferedReader(entrada);
            
            while((cad = stdInput.readLine())!= null){
                salida+=(cad+"<br>");
            }

            if(salida.compareTo("")==0){
                entrada.close();
                stdInput.close();
                return null;

            }else{
                entrada.close();
                stdInput.close();
                System.out.println("Errores son: "+salida);
                return salida;
            }
            
            
        } catch (IOException ex) {
            Logger.getLogger(Compilador.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("No se pudo compilar");
            return "Problema interno, contactar al administrador";
        }

    }
    
    public void destruirFicheros(){
        
    }
    
    public void guardarEnBase(int idAutor, String nombreProyecto, String[] nombres, String[] extensiones, String[] codigos, String ruta){

        Session hbs = HibernateUtil.getSessionFactory().openSession();
        Transaction t = hbs.beginTransaction();
        //Si el proyecto es nuevo lo guarda en la base, si no, lo ignora
        if(!proyectoExistente(hbs, nombreProyecto, idAutor)){
            
            Proyecto p = new Proyecto(nombreProyecto, idAutor);
            hbs.save(p);
            //hbs.flush();
            //t.commit();
            System.out.println("El proyecto "+nombreProyecto+" ha sido guardado en la base");
        }          
        
        int idProyecto =  (int) hbs.createQuery("SELECT idProyecto FROM Proyecto WHERE nombreProyecto='"+nombreProyecto+"'"
                + " AND autor = "+idAutor).uniqueResult();
        //List <Archivo> ars = 
        List <Archivo> archivos = hbs.createQuery("FROM Archivo WHERE idProyecto = "+idProyecto).list(); //Archivos de base de datos       
        List nombresExt = union(nombres, extensiones);//Nombres de archivos en el sistema
        Archivo arch;
        //t.begin();
        
        if(archivos.isEmpty()){ //No existe nada guardado en la base aún
            for(int i=0; i< nombresExt.size(); i++){
                arch = new Archivo(nombresExt.get(i).toString(), idAutor, ruta, idProyecto);
                System.out.println("Archivo["+i+"] a guardar");
                hbs.saveOrUpdate(arch);
            }

        }
        
        else if(nombresExt.size()<archivos.size()){ //Si el numero de nombres obtenidos del programa es menor a los que hay en la base
            for(int i=0; i<nombresExt.size(); i++){
                arch = archivos.get(i);
                arch.setAutor(idAutor);
                arch.setIdProyecto(idProyecto);
                arch.setNombreArchivo(nombresExt.get(i).toString());
                arch.setRuta(ruta);
                
                hbs.update(arch);
            }
            
            for(int i=nombresExt.size(); i<archivos.size(); i++){
                arch = archivos.get(i);
                
                hbs.delete(arch);
            }
            
        }
        
        else { //Si el numero de nombres obtenidos del programa es mayor o igual a los que hay en la base
            
            for(int i=0; i<archivos.size(); i++){
                System.err.println("Indice["+i+"]");
               
                arch = archivos.get(i);

                arch.setAutor(idAutor);
                arch.setIdProyecto(idProyecto);
                arch.setNombreArchivo(nombresExt.get(i).toString());
                arch.setRuta(ruta);
                
                hbs.saveOrUpdate(arch);
                
            }
//            

//                arch = new Archivo();
                for(int i = archivos.size(); i<nombresExt.size(); i++){
                    arch = new Archivo();
                    arch.setAutor(idAutor);
                    arch.setIdProyecto(idProyecto);
                    arch.setNombreArchivo(nombresExt.get(i).toString());
                    arch.setRuta(ruta);
                    hbs.save(arch);
                }
            //}
            
        }
        
        t.commit();
        hbs.close();      
    }
    
    public void generarCarpetaUsuario(String nombreUsuario){
        String ruta = rutaGlobal+"\\"+nombreUsuario+"\\";
        String rutaReciv = rutaGlobal+"\\"+nombreUsuario+"\\@Recibidos\\";
        File dir = new File(ruta);
        File recibidos = new File(rutaReciv);
        
        if(!dir.exists()){
                dir.mkdir();
                 
        }
        else{
            System.out.println("El directorio ya existe");
        }
        
        if(!recibidos.exists()){
                recibidos.mkdir();
                 
        }
        else{
            System.out.println("El directorio con @ ya existe");
        }

    }
    
    public void modificarCarpetaUsuario(String nombreViejo, String nombreNuevo){
        String rutaVieja = rutaGlobal+"\\"+nombreViejo+"\\";
        String rutaNueva = rutaGlobal+"\\"+nombreNuevo+"\\";
        File dirViejo = new File(rutaVieja);
        File dirNuevo = new File(rutaNueva);
        
        if(dirViejo.canRead()) //Checar esto, no funciona si alguien esta dentro del directorio
            dirViejo.renameTo(dirNuevo);
        else
            System.out.println("No se puede leer, alguien lo tiene abierto");
        

    }
    public void destruirCarpetaUsuario(String nombreUsuario){
        String ruta = rutaGlobal+"\\"+nombreUsuario+"\\";
        File dir = new File(ruta);
        
        
        if(!dir.exists()){
            System.out.println("El directorio no existe");
                
        }
        else{
            try {

                FileUtils.deleteDirectory(dir);
            } catch (IOException ex) {
                System.out.println("El directorio no se pudo eliminar");
            }
        }
    }
    //Verifica si existe un proyecto del autor con el mismo nombre
    public boolean proyectoExistente(Session sesion, String proyecto, int autor){
        Query query = sesion.createQuery("SELECT nombreProyecto FROM Proyecto WHERE autor='"+autor+"'");
        List lista = query.list();
        Iterator it = lista.iterator();
        while(it.hasNext()){
            if(it.next().equals(proyecto)){
                return true;
            }
        }
        return false;
    }
    
    //Regresa el arreglo con los nombres de los archivos guardados en la base
    private String[] archivosExistentes(Session sesion, int idProyecto){
        Query query = sesion.createQuery("SELECT nombreArchivo FROM Archivo WHERE idProyecto='"+idProyecto+"'");
        System.out.println("La lista de archivos recuperada: "+query.list());
        List lista = query.list();
        Iterator it = lista.iterator();
        String nombres[] =  new String[lista.size()];
        int i = 0;
        while(it.hasNext()){
            nombres[i] = (String) it.next();
            i++;
        }
        return nombres;
    }
    //A-B
    private ArrayList diferencia(String A[], String B[]){
        //Transformando los arreglos en ArrayList
        ArrayList <String> listaFinal = new ArrayList <String>();
        List <String> lista1 = new ArrayList <String>();
        List <String> lista2 = new ArrayList <String>();
        lista1 = Arrays.asList(A);
        lista2 = Arrays.asList(B);

        for(String elemento: lista2){
            if(!lista1.contains(elemento)){
                listaFinal.add(elemento);
            }
        }
        return listaFinal;
    }
    
    private  List union(String nombresArchivo[], String extensionesArchivo[]){
        String nuevo[] = new String[nombresArchivo.length];
        for(int i = 0; i<nombresArchivo.length; i++){
            nuevo[i] = nombresArchivo[i] + extensionesArchivo[i];
        }
        
        List nombresExt = Arrays.asList(nuevo);
        return nombresExt;
    }
    
    public static void borrarDir(File dir) throws IOException{
 
    	if(dir.isDirectory()){
 
            //directory is empty, then delete it
            if(dir.list().length==0){

               dir.delete();
               System.out.println("Directorio es borrado : " 
                                             + dir.getAbsolutePath());

            }else{

               //list all the directory contents
               String files[] = dir.list();

               for (String temp : files) {
                  //construct the file structure
                  File fileDelete = new File(dir, temp);

                  //recursive delete
                 borrarDir(fileDelete);
               }

               //check the directory again, if empty then delete it
                if(dir.list().length==0){
                    dir.delete();
                    System.out.println("Directorio es borrado : "+ dir.getAbsolutePath());
                }
            }
    		
    	}else{
            //if file, then delete it
            dir.delete();
            System.out.println("Archivo borrado : " + dir.getAbsolutePath());
    	}
    }
    
    public void copiarCarpetaProyecto(String alumno, String profesor, String nombreProyecto, String nombrePractica){	
        
    	File folderAlumno = new File(rutaGlobal+"\\"+alumno+"\\"+nombreProyecto);
    	File folderProfesor = new File(rutaGlobal+"\\"+profesor+"\\@Recibidos\\"+(nombreProyecto+"-"+alumno+"-"+nombrePractica).trim());
    	
    	//make sure source exists
    	if(!folderAlumno.exists()){

           System.out.println("No existe el folder");
           //just exit
           //System.exit(0);

        }else{

           try{
        	copyFolder(folderAlumno,folderProfesor);
            }catch(IOException e){
        	e.printStackTrace();
        	//error, just exit
                //System.exit(0);
           }
        }
    	
    	System.out.println("Hecho");
    }
     
        public static void copyFolder(File folderAlumno, File folderProfesor)
    	throws IOException{
    	
    	if(folderAlumno.isDirectory()){
    		
    		//if directory not exists, create it
    		if(!folderProfesor.exists()){
    		   folderProfesor.mkdir();
    		   System.out.println("Directorio copiado desde " 
                              + folderAlumno + "  hacia " + folderProfesor);
    		}
    		
    		//list all the directory contents
    		String files[] = folderAlumno.list();
    		
    		for (String file : files) {
                    System.out.println("Entrada");
    		   //construct the src and dest file structure
    		   File srcFile = new File(folderAlumno, file);
    		   File destFile = new File(folderProfesor, file);
    		   //recursive copy
    		   copyFolder(srcFile,destFile);
    		}
    	   
    	}else{
    		//if file, then copy it
    		//Use bytes stream to support all file types
    		InputStream in = new FileInputStream(folderAlumno);
    	        OutputStream out = new FileOutputStream(folderProfesor); 
    	                     
    	        byte[] buffer = new byte[1024];
    	    
    	        int length;
    	        //copy the file content in bytes 
    	        while ((length = in.read(buffer)) > 0){
    	    	   out.write(buffer, 0, length);
    	        }
 
    	        in.close();
    	        out.close();
    	        System.out.println("Archivo copiado desde " + folderAlumno + " to " + folderProfesor);
    	}
    }
        
}
