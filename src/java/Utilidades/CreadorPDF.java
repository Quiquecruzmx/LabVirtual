package Utilidades;

import java.io.File;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
public class CreadorPDF {
    private static final Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 26, Font.BOLDITALIC);
    private static final Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);
    
    private static final Font categoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static final Font subcategoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static final Font blueFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);    
    private static final Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    
    public void crearDocumento(File documentoPDF, String titulo, String autor,
            String tituloPractica, String objetivo, String objetivosEsp, String instrucciones, String resultado){
        try{
            Document document = new Document();
            try{
                PdfWriter.getInstance(document, new FileOutputStream(documentoPDF));
            }catch(FileNotFoundException ex){
                System.out.println("No se encontró el fichero para generar el archivo pdf");
            }
            document.open();
            document.addTitle(titulo);
            document.addAuthor(autor);
            document.addCreator(autor);
            Chunk chunk = new Chunk(tituloPractica, chapterFont);
            Chapter chapter = new Chapter(new Paragraph(chunk), 1);
            chapter.setNumberDepth(0);
            chapter.add(new Paragraph("Objetivo General:", categoryFont));
            chapter.add(new Paragraph(objetivo, paragraphFont));
            chapter.add(new Paragraph("Objetivos Especificos:", categoryFont));
            chapter.add(new Paragraph(objetivosEsp, paragraphFont));
            chapter.add(new Paragraph("Instrucciones:", categoryFont));
            chapter.add(new Paragraph(instrucciones, paragraphFont));
            chapter.add(new Paragraph("Resultado", categoryFont));
            chapter.add(new Paragraph("La siguiente captura de pantalla muestra la salida esperada", paragraphFont));
            Image imagen;
            
            try{
                imagen = Image.getInstance(resultado);
                imagen.scaleAbsolute(350f, 200f);
                //imagen.setAbsolutePosition(20, 150);
                chapter.add(imagen);
            }catch(BadElementException | IOException bad){
                bad.printStackTrace();
                System.out.println("Error en el resultado");
            }
            
            document.add(chapter);
            document.close();
            System.out.println("El documento se ha creado!");
        }catch(DocumentException de){
            System.out.println("Error al intentar generar el fichero");
        }
        
    }
    
}
