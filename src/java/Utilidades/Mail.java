package Utilidades;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Mail {
    
    public int envioEmail(String correo, String nombre, String pass){
            final String username = "labcpprecover@gmail.com";
            final String password = "labcpprecover";

            Properties props = new Properties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getInstance(props,
              new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
              });

            try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("labcpprecover@gmail.com"));//De donde se va a enviar
                message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(correo)); //Quien es el que va a recibir el correo
                message.setSubject("Recuperación");
                message.setText("Hola estimado "+nombre
                    + "\n\n Su nombre de usuario es: "+ nombre+"\nPassword: "+ pass
                    +"\nSaludos");

                Transport.send(message);

                System.out.println("HEcho envio email");
                return 1;

            } catch (MessagingException e) {
                e.printStackTrace();
                return 0;

            }
            //finally{return 0;}
    
    }
    
    public boolean envioPracticaMail(){
        
        return true;
    }

}
