
package Utilidades;

public class Proyectos {
    
    int id;
    private String nombre;
    private String codigo;
    private String extension;

    public Proyectos() {
    }

    public Proyectos(int id, String nombre, String codigo, String extension) {
        this.id = id;
        this.nombre = nombre;
        this.codigo = codigo;
        this.extension = extension;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
    
}
