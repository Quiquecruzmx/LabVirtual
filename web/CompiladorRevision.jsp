<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compilador</title>
        <link href="css/alumnoCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/compilador.css" type="text/css" rel="stylesheet">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link rel="stylesheet" href="https://codemirror.net/lib/codemirror.css">
        <link rel="stylesheet" href="https://codemirror.net/addon/hint/show-hint.css">
        <script src="js/jquery-3.2.1.js" type="text/javascript"></script>
        <script src="https://code.jquery.com/ui/1.8.23/jquery-ui.js"></script>
        <script src="https://codemirror.net/lib/codemirror.js"></script>
        <script src="https://codemirror.net/addon/edit/matchbrackets.js"></script>
        <script src="https://codemirror.net/addon/hint/show-hint.js"></script>
        <script src="https://codemirror.net/mode/clike/clike.js"></script>
        <script type="text/javascript" src="js/compiladorFunciones.js"></script>
        <script src="js/autocompletado.js" type="text/javascript"></script>
        <style>.CodeMirror {border: 2px inset #dee;}</style>
        
    </head>
    <body onload="rec()">
        
        <div id="idPopupForm" class="popupForm">
            <div class="contenedorForm">
                <div class="headerForm">
                    <span class="close">&times;</span>
                    <h2>Rúbrica de evaluación</h2>
                </div>
               <div class="cuerpoForm">
           <table class="tabla">
                <thead>
                    <tr class="tabRow2">
                    <th class="tabHeader">
                        Aspecto a evaluar
                        <input type="number" id="califUser" value="0" contenteditable="false" readonly="true">/<input type="number" id="califTotal" value="0" contenteditable="false" readonly="true">
                    </th>
                    <th class="tabHeader">
                        Nivel avanzado<br>
                        <input type="number" max="10" min="0" value="10" id="lvlavanzado" contenteditable="false" readonly="true">
                    </th>
                    <th class="tabHeader">
                        Nivel intermedio<br>
                        <input type="number" max="10" min="0" value="5" id="lvlinter" contenteditable="false">
                    </th>
                    <th class="tabHeader">
                        Nivel Básico<br>
                        <input type="number" max="10" min="0" value="0" id="lvlbasico" contenteditable="false">
                    </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar">
                            Resolución<br>
                            <input type="number" max="10" min="0" value="2" class="check" contenteditable="false">
                        </td>
                        <td class="tabCel avanzado">
                            <li>Utiliza las instrucciones y algoritmos más adecuados para resolver la práctica</li>
                            <li>El funcionamiento es completo</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel inter">
                            <li>Utiliza instrucciones y algoritmos que no son acordes para resolver el ejercicio</li>
                            <li>El funcionamiento tiene fallos importantes</li>
                            <input type="hidden" class="entrada">
                            
                        </td>
                        <td class="tabCel basico">
                            <li>Utiliza algoritmos que no resuelven la práctica</li>
                            <li>La práctica no funciona</li>
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar">
                            Identificación de variables y componentes<br>
                            <input type="number" max="10" min="0" value="2" class="check" contenteditable="false">
                        </td>
                        <td class="tabCel avanzado">
                            <li>Nombra correctamente todas las variables, clases y métodos</li>
                            <li>Hace uso adecuado de la herencia y polimorfismo</li>
                            <input type="hidden" class="entrada">
                            
                            
                        </td>
                        <td class="tabCel inter">	
                            <li>Nombra correctamente solo algunas variables y clases</li>
                            <li>Hace muy poco uso de la herencia y polimorfismo</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel basico">
                            <li>No nombra adecuadamente ningún componente ni variable</li>
                            <li>No hace uso de herencia y polimorfismo</li>
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar">
                            Documentación interna<br>
                            <input type="number" max="10" min="0" value="2" class="check" contenteditable="false">
                        </td>
                        <td class="tabCel avanzado">
                            <li>Aporta una documentación en el código para estructurar y entender claramente</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel inter">	
                            <li>La documentación aportada es la justa para comprender el código</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel basico">
                            <li>No aporta documentación</li>
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar">
                            Presentación de resultados<br>
                            <input type="number" max="10" min="0" value="2" class="check" contenteditable="false">
                        </td>
                        <td class="tabCel avanzado">
                            <li>Los resultados de la práctica son iguales a las imágenes del docente</li>
                            <li>No se muestran errores en la salida</li>
                            <input type="hidden" class="entrada">
                           
                        </td>
                        <td class="tabCel inter">	
                            <li>Los resultados de la práctica son similares a las imágenes del docente</li>
                            <li>La salida muestra algunos errores</li>
                            <input type="hidden" class="entrada">
                            
                        </td>
                        <td class="tabCel basico">
                            <li>Los resultados difieren a los del docente</li>
                            <li>La salida está llena de errores</li>
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar" contenteditable="true">
                            Actitudes<br>
                            <input type="number" max="10" min="0" value="2" class="check" contenteditable="false">
                        </td>
                        <td class="tabCel avanzado" >
                            <li>Entrega en la fecha previa establecida por el docente</li>
                            <li>Muestra perseverancia al aprovechar los errores marcados en prácticas previas para mejorar su trabajo</li>
                            <li>Organiza el código de manera legible</li>    
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel inter">	
                            <li>Entrega en la fecha establecida por el docente</li>
                            <li>Muestra perseverancia al aprovechar los errores marcados en prácticas previas para mejorar su trabajo</li>
                            <li>La organización del código es legible sólo en algunas partes</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel basico">
                            <li>Entrega en una fecha posterior a la establecida</li>
                            <li>El código es ilegible</li> 
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                </tbody>
            </table>

            <input type="hidden" id="suma">

            </div>
            </div>
        </div>
        
                    <script>
    var sum = 0; //Suma para obtener la calificacion total
    var sumAlumno = 0;
    var porcentaje;
    
    
    $(".tabCel2").children('.check').each(function(){
        sum += parseFloat($(this).val());
    });
    $("#califTotal").val(sum);
    sum=0;
    
    $(document).on("change", ".tabCel2", function() {
        sum = 0;
        $(".tabCel2").children('.check').each(function(){
            sum += parseFloat($(this).val());
        });
        $("#califTotal").val(sum);

    });
    
    
    $(document).on("change", ".tabCel2", function() {
        var sum = 0;
        $(".tabCel2").children('.check').each(function(){
            sum += +$(this).val();
        });
        $("#califTotal").val(sum);

    });
    
    
    
    
    $('.tabCel').click(function() {
        $(this).parent().find('.tabCel').css('background-color', '#ffffff');
        $(this).css('background-color', '#ffe8a1');
        
        $(this).parent().children().children('.entrada').each(function(){
            $(this).val(0);
            //alert($(this).val());
        });
        
        
        
        
        if($(this).hasClass('avanzado')){ //Si se clickea en una celda con clase = avanzado, se toma ese porcentaje como referencia
           
            porcentaje = (parseInt($('#lvlavanzado').val()) * parseInt($(this).parent().children('.remarcar').children('.check').val())) / 10;
            
            $(this).children('.entrada').val(porcentaje);
        }
        
        else if($(this).hasClass('inter')){ //Si se clickea en una celda con clase = inter, se toma ese porcentaje como referencia
            porcentaje = (parseInt($('#lvlinter').val()) * parseInt($(this).parent().children('.remarcar').children('.check').val())) / 10;
            $(this).val(porcentaje);
            $(this).children('.entrada').val(porcentaje);
        }
        
        else{
            porcentaje = (parseInt($('#lvlbasico').val()) * parseInt($(this).parent().children('.remarcar').children('.check').val())) / 10;
            $(this).val(porcentaje); //Si se clickea en una celda con clase = basico, se toma ese porcentaje como referencia
            $(this).children('.entrada').val(porcentaje);
            //sumAlumno += parseFloat($(this).val());
        }
        
        $(this).parent().parent().children().children().children('.entrada').each(function(){
            
            if(!isNaN(parseFloat($(this).val()))){
            
                sumAlumno+=parseFloat($(this).val());
                //alert("Suma acum:"+ sumAlumno);
            }
            //alert("final: "+$(this).val());
        });
        
        
        $("#califUser").val(sumAlumno);
        
        sumAlumno = 0;
      });

            </script>
        
        
        <div id="idPopupForm2" class="popupForm">
            <div class="contenedorForm">
                <div class="headerForm">
                    <span class="close2">&times;</span>
                    <h2>Lista de archivos, seleccione los que desee descargar</h2></div>
                <div class="cuerpoForm">
                    <s:form action="DescargarEvaluador">
                    <table class="tabla">
                        <thead>
                        <tr class="tabRow">
                            <th class="tabHeader">Seleccionar <br></th>
                            <th class="tabHeader">NombreArchivo</th>
                        </tr>
                       </thead>
                       <tbody>
                           <s:iterator value="proyectos" status="stat">   
                            <tr class="tabRow">     
                                <td class="tabCel"><s:checkbox  name="archivosDescarga" fieldValue="%{nombre+''+extension}" theme="simple"/></td>
                                <td class="tabCel"><s:property value="nombre"/><s:property value="extension"/></td>
                            </tr>

                        </s:iterator>
                        </tbody>

                    </table>
                        <s:hidden name="nombreProyecto" value="%{nombreProyecto}"/>
                        <s:hidden name="nombreAutor" value="%{nombreAutor}"/>
                    <s:submit value="Descargar ficheros"/>
                    </s:form>
                </div>
            </div>
        </div>
        
        
        <s:url id="fileDownload" action="DescargarEvaluador" >
            <s:param name="nombreProyecto" value="%{nombreProyecto}"></s:param>
            <s:param name="nombreAutor" value="%{nombreAutor}"></s:param>
        </s:url>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
           <a class="navbar-brand" href="ProfesorMain"><s:property value = "#session.USER.username "/></a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
           </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');" href = "ProfesorMain">Inicio</a></li>

                    <li class="nav-item"><s:a cssClass="nav-link" href="%{fileDownload}">Descargar .exe</s:a></li>
                    <li class="nav-item"><s:a cssClass="nav-link" href="#" id="despliegaForm2">Descargar Ficheros</s:a></li>


                </ul>
                <a class="btn btn-outline-warning my-2 my-sm-0 float-right" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');" href="Logout">Salir</a>
            </div>    
        </nav>
        <div class="modal-body row">
        <div id="tabGral">
            
            <li id="0" class="tablinks">
                <a onclick="abrirEditor(this.parentNode.id)"><s:property value="proyectos[0].nombre"/><s:property value="proyectos[0].extension"/></a>
            
            </li>
            <div hidden="true">
                <li class="nav-item dropdown">
                <a  class="nav-link dropdown-toggle agrega" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Agregar archivo</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" >

                        <form onsubmit="return false;">
                            Nombre de Archivo: <input autocomplete="off" class="form-control input-sm" type="text" id="nuevoArchivo"/>
                            <s:select id = "nuevaExtension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                            <button class="btn btn-primary" onclick="nuevoArch()">Crear</button>
                        </form>

                    </div>
                </li>
            </div>
            
            
        </div>

        <div class="col-md-8">
        <form action="setCalificacion" >
            <s:hidden value="%{idP}" name="idProy"></s:hidden>
            <s:hidden name="nombrePractica" value="%{#session.nombrePractica}"></s:hidden>
            <s:textfield name="calificacion" label="Calificacion" value="%{#session.calif}" id="despliegaForm" readonly="true"/>
            <s:label>Observaciones:</s:label>
            <s:textarea  id="htmlRubrica" cssStyle="resize: none" rows="12" cols="100"/>
        </form>
        <button id="mapper" onclick="mapperHTML()">Mapear como rubrica</button>
        </div>
                        
    <div class="log">
        <h4 style="color:white">Errores:</h4>   
            <s:actionerror escape="false" />
            <s:actionmessage escape="false" />
    </div>    
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script>

        var modal = document.getElementById('idPopupForm');
        var btn = document.getElementById("despliegaForm");
        var span = document.getElementsByClassName("close")[0];

        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
            if(document.getElementById("califUser").value !=0){
                btn.value = parseFloat(document.getElementById("califUser").value) / parseFloat(document.getElementById("califTotal").value) * 10;
            }   
           
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
                if(document.getElementById("califUser").value !=0){
                
                    btn.value = parseFloat(document.getElementById("califUser").value) / parseFloat(document.getElementById("califTotal").value) * 10 ;
                }
                 var rub = document.getElementById("htmlRubrica");
                 rub.value = modal.innerHTML;
            }
        }
        
        function mapperHTML(){
            var html = document.getElementById("htmlRubrica");
            var res = html.value.replace(/contenteditable="true"/g, "contenteditable=\"false\"");
            alert(res);
            document.write(res);
        }
    </script> 
     <script src="js/popper.min.js"></script>
     <script src="js/bootstrap-4.0.0.js"></script>
    </body>
</html>
