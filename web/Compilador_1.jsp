<%--
    Document   : Compilador
    Created on : 15/02/2018, 01:48:41 PM
    Author     : Enrique
--%>

<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compilador</title>
        
        <script src="js/jquery-3.2.1.js" type="text/javascript"></script>
        <link href="css/compilador.css" type="text/css" rel="stylesheet">
        <script src="https://code.jquery.com/ui/1.8.23/jquery-ui.js"></script>
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link rel="stylesheet" href="https://codemirror.net/lib/codemirror.css">
        <script src="https://codemirror.net/lib/codemirror.js"></script>
        <script src="https://codemirror.net/addon/edit/matchbrackets.js"></script>
        <link rel="stylesheet" href="https://codemirror.net/addon/hint/show-hint.css">
        <script src="https://codemirror.net/addon/hint/show-hint.js"></script>
        <script src="https://codemirror.net/mode/clike/clike.js"></script>
        <script type="text/javascript" src="js/compiladorFunciones.js"></script>
        <script src="js/autocompletado.js" type="text/javascript"></script>
        <style>.CodeMirror {border: 2px inset #dee;}</style>
        
    </head>
    <body onload="rec()">
        
        <h1>Compilador C++ para <s:property value="%{#session.USER.username}"/></h1>
        <s:url id="fileDownload" action="DescargarArchivo" >
            <s:param name="nombreProyecto" value="%{nombreProyecto}"></s:param>
        </s:url>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
           <a class="navbar-brand" href="#"><s:property value = "#session.USER.username "/></a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
           </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');" href = "AlumnoMain">Inicio</a></li>

                    <li class="nav-item"><s:a class="nav-link" href="%{fileDownload}">Descargar</s:a></li>


                </ul>
                <a class="btn btn-outline-warning my-2 my-sm-0 float-right" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');" href="Logout">Salir</a>
            </div>    
        </nav>
        
        <s:actionerror  />
        <s:actionmessage />
        <div class="modal-body row">
        <div id="tabGral" class="col-md-3">
            
            <li id="0" class="tablinks">
                <a onclick="abrirEditor(this.parentNode.id)"><s:property value="proyectos[0].nombre"/><s:property value="proyectos[0].extension"/></a>
                <a class="eliminar" onclick ="borrar(this.parentNode.id)">-</a>
            </li>
            <li class="nav-item dropdown">
                <a  class="nav-link dropdown-toggle agrega" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Agregar archivo</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown" >
                    
                    <form onsubmit="return false;">
                        Nombre de Archivo: <input autocomplete="off" class="form-control input-sm" type="text" id="nuevoArchivo"/>
                        <s:select id = "nuevaExtension" list="{'.c', '.cpp', '.h'}" label = "Extensión"></s:select>

                        <button class="btn btn-primary" onclick="nuevoArch()">Crear</button>
                    </form>
					
                </div>
            </li>
            
            
        </div>
       
        <div class="col-md-9">
           
            <s:form action="codeAlumno" id="formulario" autocomplete="off">
                <s:textfield readonly="true" name="nombreProyecto" id="proyecto" label="Nombre Proyecto"></s:textfield>
                <s:hidden name="tam" id="tam" value="%{proyectos.size}"/>
                <s:textfield name="proyectos[0].nombre"  id="nombre0" readonly="true" cssClass="cont" label="Archivo"/>
                <s:textfield name = "proyectos[0].extension" id="ext0" readonly="true" cssClass="cont" cssStyle="width: 40px;"></s:textfield>     
                
                <s:textarea  id="edit0" name="proyectos[0].codigo" cssClass="cont"/>
                
                
                
                <s:hidden id="accion" name="accion" value="compilar"/>
                <s:submit value="COMPILAR" onclick="document.getElementById('accion').value='compilar'"/>
                <s:submit value="GUARDAR" onclick="document.getElementById('accion').value='guardar'"/> 
                
            </s:form>
            
        </div>
        
        <div hidden="true">
            <s:iterator value="proyectos" status="stat">
            <s:textfield id = "proyectos[%{#stat.index}].nombre" name="proyectos[%{#stat.index}].nombre"/>
            <s:textfield id = "proyectos[%{#stat.index}].extension" name="proyectos[%{#stat.index}].extension"/>
            <s:textarea id = "proyectos[%{#stat.index}].codigo" name="proyectos[%{#stat.index}].codigo"/>
        </s:iterator>
        </div>
    </div>

     <!-- Include all compiled plugins (below), or include individual files as needed --> 
     <script src="js/popper.min.js"></script>
     <script src="js/bootstrap-4.0.0.js"></script>
    </body>
</html>
