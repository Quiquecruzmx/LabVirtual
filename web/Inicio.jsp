<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Inicio de sesión</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
    <link href="css/inicio.css" rel="stylesheet">
    <link href="css/alumnoCss.css" type="text/css" rel="stylesheet"/>
    <link rel="icon" href="images/logo_lab_cpp.png">

  </head>
  <body>

    <div class="jumbotron jumbotron-fluid text-center">
       <div class="images-bar"> 
           <img src="images/ipn-logo.png" width="140" height="140" class="ipn">
           <img src="images/escom_logo.png" width="140" height="140" class="escom">
       </div>
        <h4 class="display-3"><strong>Laboratorio Virtual de Programación C++</strong></h4>
       <p class="lead">Trabajo Terminal II</p>
       <hr class="my-4">
       <img src="images/logo_lab_cpp.png" style="margin-left: 300px ; position: absolute"> 
       <h3>Inicio de sesión</h3>
        <s:form action="Login">

            <p class="lead"><s:textfield cssClass="form-control input-sm" name="username" label="Usuario" /></p>
            <p class="lead"><s:password cssClass="form-control input-sm" name="password" label="Password" /></p>
            <p class="lead">
                <s:submit value="ENTRAR" cssClass="btn btn-primary btn-lg"/>
            </p>
        </s:form>
       <s:actionerror cssStyle="color: #ff0000"/>
       <s:actionmessage cssStyle="color: #00aa00"/>

    </div>
    <div class="banner">
        <h5><a href="#" id="despliegaForm">¿Olvidaste nombre y/o contraseña?</a></h5>
    </div>
       
       
   <s:div id="idPopupForm" cssClass="popupForm">   
        <s:div cssClass="contenedorForm" >
            <s:div cssClass="headerForm">
                <span class="close">&times;</span>
                <h2>Recuperar contraseña</h2>
            </s:div>
            <s:div cssClass="cuerpoForm" cssStyle="display: inline-block">
                <s:form action="recuperar" cssStyle="width: 70%;margin:0 0 0 15%">    
                    
                    <s:textfield name="correo" cssClass="form-control input-sm" label="Ingrese su correo"/>
                    <s:submit value="RECUPERA" cssStyle="float: right" cssClass="btn btn-success"/>
                </s:form>

            </s:div>

        </s:div>
    </s:div>  
            
    <script>
        var modal = document.getElementById('idPopupForm');
        var btn = document.getElementById("despliegaForm");
        var span = document.getElementsByClassName("close")[0];

        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
            
            
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
               
            }
        }
           
            
    </script>            

  </body>
</html>
