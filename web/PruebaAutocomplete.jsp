<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <script src="https://code.jquery.com/ui/1.8.23/jquery-ui.js"></script>
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/jqueryAutocomplete.js"></script>
        <style>
            .active{
                background-color: #99afe9;
                width: 100px;
            }

        </style>
    </head>

<body>
    
    <textarea id="textarea2"></textarea>
    
    <script>
        var elements = ['span', 'div', 'h1', 'h2', 'h3'];
$('#textarea2').textcomplete([
    { // html
        match: /<(\w*)$/,
        search: function (term, callback) {
            callback($.map(elements, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
        },
        index: 1,
        replace: function (element) {
            return ['<' + element + '>', '</' + element + '>'];
        }
    }
]);
    </script>
	
</body>
</html>