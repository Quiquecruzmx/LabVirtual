<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Recuperar cuenta</title>
    </head>
    <body>
        
        <s:form action="recuperar">
            
                <s:textfield name="correo" label="Ingrese su correo"/>
                
                <s:submit value="RECUPERA" cssStyle="float: right"/>
        </s:form>
    </body>
</html>
