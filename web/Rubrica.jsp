<%-- 
    Document   : Rubrica
    Created on : 25/04/2018, 10:18:57 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Rúbrica evaluación</title>
        <link href="css/admonCss.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                <th>Aspecto a evaluar</th>
                <th>Nivel avanzado</th>
                <th>Nivel intermedio</th>
                <th>Nivel Básico</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Resolución</td>
                    <td>
                        <li>Utiliza las instrucciones y algoritmos más adecuados para resolver la práctica</li>
                        <li>El funcionamiento es completo</li>
                        <input type="checkbox" class="check" value="2">
                    </td>
                    <td>
                        <li>Utiliza instrucciones y algoritmos que no son acordes para resolver el ejercicio</li>
                        <li>El funcionamiento tiene fallos importantes</li>
                        <input type="checkbox" class="check" value="1">
                    </td>
                    <td>
                        <li>Utiliza algoritmos que no resuelven la práctica</li>
                        <li>La práctica no funciona</li>
                        <input type="checkbox" class="check" value="0">
                    </td>
                    
                </tr>
                <tr>
                    <td>Identificación de variables y componentes</td>
                    <td>
                        <li>Nombra correctamente todas las variables, clases y métodos</li>
                        <li>Hace uso adecuado de la herencia y polimorfismo</li>
                        <input type="checkbox" class="check" value="2">
                    </td>
                    <td>	
                        <li>Nombra correctamente solo algunas variables y clases</li>
                        <li>Hace muy poco uso de la herencia y polimorfismo</li>
                        <input type="checkbox" class="check" value="1">
                    </td>
                    <td>
                        <li>No nombra adecuadamente ningún componente ni variable</li>
                        <li>No hace uso de herencia y polimorfismo</li>
                        <input type="checkbox" class="check" value="0">
                    </td>
                    
                </tr>
                <tr>
                    <td>Documentación interna</td>
                    <td>
                        <li>Aporta una documentación en el código para estructurar y entender claramente</li>
                        <input type="checkbox" class="check" value="2">
                    </td>
                    <td>	
                        <li>La documentación aportada es la justa para comprender el código</li>
                       
                        <input type="checkbox" class="check" value="1">
                    </td>
                    <td>
                        <li>No aporta documentación</li>
                        <input type="checkbox" class="check" value="0">
                    </td>
                    
                </tr>
                <tr>
                    <td>Presentación de resultados</td>
                    <td>
                        <li>Los resultados de la práctica son iguales a las imágenes del docente</li>
                        <li>No se muestran errores en la salida</li>
                        <input type="checkbox" class="check" value="2">
                    </td>
                    <td>	
                        <li>Los resultados de la práctica son similares a las imágenes del docente</li>
                        <li>La salida muestra algunos errores</li>
                        <input type="checkbox" class="check" value="1">
                    </td>
                    <td>
                        <li>Los resultados difieren a los del docente</li>
                        <li>La salida está llena de errores</li>
                        <input type="checkbox" class="check" value="0">
                    </td>
                    
                </tr>
                <tr>
                    <td>Actitudes</td>
                    <td>
                        <li>Entrega en la fecha previa establecida por el docente</li>
                        <li>Muestra perseverancia al aprovechar los errores marcados en prácticas previas para mejorar su trabajo</li>
                        <li>Organiza el código de manera legible</li>
                        <input type="checkbox" class="check" value="2">
                    </td>
                    <td>	
                        <li>Entrega en la fecha establecida por el docente</li>
                        <li>Muestra perseverancia al aprovechar los errores marcados en prácticas previas para mejorar su trabajo</li>
                        <li>La organización del código es legible sólo en algunas partes</li>
                        <input type="checkbox" class="check" value="1">
                    </td>
                    <td>
                        <li>Entrega en una fecha posterior a la establecida</li>
                        <li>El código es ilegible</li>
                        <input type="checkbox" class="check" value="0">
                    </td>
                    
                </tr>
            </tbody>
        </table>
        
        Cant: <input type="text" id="suma">
        
        <script>
            var $suma = 0;
        $('.check').click(function () {                  
            var checkedState =   $(this).prop("checked");
            $(this).parent().parent().children().children('.check:checked').prop("checked", false);

            $(this).prop("checked", checkedState);
           
            
            $('.check[type=checkbox]:checked').each(function()
{
                $suma += parseInt($(this).val());
            });
            
            $("#suma").val($suma); 
            $suma=0;
        });
        
        </script>
    </body>
</html>
