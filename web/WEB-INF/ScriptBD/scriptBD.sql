CREATE DATABASE labvirtual;
use labvirtual;
CREATE TABLE usuario(idUsuario int PRIMARY KEY auto_increment, username VARCHAR(20) UNIQUE NOT NULL, password VARCHAR(20) NOT NULL, nombre VARCHAR(40) NOT NULL, apePat VARCHAR(40) NOT NULL, apeMat VARCHAR(40) NOT NULL, correo VARCHAR(40) NOT NULL, rol enum('admon', 'profesor', 'alumno') NOT NULL);
CREATE TABLE archivo(idArchivo int PRIMARY KEY auto_increment, nombreArchivo VARCHAR(40) NOT NULL, autor int NOT NULL, ruta VARCHAR(100) NOT NULL, idProyecto int NOT NULL);
CREATE TABLE material(idMaterial int PRIMARY KEY auto_increment, ruta VARCHAR(100) NOT NULL, autor int NOT NULL, nombreArchivo VARCHAR(40) NOT NULL);
CREATE TABLE proyecto(idProyecto int PRIMARY KEY auto_increment, nombreProyecto VARCHAR(40) NOT NULL, autor int NOT NULL);
CREATE TABLE calificacion(idcal int PRIMARY KEY auto_increment, idProyecto int NOT NULL, calificacion int(2), idAlumnoAutor int NOT NULL, idProfesorEvaluador int NOT NULL, nombrePractica varchar(50), rubricaRuta varchar(100), observaciones varchar(200));
CREATE TABLE grupo(idGrupo int PRIMARY KEY auto_increment, nombreGrupo VARCHAR(40) NOT NULL, idProfesor int NOT NULL, passGrupo VARCHAR(20);
CREATE TABLE alumno(idTablaAlumno int PRIMARY KEY auto_increment, idAlumno int NOT NULL, idGrupo int NOT NULL);
CREATE TABLE practica(idPractica int PRIMARY KEY auto_increment, titulo VARCHAR(100) NOT NULL, objGral VARCHAR(100), objsEsp VARCHAR(200), instrucciones VARCHAR(200), resultado VARCHAR(100), pdf VARCHAR(100), idUsuario int, rubricaRuta varchar(100));
CREATE TABLE materialpractica(idTabla int PRIMARY KEY auto_increment, idMaterial int, idPractica int NOT NULL, idGrupo int NOT NULL);
CREATE TABLE Materialgrupo (idTabla int PRIMARY KEY auto_increment, idMaterial int NOT NULL, idGrupo int NOT NULL);
CREATE TABLE practicacompartida (id int PRIMARY KEY auto_increment, idPractica int NOT NULL);

ALTER TABLE archivo ADD FOREIGN KEY(autor) REFERENCES usuario(idUsuario) ON DELETE CASCADE;
ALTER TABLE material ADD FOREIGN KEY(autor) REFERENCES usuario(idUsuario) ON DELETE CASCADE;
ALTER TABLE grupo ADD FOREIGN KEY(idProfesor) REFERENCES usuario(idUsuario) ON DELETE CASCADE;--lISTO
ALTER TABLE alumno ADD FOREIGN KEY(idAlumno) REFERENCES usuario(idUsuario) ON DELETE CASCADE;--LISTO
ALTER TABLE alumno ADD FOREIGN KEY(idGrupo) REFERENCES grupo(idGrupo) ON DELETE CASCADE;--LISTO
ALTER TABLE proyecto ADD FOREIGN KEY(autor) REFERENCES usuario(idUsuario) ON DELETE CASCADE;--LISTO pero NO PROBADO
--ALTER TABLE archivo ADD FOREIGN KEY(autor) REFERENCES proyecto(idProyecto) ON DELETE CASCADE;
ALTER TABLE archivo ADD FOREIGN KEY(idProyecto) REFERENCES proyecto(idProyecto) ON DELETE CASCADE;
ALTER TABLE calificacion ADD FOREIGN KEY(idProyecto) REFERENCES proyecto(idProyecto) ON DELETE CASCADE;
ALTER TABLE calificacion ADD FOREIGN KEY(idAlumnoAutor) REFERENCES usuario(idUsuario) ON DELETE CASCADE;
ALTER TABLE calificacion ADD FOREIGN KEY(idProfesorEvaluador) REFERENCES usuario(idUsuario) ON DELETE CASCADE;
ALTER TABLE materialpractica ADD FOREIGN KEY(idMaterial) REFERENCES material(idMaterial) ON DELETE CASCADE;
ALTER TABLE materialpractica ADD FOREIGN KEY(idPractica) REFERENCES practica(idPractica) ON DELETE CASCADE;
ALTER TABLE materialpractica ADD FOREIGN KEY(idGrupo) REFERENCES grupo(idGrupo) ON DELETE CASCADE;
ALTER TABLE materialgrupo ADD FOREIGN KEY(idMaterial) REFERENCES material(idMaterial) ON DELETE CASCADE;
ALTER TABLE materialgrupo ADD FOREIGN KEY(idGrupo) REFERENCES grupo(idGrupo) ON DELETE CASCADE;
ALTER TABLE practica ADD FOREIGN KEY(idUsuario) REFERENCES usuario(idUsuario) ON DELETE CASCADE;
ALTER TABLE practicacompartida ADD FOREIGN KEY (idPractica) REFERENCES practica(idPractica);


