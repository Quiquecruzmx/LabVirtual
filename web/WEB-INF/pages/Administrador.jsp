<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/admonCss.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/tooltip.css" type="text/css" rel="stylesheet"/>
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Bienvenido Administrador</title>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href=""><h4><s:property value = "#session.USER.username"/></h4></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="Ediciones">Editar Registros</a>
                 </li>
    
                <li class="nav-item">
                    <a class="nav-link" href="Grupos">Crear Grupos</a>
                </li>
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav> 
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">¡Bienvenido <s:property value = "#session.USER.username"/>!</h1>
            <br>
            <p class="lead">En este apartado usted puede realizar la administración sobre los usuarios que utilicen este sitio, desde dar de alta a
                <s:a href="NuevoUsuario">nuevos usuarios</s:a> como alumnos o profesores, así como la edición de su información.</p>
            <p class="lead">También tiene la facultad de <s:a href="Grupos">generar grupos nuevos</s:a>, decidir a que profesor asignarlos así como los alumnos que tendrá a cargo</p>

        </div>
            <div style="width: 20%; margin: 0 auto;">
                <a href="Ediciones" >
                    <img src="images/logo_lab_cpp.png" alt="logo lab cpp" title="Comenzar">
                </a>
                <h3 style="margin-left: 50px">Comenzar</h3>
            </div>   
         
        <a href="Ediciones"></a>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
    </body>
</html>