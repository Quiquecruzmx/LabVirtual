<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/alumnoCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Login Alumno</title>
        
    </head>
    <body>
        <div>     
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#"><s:property value = "#session.USER.username "/></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                    </a>
                    <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                        <form action="ProyectoNuevoAlumno" class="form-horizontal" method="post" id="nuevoProyecto">
                            Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                            Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                            <s:select cssClass="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Ext."></s:select>

                            <input class="btn btn-primary" type ="submit" value="CREAR">
                        </form>

                    </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="GruposListaAlumno">Ver grupos</a></li>
                <li class="nav-item"><a class="nav-link" href="ListaProyectosAlumno">Detalles de proyectos</a></li>
                <li class="nav-item"><a class="nav-link" href="verpractica">Prácticas</a></li>
                
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav>
        </div>   
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">¡Hola <s:property value = "#session.USER.nombre"/>!</h1>
            <br>
            <p class="lead">Como alumno, estarás inscrito a uno o varios grupos con diferentes profesores a cargo los cuales 
                te dejarán actividades/prácticas a realizar y tú podrás desarrollar con las herramientas disponibles en este sitio. 
            </p>
            <div class="row">
                <div class="input-group">
                    <input type="text" class="form-control" id="busqueda" placeholder="Buscar proyecto"/>         
                </div>
            </div>
            <s:actionmessage/>
            <s:actionerror/>
        </div>                      
        <div style="margin-top: 30px;">
            <table class="tabla" id="table">
                <tr>
                    <th class="tabHeader">Proyecto</th>
                    <th class="tabHeader">Abrir</th>
                    <th class="tabHeader">Enviar</th>
                    <th class="tabHeader">Borrar</th>
                </tr>
                <s:iterator value="#session.PROYECTOS_ALU">
                    <s:url action="AbrirProyecto" var="abrir">
                        <s:param name="idP" value="%{idProyecto}"/>
                        <s:param name="nombreProyecto" value="%{nombreProyecto}"/>
                    </s:url>

                    <s:url action="EliminarProyecto" var="eliminarP">
                        <s:param name="idP" value="%{idProyecto}"/>
                        
                    </s:url>
                    <tr class="tabRow">
                        <td class="tabCel"><s:property value="nombreProyecto"/></td>
                        <td class="tabCel"><s:a href="%{abrir}"><img src="images/open-folder-icon.jpg" width="25" height="25"></s:a></td>
                        <td class="tabCel">
                        <s:a href="#" id="despliegaForm" onclick="abreForm(%{idProyecto}, '%{nombreProyecto}')">
                                <img src="images/send-icon.png" width="25" height="25">
                            </s:a>

                        </td>
                        <td class="tabCel"><s:a href="%{eliminarP}" onclick="return confirm('¿Eliminar este proyecto?');"><img src="images/basura_opt.png" width="25" height="25"></s:a></td>
                    </tr>
                </s:iterator>
                   
            </table>

        

             
        </div>  
        
        <s:div id="idPopupForm" cssClass="popupForm">   
            <s:div cssClass="contenedorForm" >
                <s:div cssClass="headerForm">
                    <span class="close">&times;</span>
                    <h2>Enviar práctica</h2>
                </s:div>
                <s:div cssClass="cuerpoForm" cssStyle="display: inline-block">
                    <s:form action="EnvioProfe" cssStyle="margin:0 0 0 15%">
                    
                    <s:hidden value="%{idProyecto}" name="idProy"/>
                    <s:textfield label="Nombre proyecto" readonly="true"  name="nombreProy" value="%{nombreProyecto}" cssClass="form-control input-sm"/>
                    <s:select list="#session.PRACTICAS_ALU" name="idProfe" listKey="%{idUsuario.id}" listValue="%{titulo+' [Profesor: '+idUsuario.nombre+']'}" 
                              label="Para practica" onchange="setNamePrac()" id="nombrePract" cssClass="form-control input-sm"/>
                    <s:hidden value="%{#session.PRACTICAS_ALU[0].titulo}" name="nombrePracticaEnvio"/>
                    <s:submit value="ENVIAR" cssClass="btn btn-success"/>
                    </s:form>
               
                </s:div>

            </s:div>
        </s:div>  
                    
         <script>
            function setNamePrac(){
                // set text box value here
                var elem =  document.getElementById("nombrePract");
                var str = elem.options[elem.selectedIndex].text;
                var res = str.substring(0, str.indexOf("["));
                document.getElementsByName("nombrePracticaEnvio")[0].value =  res;
            }
             
            function abreForm(numProy, nombreProy){
                 var modal = document.getElementById('idPopupForm');
                 modal.style.display = "block";
                 var span = document.getElementsByClassName("close")[0];
                 
                 document.getElementsByName("idProy")[0].value = numProy;
                 var nombre = document.getElementsByName("nombreProy")[0];
                 var nombre2 = document.getElementsByName("nombreProy")[1];
                 nombre.value = nombreProy;
                 //nombre2.value = nombreProy;
               span.onclick = function() {
                   modal.style.display = "none";
               }

               // When the user clicks anywhere outside of the modal, close it
               window.onclick = function(event) {
                   if (event.target == modal) {
                       modal.style.display = "none";
                   }
               }
            }
    </script>                
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap-4.0.0.js"></script>
    <script>
        // Write on keyup event of keyword input element
        $("#busqueda").keyup(function(){
            var searchText = $(this).val().toLowerCase();
            // Show only matching TR, hide rest of them
            $.each($("#table tbody tr"), function() {
                if($(this).text().toLowerCase().indexOf(searchText) === -1)
                   $(this).hide();
                else
                   $(this).show();                
            });
        }); 

    </script>  
    </body>
</html>
