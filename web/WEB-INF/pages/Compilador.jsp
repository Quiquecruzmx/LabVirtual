<%--
    Document   : Compilador
    Created on : 15/02/2018, 01:48:41 PM
    Author     : Enrique
--%>

<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compilador</title>
        
        <script src="js/jquery-3.2.1.js" type="text/javascript"></script>
        <link href="css/compilador.css" type="text/css" rel="stylesheet">
        <link href="css/alumnoCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <script src="https://code.jquery.com/ui/1.8.23/jquery-ui.js"></script>
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link rel="stylesheet" href="https://codemirror.net/lib/codemirror.css">
        <script src="https://codemirror.net/lib/codemirror.js"></script>
        <script src="https://codemirror.net/addon/edit/matchbrackets.js"></script>
        <link rel="stylesheet" href="https://codemirror.net/addon/hint/show-hint.css">
        <script src="https://codemirror.net/addon/hint/show-hint.js"></script>
        <script src="https://codemirror.net/mode/clike/clike.js"></script>
        <script type="text/javascript" src="js/compiladorFunciones.js"></script>
        <script src="js/autocompletado.js" type="text/javascript"></script>
        <style>.CodeMirror {border: 2px inset #dee;}</style>
        <link rel="icon" href="images/logo_lab_cpp.png">
        
    </head>
    <body onload="rec()">
        
        <s:url id="fileDownload" action="DescargarArchivo" >
            <s:param name="nombreProyecto" value="%{nombreProyecto}"></s:param>
        </s:url>
        
        <div id="idPopupForm" class="popupForm">
            <div class="contenedorForm">
                <div class="headerForm">
                    <span class="close">&times;</span>
                    <h2>Lista de archivos, seleccione los que desee descargar</h2></div>
                <div class="cuerpoForm">
                    <s:form action="DescargarAlumno">
                    <table class="tabla">
                        <thead>
                        <tr class="tabRow">
                            <th class="tabHeader">Seleccionar <br></th>
                            <th class="tabHeader">NombreArchivo</th>
                        </tr>
                       </thead>
                       <tbody>
                           <s:iterator value="proyectos" status="stat">   
                            <tr class="tabRow">     
                                <td class="tabCel"><s:checkbox  name="archivosDescarga" fieldValue="%{nombre+''+extension}" theme="simple"/></td>
                                <td class="tabCel"><s:property value="nombre"/><s:property value="extension"/></td>
                            </tr>

                        </s:iterator>
                        </tbody>

                    </table>
                        <s:hidden name="nombreProyecto" value="%{nombreProyecto}"/>
                        <s:hidden name="nombreAutor" value="%{nombreAutor}"/>
                    <s:submit value="Descargar ficheros"/>
                    </s:form>
                </div>
            </div>
        </div>
        
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
           <a class="navbar-brand" href="AlumnoMain" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');"><s:property value = "#session.USER.username "/></a>
           <a class="navbar-brand" style="color: white" ><s:property value="nombreProyecto"/></a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
           </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');" href = "AlumnoMain">Inicio</a></li>
                    <li class="nav-item"><s:a cssClass="nav-link" href="%{fileDownload}">Descargar .exe</s:a></li>
                    <li class="nav-item"><s:a cssClass="nav-link" href="#" id="despliegaForm">Descargar Ficheros</s:a></li>
                    


                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item float-right"><s:a cssClass="nav-link" href="#" id="compilar">Compilar</s:a></li>
                    <li class="nav-item float-right"><s:a cssClass="nav-link" href="#" id="guardar">Guardar</s:a></li>
                </ul>
                <a class="btn btn-outline-warning my-2 my-sm-0 float-right" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');" href="Logout">Salir</a>
            </div>    
        </nav>

        <div class="modal-body col-12">
        <div id="tabGral">
            
            <li id="0" class="tablinks">
                <a onclick="abrirEditor(this.parentNode.id)"><s:property value="proyectos[0].nombre"/><s:property value="proyectos[0].extension"/></a>
                <a class="eliminar" onclick ="borrar(this.parentNode.id)">-</a>
            </li>
            <li class="nav-item dropdown">
                <a  class="nav-link  agrega" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >+</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown" >
                    
                    <form onsubmit="return false;">
                        Nombre de Archivo: <input autocomplete="off" class="form-control input-sm" type="text" id="nuevoArchivo" maxlength="12"/>
                        <s:select cssClass="form-control input-sm" id = "nuevaExtension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                        <button class="btn btn-primary" onclick="nuevoArch()">Crear</button>
                    </form>
					
                </div>
            </li>
            
            
        </div>
       
        <div class="col-md-12">
           
            <s:form action="codeAlumno" id="formulario" autocomplete="off">
                <s:hidden readonly="true" name="nombreProyecto" id="proyecto" label="Nombre Proyecto"/>
                <s:textfield name="proyectos[0].nombre"  id="nombre0" readonly="true" cssClass="cont" hidden="hidden"/>
                <s:hidden name = "proyectos[0].extension" id="ext0" readonly="true" cssClass="cont"/>  
                <s:hidden name="tam" id="tam" value="%{proyectos.size}"/>
                <s:textarea  id="edit0" name="proyectos[0].codigo" cssClass="cont"/>

                
                <s:hidden id="accion" name="accion" value="compilar"/>
                
            </s:form>
            
        </div>
    </div>   
    <script>
        var form = document.getElementById("formulario");

        document.getElementById("compilar").addEventListener("click", function () {
            document.getElementById('accion').value='compilar';
            form.submit();
        });
        
        document.getElementById("guardar").addEventListener("click", function () {
            document.getElementById('accion').value='guardar';
            form.submit();
        });
    </script>          
    <div class="log">
        <h4 style="color:white">Errores:</h4>   
        <s:actionerror  escape="false"/>
        <s:actionmessage escape="false" />
    </div>     
    <div hidden="true">
        <s:iterator value="proyectos" status="stat">
        <s:textfield id = "proyectos[%{#stat.index}].nombre" name="proyectos[%{#stat.index}].nombre"/>
        <s:textfield id = "proyectos[%{#stat.index}].extension" name="proyectos[%{#stat.index}].extension"/>
        <s:textarea id = "proyectos[%{#stat.index}].codigo" name="proyectos[%{#stat.index}].codigo"/>
        </s:iterator>
    </div>
    <script>
        $.ajax({
            statusCode: {
              404: function() {
                alert( "Pagina no encontrada" );
                
              },
              204: function() {
                alert( "Archivo no creado aun" );
                console.log("Archivo no encontrado");
              }
            }
          });

        var modal = document.getElementById('idPopupForm');
        var btn = document.getElementById("despliegaForm");
        var span = document.getElementsByClassName("close")[0];

        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
            btn.value = document.getElementById("suma").value;
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
                btn.value = document.getElementById("suma").value;
            }
        }
    </script>
     <!-- Include all compiled plugins (below), or include individual files as needed --> 
     <script src="js/popper.min.js"></script>
     <script src="js/bootstrap-4.0.0.js"></script>
    </body>
</html>
