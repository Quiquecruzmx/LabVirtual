<%-- 

    Document   : CrearGrupo
    Created on : 14/04/2018, 09:30:22 AM
    Author     : admin
--%>

<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/admonCss.css">
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Grupos</title>
    </head>
    
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="AdminMain"><h4><s:property value = "#session.USER.username"/></h4></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="Ediciones">Editar Registros</a>
                 </li>
    
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav> 
        
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Grupos</h1>
            <p class="lead">Aquí puede crear nuevos grupos y asignarlos a los docentes así como la facultad de inscribir a los alumnos o dar de baja un grupo si así lo desea</p>
            <div class="row">
                
                    <div class="input-group">
                        <input type="text" class="form-control" id="busqueda" placeholder="Buscar grupo"/> 
                        
                    </div><!-- /input-group -->
                
            </div><!-- /.row -->
         </div>
        
        <s:actionmessage/>
        <s:actionerror/>
        <div>
        <table class="tabla">
            <thead>
            <tr class="tabRow">
                <th class="tabHeader">Nombre Grupo</th>
                <th class="tabHeader">Profesor a cargo</th>
                <th class="tabHeader">Inscribir Alumnos</th>
                <th class="tabHeader">Eliminar</th>
                
            </tr>
           </thead>
           
           <s:iterator value="#session.GRUPOS" >     
                <s:url action="InscribirAlumnos" var="grupo" ><%-- var grupo es el valor proveniente del href--%>
                    <s:param name="grupo" value="%{idGrupo}"/><%--name grupo es el nombre con el que se va a recuperar en el action--%>
                                <%--value idGrupo es el valor que se va a enviar al action y que proviene del objeto Grupo recuperado--%>
                </s:url>
                <s:url  action="BorrarGrupo" var="borrarGrupo">

                    <s:param name="grupoBorrar" value="%{idGrupo}"/>
                               
                </s:url>
                
               
                <tbody>
                <tr class="tabRow">     
                    <td class="tabCel"><s:property value="nombreGrupo"/></td>
                    <td class="tabCel"><s:property value="titular.username"/></td>
                    <td class="tabCel"><s:a href="%{grupo}"><img src="images/eye-icon1.png" width="25" height="25"/></s:a></td>
                    <td class="tabCel"><s:a href="%{borrarGrupo}" onclick="return confirm('¿Estás seguro de eliminar este grupo?')"><img src="images/basura_opt.png" width="25" height="25"/></s:a></td>
                    
                   
                </tr>
                </tbody>
            </s:iterator>
           
           
           
        </table>
        </div>
        <div style=" margin-top: 30px; text-align: center;">
            <h3>Crear nuevo grupo</h3>
            <s:form action="CrearGrupo" cssClass="lead" cssStyle="margin-left: 20%">           
            <s:textfield label = "Nombre Grupo" 
                         name="nombreGrupo" 
                         autocomplete="off"  
                         cssStyle="width: 100%" 
                         cssClass="form-control input-sm" />
            <s:select list="#session.PROFESORES" 
                    listValue="%{username+': '+nombre+' '+apePat}"  
                    listKey="%{id}" label="Profesor titular" 
                    name="userTitular"
                    cssClass="form-control input-sm"
                    />
            <s:submit value="Crear grupo" cssClass="btn btn-primary"/>
        </s:form>
        </div>    
                <script>
                    // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($(".tabla tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
             
             
       </script>   
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
    </body>
</html>
