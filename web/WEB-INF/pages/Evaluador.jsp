<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compilador</title>
        <link href="css/alumnoCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/compilador.css" type="text/css" rel="stylesheet">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link rel="stylesheet" href="https://codemirror.net/lib/codemirror.css">
        <link rel="stylesheet" href="https://codemirror.net/addon/hint/show-hint.css">
        <script src="js/jquery-3.2.1.js" type="text/javascript"></script>
        <script src="https://code.jquery.com/ui/1.8.23/jquery-ui.js"></script>
        <script src="https://codemirror.net/lib/codemirror.js"></script>
        <script src="https://codemirror.net/addon/edit/matchbrackets.js"></script>
        <script src="https://codemirror.net/addon/hint/show-hint.js"></script>
        <script src="https://codemirror.net/mode/clike/clike.js"></script>
        <script type="text/javascript" src="js/compiladorFunciones.js"></script>
        <script src="js/autocompletado.js" type="text/javascript"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <style>.CodeMirror {border: 2px inset #dee;}</style>
        
    </head>
    <body onload="rec()">
        
        <div id="idPopupForm" class="popupForm">
            <div class="contenedorForm">
                <div class="headerForm">
                    <span class="close">&times;</span>
                    <h2>Rúbrica de evaluación</h2>
                </div>
               <s:div cssClass="cuerpoForm">
                   <!--Rubrica-->
                   <s:property escape="false" value="#session.rubrica"/>
            <input type="hidden" id="suma">
            </s:div>
            </div>
        </div>
        
        <script type="text/javascript" src="js/rubrica.js"></script>
        
        <div id="idPopupForm2" class="popupForm">
            <div class="contenedorForm">
                <div class="headerForm">
                    <span class="close">&times;</span>
                    <h2>Lista de archivos, seleccione los que desee descargar</h2></div>
                <div class="cuerpoForm">
                    <s:checkbox id="todos" name="nombre" label="Seleccionar todos" cssStyle="margin-left: 10%"/>
                    <s:form action="DescargarEvaluador">
                        
                    <table class="tabla">
                        <thead>
                        <tr class="tabRow">
                            <th class="tabHeader">Seleccionar <br></th>
                            <th class="tabHeader">NombreArchivo</th>
                        </tr>
                       </thead>
                       <tbody>
                           <s:iterator value="proyectos" status="stat">   
                            <tr class="tabRow">     
                                <td class="tabCel"><s:checkbox  name="archivosDescarga" fieldValue="%{nombre+''+extension}" theme="simple"/></td>
                                <td class="tabCel"><s:property value="nombre"/><s:property value="extension"/></td>
                            </tr>

                        </s:iterator>
                        </tbody>

                    </table>
                        <s:hidden name="nombreProyecto" value="%{nombreProyecto}"/>
                        <s:hidden name="nombreAutor" value="%{nombreAutor}"/>
                    <s:submit value="Descargar ficheros"/>
                    </s:form>
                </div>
            </div>
        </div>
        
        
        <s:url id="fileDownload" action="DescargarEvaluador" >
            <s:param name="nombreProyecto" value="%{nombreProyecto}"></s:param>
            <s:param name="nombreAutor" value="%{nombreAutor}"></s:param>
        </s:url>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
           <a class="navbar-brand" href="ProfesorMain"><s:property value = "#session.USER.username "/></a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
           </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');" href = "ProfesorMain">Inicio</a></li>

                    <li class="nav-item"><s:a cssClass="nav-link" href="%{fileDownload}">Descargar .exe</s:a></li>
                    <li class="nav-item"><s:a cssClass="nav-link" href="#" id="despliegaForm2">Descargar Ficheros</s:a></li>


                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item float-right"><s:a cssClass="nav-link" href="#" id="compilar">Compilar</s:a></li>
                 <li class="nav-item float-right"><s:a cssClass="nav-link" href="#" id="calificar">Calificar</s:a></li>
                </ul>
                <a class="btn btn-outline-warning my-2 my-sm-0 float-right" onclick="return confirm('Asegurese de guardar sus cambios antes de salir');" href="Logout">Salir</a>
            </div>    
        </nav>
        <div class="modal-body row">
        <div id="tabGral">
            
            <li id="0" class="tablinks">
                <a onclick="abrirEditor(this.parentNode.id)"><s:property value="proyectos[0].nombre"/><s:property value="proyectos[0].extension"/></a>
            
            </li>
            <div hidden="true">
                <li class="nav-item dropdown">
                <a  class="nav-link dropdown-toggle agrega" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Agregar archivo</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" >

                        <form onsubmit="return false;">
                            Nombre de Archivo: <input autocomplete="off" class="form-control input-sm" type="text" id="nuevoArchivo"/>
                            <s:select id = "nuevaExtension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                            <button class="btn btn-primary" onclick="nuevoArch()">Crear</button>
                        </form>

                    </div>
                </li>
            </div>
            
            
        </div>
        <div class="col-md-10">
           
            <s:form action="evaluar" id="formulario" autocomplete="off">
                <s:hidden value="%{idP}" name="idP"></s:hidden>
                <s:hidden readonly="true" name="nombreProyecto" id="proyecto" />
                <s:hidden name="tam" id="tam" value="%{proyectos.size}"/>
                <s:hidden name="nombreAutor" value="%{nombreAutor}"/>
                <s:textfield name="proyectos[0].nombre"  id="nombre0" readonly="true" cssClass="cont" hidden="hidden"/>
                <s:hidden name = "proyectos[0].extension" id="ext0" readonly="true" cssClass="cont" cssStyle="width: 40px;"/>  
                
                <s:textarea  id="edit0" name="proyectos[0].codigo" cssClass="cont"/>
                
                
                
                <s:hidden id="accion" name="accion" value="compilar"/>
                
            </s:form>
            
        </div>
        <div class="col-md-2 column">
        <s:form action="setCalificacion" id="califForm">
            <s:hidden value="%{idP}" name="idProy"></s:hidden>
            <s:hidden name="nombrePractica" value="%{#session.nombrePractica}"></s:hidden>
            <s:hidden name="rubrica" id="rubrica"/>
            <s:textfield name="calificacion" value="%{#session.calif}" id="despliegaForm" readonly="true" cssClass="form-control input-sm"/>
            
            <s:textarea  name="observaciones" value="%{#session.observaciones}" placeholder ="Escriba sus observaciones" cssStyle="resize: none" rows="12" cssClass="form-control input-sm"/>
        </s:form>
        <!--<button id="despliegaForm">Calificar</button>-->
        </div>
                        
        <div hidden="true">
        <s:iterator value="proyectos" status="stat">
            <s:textfield id = "proyectos[%{#stat.index}].nombre" name="proyectos[%{#stat.index}].nombre"/>
            <s:textfield id = "proyectos[%{#stat.index}].extension" name="proyectos[%{#stat.index}].extension"/>
            <s:textarea id = "proyectos[%{#stat.index}].codigo" name="proyectos[%{#stat.index}].codigo"/>
        </s:iterator>
        </div>
    </div>
    <div class="log">
        <h4 style="color:white">Errores:</h4>   
            <s:actionerror escape="false" />
            <s:actionmessage escape="false" />
    </div>    
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script>

        var modal = document.getElementById("idPopupForm");
        var btn = document.getElementById("despliegaForm");
        var span = document.getElementsByClassName("close")[0];
        var rubrica = document.getElementsByClassName("cuerpoForm")[0];

        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            
            modal.style.display = "none";
            if(document.getElementById("califUser").value !=0){
                btn.value = parseFloat(document.getElementById("califUser").value) / parseFloat(document.getElementById("califTotal").value) * 10;
                var rub = document.getElementById("rubrica");
                rub.value = rubrica.innerHTML;
                var res = rub.value.replace(/contenteditable="true"/g, "contenteditable=\"false\"");
                rub.value = res;
            }    
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target === modal) {
                
                modal.style.display = "none";
                if(document.getElementById("califUser").value !=0){
                    
                    btn.value = parseFloat(document.getElementById("califUser").value) / parseFloat(document.getElementById("califTotal").value) * 10 ;
                    var rub = document.getElementById("rubrica");
                    rub.value = rubrica.innerHTML;
                    var res = rub.value.replace(/contenteditable="true"/g, "contenteditable=\"false\"");
                    rub.value = res;
                }
            }
            
            if (event.target == modal2) {
                modal2.style.display = "none";

            }
        }
        
        
        
        var modal2 = document.getElementById("idPopupForm2");
        var btn2 = document.getElementById("despliegaForm2");
        var span2 = document.getElementsByClassName("close")[1];

        btn2.onclick = function() {
            modal2.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span2.onclick = function() {
            modal2.style.display = "none";
        }

    
    
    var form = document.getElementById("formulario");
    var formCalif = document.getElementById("califForm");
        document.getElementById("compilar").addEventListener("click", function () {
            document.getElementById('accion').value='compilar';
            form.submit();
        });
        
        document.getElementById("calificar").addEventListener("click", function () {
            formCalif.submit();
        });
        
        
        $('#todos').click(function() {
            var c = this.checked;
            $(':checkbox').prop('checked',c);
        });

    </script> 
     <script src="js/popper.min.js"></script>
     <script src="js/bootstrap-4.0.0.js"></script>
    </body>
</html>
