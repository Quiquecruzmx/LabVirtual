<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/admonCss.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Nuevo Usuario</title>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="AdminMain"><h4><s:property value = "#session.USER.username"/></h4></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="Ediciones">Editar Registros</a>
                 </li>
    
                <li class="nav-item">
                    <a class="nav-link" href="Grupos">Crear Grupos</a>
                </li>
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav> 
        <h4>
                <br>Ingrese los datos correspondientes
	</h4>
        <s:form action="InsertarUsuario" >
            <s:textfield name="newUsername" label="Username"cssClass="form-control input-sm"/> 
            <s:password name="newPass" label="Contraseña" cssClass="form-control input-sm" maxLength="10"/>
            <s:textfield name="newNombre" label="Nombre(s)" cssClass="form-control input-sm"/> 
            <s:textfield name="newApePat" label="Apellido Paterno" cssClass="form-control input-sm"/>
            <s:textfield name="newApeMat" label="Apellido Materno" cssClass="form-control input-sm"/> 
            <s:select list="{'Profesor', 'Alumno'}" label = "Rol" name = "newRol" cssClass="form-control input-sm"/>
            <s:textfield name="newCorreo" label="Correo" cssClass="form-control input-sm"/> 
            
            <s:submit value="Agregar"  cssClass="btn btn-success"/>
            <s:actionmessage  />
        </s:form>
    </body>
</html>
