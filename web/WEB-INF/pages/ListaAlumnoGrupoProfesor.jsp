<%-- 
    Document   : ListaAlumnosGrupo
    Created on : 15/04/2018, 10:43:13 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
         <link rel="stylesheet" type="text/css" href="css/admonCss.css">
         <script src="js/jquery-3.2.1.min.js"></script>
         <link rel="icon" href="images/logo_lab_cpp.png">
        <title> Grupo </title>
    </head>
    <body>
        <div class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="ProfesorMain"><h5 ><s:property value="#session.USER.username"/></h5></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                     <a class="nav-link" href="ProfesorMain">Inicio </a>
                  </li>
               </ul>
               <form class="form-inline my-2 my-lg-0">
                  <a href="Logout" class="btn btn-outline-warning my-2 my-sm-0">Salir</a>
                  
               </form>
            </div>
         </div>
        <div class="jumbotron jumbotron-fluid text-center">
            <h4 clas="display-4">Alumnos inscritos</h4>
            <p class="lead">Aquí puede observar la lista de alumos inscritos en el grupo: <s:property value="nombreGrupo"/></p>
            <div class="row">
                <div class="input-group">
                    <input type="text" class="form-control" id="busqueda" placeholder="Buscar alumno"/>         
                </div>
            </div>
        </div>
        <s:actionmessage/>
        
        <div>
        <table class="tabla" id="table">
            <thead>
            <tr  class="tabRow">
                <th class="tabHeader">Usuario</th>
                <th class="tabHeader">Nombre</th>
                
            </tr>
           </thead>
           <tbody>
               <s:iterator value="alumnos" status="elem">     
                
                
                <tr class="tabRow">     
                    
                    <td class="tabCel"><s:property value="idAlumno.username"/></td>
                    <td class="tabCel"><s:property value="idAlumno.nombre"/> <s:property value="idAlumno.apePat"/></td>

                   
                </tr>
                
            </s:iterator>
            </tbody>

        </table><br>
        <div style="margin-left:40%;"><a href="GruposLista" class="btn btn-outline-success my-2 my-sm-0">Regresar a Grupos</a></div> 
        </div>
         
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
         <script>
                    // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($("#table tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
             
             
       </script>  
    </body>
</html>
