<%-- 
    Document   : ListaAlumnosGrupo
    Created on : 15/04/2018, 10:43:13 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
         <link rel="stylesheet" type="text/css" href="css/admonCss.css">
         <script src="js/jquery-3.2.1.min.js"></script>
         <link rel="icon" href="images/logo_lab_cpp.png">
        <title> Grupo </title>
    </head>
    <body>
        <div>     
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="AlumnoMain"><s:property value = "#session.USER.username "/></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                    </a>
                    <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                        <form action="ProyectoNuevoAlumno" class="form-horizontal" method="post" id="nuevoProyecto">
                            Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                            Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                            <s:select cssClass="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Ext."></s:select>

                            <input class="btn btn-primary" type ="submit" value="CREAR">
                        </form>

                    </div>
                </li>
              
                <li class="nav-item"><a class="nav-link" href="ListaProyectosAlumno">Detalles de proyectos</a></li>
                <li class="nav-item"><a class="nav-link" href="verpractica">Prácticas</a></li>
                
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav>
        </div> 
        <div class="jumbotron jumbotron-fluid text-center">
            <s:actionmessage/>
            <h4>Alumnos inscritos</h4>
            <div class="row">
                <div class="input-group">
                    <input type="text" class="form-control" id="busqueda" placeholder="Buscar alumno"/>         
                </div>
            </div>
        </div>
        <div>
        <table class="tabla" id="table">
            <thead>
            <tr  class="tabRow">
                <th class="tabHeader">Usuario</th>
                <th class="tabHeader">Nombre</th>
                
            </tr>
           </thead>
           <tbody>
               <s:iterator value="alumnos" status="elem">     
                
                
                <tr class="tabRow">     
                    
                    <td class="tabCel"><s:property value="idAlumno.username"/></td>
                    <td class="tabCel"><s:property value="idAlumno.nombre"/> <s:property value="idAlumno.apePat"/></td>

                   
                </tr>
                
            </s:iterator>
            </tbody>

        </table>
        <a href="GruposListaAlumno" class="btn btn-outline-success my-2 my-sm-0">Regresar a Grupos</a>   
        </div>
         
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
        <script>
            // Write on keyup event of keyword input element
            $("#busqueda").keyup(function(){
                var searchText = $(this).val().toLowerCase();
                // Show only matching TR, hide rest of them
                $.each($("#table tbody tr"), function() {
                    if($(this).text().toLowerCase().indexOf(searchText) === -1)
                       $(this).hide();
                    else
                       $(this).show();                
                });
            });     
      </script>
    </body>
</html>
