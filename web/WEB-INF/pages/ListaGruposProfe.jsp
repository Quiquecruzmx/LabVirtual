<%-- 
    Document   : ListaGruposProfe
    Created on : 30/04/2018, 01:25:08 PM
    Author     : admin
--%>

<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/admonCss.css">
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Lista Grupos</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="ProfesorMain"><h4><s:property value = "#session.USER.username "/></h4></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="material">Material</a>
                 </li>
                 <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                    </a>
                        <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                            <form action="ProyectoNuevoProfesor" id="formInicial" class="form-horizontal" method="post">
                                Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                                Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                                <s:select class="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                                <input class="btn btn-primary" type ="submit" value="CREAR">
                            </form>

                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="VerPracticasProfesor">Ver prácticas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nuevapractica">Crear Práctica</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ListaCalificar">Calificar proyectos</a>
                </li>
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav> 
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Titular: <s:property value = "#session.USER.nombre"/> <s:property value = "#session.USER.apePat"/> </h1>
            <br>
            <p class="lead">.
                Aquí se mostrarán los grupos de los cuales usted está a cargo, y a los cuales podrá dejar prácticas o actividades a resolver.
                Si lo desea, podrá ver quienes están inscritos en cada uno de sus grupos aunque no podrá editarlos a menos que lo solicite
                al administrador.
            </p>
            <div class="row">
                <div class="input-group">
                    <input type="text" class="form-control" id="busqueda" placeholder="Buscar grupo"/>         
                </div>
            </div>
            <s:actionmessage/>
            <s:actionerror/>
        </div>                        
                               
        <div>
        <table class="tabla" id="table">
            <thead>
            <tr class="tabRow">
                <th class="tabHeader">Nombre Grupo</th>
                <th class="tabHeader">Ver Grupo</th>
                
            </tr>
           </thead>
           
           <s:iterator value="grupos" >     
                <s:url action="VerListaGrupo" var="grupo" ><%-- var grupo es el valor proveniente del href--%>
                    <s:param name="grupo" value="%{idGrupo}"/><%--name grupo es el nombre con el que se va a recuperar en el action--%>
                                <%--value idGrupo es el valor que se va a enviar al action y que proviene del objeto Grupo recuperado--%>
                </s:url>
                <s:url  action="BorrarGrupo" var="borrarGrupo">

                    <s:param name="grupoBorrar" value="%{idGrupo}"/>
                               
                </s:url>
                
               
                <tbody>
                <tr class="tabRow">     
                    <td class="tabCel"><s:property value="nombreGrupo"/></td>
                    <td class="tabCel"><s:a href="%{grupo}"><img src="images/eye-icon1.png" width="25" height="25"/></s:a></td>
                    
                   
                </tr>
                </tbody>
            </s:iterator>
           
           
           
        </table>
        </div>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
      <script>
                    // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($("#table tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
             
             
       </script>  
    </body>
</html>
