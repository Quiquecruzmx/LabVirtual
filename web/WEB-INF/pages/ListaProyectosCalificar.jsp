<%-- 
    Document   : ListaProyectosCalificar
    Created on : 28/04/2018, 12:31:55 PM
    Author     : admin
--%>

<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Lista de proyectos</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="ProfesorMain"><h4><s:property value = "#session.USER.username "/></h4></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="material">Material</a>
                    </li>
                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Crear Proyecto Nuevo
                        </a>
                            <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                                <form action="ProyectoNuevoProfesor" id="formInicial" class="form-horizontal" method="post">
                                    Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                                    Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                                    <s:select class="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h'}" label = "Extensión"></s:select>

                                    <input class="btn btn-primary" type ="submit" value="CREAR">
                                </form>

                            </div>
                    </li>
                
                    <li class="nav-item">
                        <a class="nav-link" href="GruposLista">Ver grupos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="nuevapractica">Crear Práctica</a>
                    </li>
                
                </ul>
                <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
            </div>    
        </nav>
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Lista de proyectos para calificar</h1>
            <br>
            <p class="lead">
                Aquí se almacenarán todos los proyectos que sus alumnos le envien en relación a las prácticas que les haya dejado.
                Usted podrá acceder al código de sus proyectos y compilarlos, así como asignarles una calificación y sus observaciones
                al código que ellos podrán ver desde su cuenta.
            </p>
            <div class="input-group">
                <input type="text" class="form-control" id="busqueda" placeholder="Buscar proyecto específico"/> 
                        
            </div><!-- /input-group -->
            <s:actionmessage/>
            <s:actionerror/>
        </div>                            
        <div>    
            <table class="tabla">
                <thead>
                    <tr class="tabRow">
                        <th class="tabHeader">Nombre del Proyecto</th>
                        <th class="tabHeader">Autor</th>
                        <th class="tabHeader">Para Práctica</th>
                        <th class="tabHeader">Calificar</th>
                        <th class="tabHeader">Status</th>
                    </tr>
                </thead>
            <s:iterator value="proyectosEvaluacion">
                    <s:url action="Calificar" var="revisa">
                        <s:param name="idP" value="%{idProyecto.idProyecto}"/>
                        <s:param name="idAutor" value="%{idProyecto.autor}"/>
                        <s:param name="nombreProyecto" value="%{idProyecto.nombreProyecto}"/>
                        <s:param name="nombrePractica" value="%{nombrePractica}"/>
                    </s:url>
                    <tbody>
                        <tr class="tabRow">
                            <td class="tabCel"><s:property value="idProyecto.nombreProyecto"/></td>
                            <td class="tabCel"><s:property value="idAlumno.username"/></td>
                            <td class="tabCel"><s:property value="nombrePractica"/></td>
                            <td class="tabCel"><s:a href="%{revisa}">Revisar</s:a></td>
                            
                                <s:if test="calificacion == null">
                                    <td class="tabCel" style="color: red">SIN CALIFICAR</td>
                                </s:if>
                                <s:elseif test="calificacion == ''">
                                    <td class="tabCel" style="color: red">SIN CALIFICAR</td>
                                </s:elseif>
                                <s:else>
                                    <td class="tabCel" style="color: green">CALIFICADO: <s:property value="calificacion"/></td>
                                </s:else>
                            
                        </tr>
                    </tbody>
                
            </s:iterator>
                    
            </table>
            
            
        </div>
        <script>
            $("#busqueda").keyup(function(){
                var searchText = $(this).val().toLowerCase();
                // Show only matching TR, hide rest of them
                $.each($(".tabla tbody tr"), function() {
                    if($(this).text().toLowerCase().indexOf(searchText) === -1)
                       $(this).hide();
                    else
                       $(this).show();                
                });
            }); 
        </script>
    </body>
</html>
