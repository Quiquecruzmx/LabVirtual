<%-- 
    Document   : SubirArchivos
    Created on : 5/03/2018, 01:01:27 PM
    Author     : Enrique
--%>
<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link type="text/css" rel="stylesheet" href="css/profCss.css" >
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Subir archivos</title>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="ProfesorMain"><h4><s:property value = "#session.USER.username "/></h4></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                 <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                    </a>
                        <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                            <form action="ProyectoNuevoProfesor" id="formInicial" class="form-horizontal" method="post">
                                Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                                Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                                <s:select class="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                                <input class="btn btn-primary" type ="submit" value="CREAR">
                            </form>

                    </div>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="GruposLista">Ver grupos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="VerPracticasProfesor">Ver prácticas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nuevapractica">Crear Práctica</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ListaCalificar">Calificar proyectos</a>
                </li>
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav> 
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Material didáctico</h1>
            <br>
            <p class="lead">
                Puede subir archivos con extensión .pdf, .jpg, .png, .txt los cuales podrá asociar a prácticas como material de apoyo para 
                sus alumnos.
            </p>
            <div class="row">
                <div class="input-group">
                    <input type="text" class="form-control" id="busqueda" placeholder="Buscar material"/>         
                </div>
            </div>
            <s:actionmessage/>
            <s:actionerror/>
        </div>                         

        <table class="tabla" id="table">
            <tr class="tabRow">
                <th class="tabHeader">Archivo</th>
                <th class="tabHeader">Borrar</th>
            </tr>
            <p value="#session.lista"></p>
            <s:iterator value="lista" >
                <s:url action="BorrarMaterial" var="borrar">
                    <s:param name="id" value="%{idMaterial}"/>
                </s:url>
                <tr class="tabRow">
                    <td class="tabCel"><s:property value="nombreMaterial" /></td>
                    <td class="tabCel"><s:a href="%{borrar}" onclick="return confirm('¿Estás seguro de eliminar?')">
                            <img src="images/basura_opt.png" width="25" height="25">
                        </s:a></td>
                </tr>
            </s:iterator>               
        </table>
        <div class="col-xs-1 text-center" style="margin-top: 50px">
            <h3>Subir nuevo archivo</h3>
            <s:form action="subirArchivo" method="POST" enctype="multipart/form-data" cssClass="lead" cssStyle="margin-left:17%">
                <s:file name="archivoSubido" label="Archivo" cssClass="form-control input-sm"/>
                <s:textfield name="nombreNuevo" label="Nombre" cssClass="form-control input-sm"/>
                <s:submit value="Subir" name="submit" cssClass="btn btn-info"/>
            </s:form>
        </div>
        
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
        <script>
                    // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($("#table tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
             
             
       </script>  
    </body>
</html>
