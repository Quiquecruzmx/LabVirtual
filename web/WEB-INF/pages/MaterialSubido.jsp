<%-- 
    Document   : MaterialSubido
    Created on : 5/03/2018, 01:43:00 PM
    Author     : Enrique
--%>
<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Plantillas.Usuario" %>
<% 
    Usuario user = (Usuario) session.getAttribute("USER");
    if(user == null){
        System.out.println("No estás loggeado, redireccionando al inicio...");
        response.sendRedirect("Inicio.jsp");
    }else if(user.getRol().compareTo("profesor") != 0){
        System.out.println("¡No tiene permisos para estar aquí!");
        response.sendRedirect("Prohibido.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Nombre del archivo: <s:property value="archivoSubidoFileName"/></h1>
        <h2>Content Type: <s:property value="archivoSubidoContentType"/></h2>
        <h2>Archivo guardado en: <s:property value="ruta"/></h2>
        
    </body>
</html>
