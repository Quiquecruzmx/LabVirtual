<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link type="text/css" rel="stylesheet" href="css/profCss.css" >
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/jqueryAutocomplete.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        
        <title>Nueva práctica</title>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="ProfesorMain"><h4><s:property value = "#session.USER.username "/></h4></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="material">Material</a>
                 </li>
                 <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                    </a>
                        <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                            <form action="ProyectoNuevoProfesor" id="formInicial" class="form-horizontal" method="post">
                                Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                                Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                                <s:select class="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                                <input class="btn btn-primary" type ="submit" value="CREAR">
                            </form>

                    </div>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="GruposLista">Ver grupos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="VerPracticasProfesor">Ver prácticas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ListaCalificar">Calificar proyectos</a>
                </li>
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav> 
        <div class="jumbotron jumbotron-fluid text-center" style="height: 250px">
            <h1 class="display-4">Nueva Práctica</h1>
            <br>
            <p class="lead">
                Podrá crear prácticas para sus alumnos junto con las instrucciones y materiales necesarios para que los estudiantes
                puedan llevarla a cabo.
            </p>
            <s:actionmessage/>
            <s:actionerror/>
        </div>
        <s:form action="crearpractica" method="POST" enctype="multipart/form-data" id="formulario-id" cssClass="lead" cssStyle="margin-left: 15%">
            <s:textfield autocomplete="off" key="titulo"  name="titulo" label="Título" id="titulo" value="%{#session.tit}" cssClass="form-control input-sm"/>
            <s:textfield name="objetivo" label="Objetivo General" value="%{#session.obj}" id="objetivos" autocomplete="off" cssClass="form-control input-sm"/>
            <s:textarea name="objetivosEsp" label="Objetivos específicos" autocomplete="off" id="objsEsp" cssClass="form-control input-sm"/>
            <s:textarea name="instrucciones" label="Instrucciones" value="%{#session.ins}" cssClass="form-control input-sm"/>
            <s:checkboxlist name="listaGrupos" list="grupos"  label="Grupo(s)"/>
            <s:file name="resultado" label="Resultado" cssClass="form-control input-sm"/>
            <s:checkboxlist name="listaMateriales" list="materiales"  label="Material" />
            <s:textfield placeholder="Click para mostrar/editar rúbrica" id="despliegaForm" label="Rúbrica de evaluación" cssClass="form-control input-sm"/>
            <s:hidden name="rubrica" id="rubrica"/>
            <%--<s:textfield name="links" label="Links (opcional)" />--%>
            <s:submit value="CREAR" cssClass="btn btn-primary"/>
            
        </s:form>
        <s:div cssStyle="color:Red;"><s:actionmessage/></s:div>
        <s:div cssStyle="color:Red;"><s:actionerror/></s:div>
        
        <div id="idPopupForm" class="popupForm">
            <div class="contenedorForm">
                <div class="headerForm">
                    <span class="close">&times;</span>
                    <h2>Rúbrica de evaluación</h2>
                </div>
               <div class="cuerpoForm">
           <table class="tabla">
                <thead>
                    <tr class="tabRow2">
                    <th class="tabHeader">
                        Aspecto a evaluar
                        <input type="number" id="califUser" value="0" contenteditable="false" readonly="true">/<input type="number" id="califTotal" value="0" contenteditable="false" readonly="true">
                    </th>
                    <th class="tabHeader">
                        Nivel avanzado<br>
                        <select><option id="lvlavanzado" value="10">10</option></select>
                    </th>
                    <th class="tabHeader">
                        Nivel intermedio<br>
                        <select id="lvlinter">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5" selected="selected">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </th>
                    <th class="tabHeader">
                        Nivel Básico<br>
                        <select id="lvlbasico">
                            <option value="0" selected="selected">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar">
                            Resolución<br>
                            <select class="check">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2" selected="selected">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </td>
                        <td class="tabCel avanzado">
                            <li>Utiliza las instrucciones y algoritmos más adecuados para resolver la práctica</li>
                            <li>El funcionamiento es completo</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel inter">
                            <li>Utiliza instrucciones y algoritmos que no son acordes para resolver el ejercicio</li>
                            <li>El funcionamiento tiene fallos importantes</li>
                            <input type="hidden" class="entrada">
                            
                        </td>
                        <td class="tabCel basico">
                            <li>Utiliza algoritmos que no resuelven la práctica</li>
                            <li>La práctica no funciona</li>
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar">
                            Identificación de variables y componentes<br>
                            <select class="check">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2" selected="selected">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </td>
                        <td class="tabCel avanzado">
                            <li>Nombra correctamente todas las variables, clases y métodos</li>
                            <li>Hace uso adecuado de la herencia y polimorfismo</li>
                            <input type="hidden" class="entrada">
                            
                            
                        </td>
                        <td class="tabCel inter">	
                            <li>Nombra correctamente solo algunas variables y clases</li>
                            <li>Hace muy poco uso de la herencia y polimorfismo</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel basico">
                            <li>No nombra adecuadamente ningún componente ni variable</li>
                            <li>No hace uso de herencia y polimorfismo</li>
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar">
                            Documentación interna<br>
                            <select class="check">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2" selected="selected">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </td>
                        <td class="tabCel avanzado">
                            <li>Aporta una documentación en el código para estructurar y entender claramente</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel inter">	
                            <li>La documentación aportada es la justa para comprender el código</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel basico">
                            <li>No aporta documentación</li>
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar">
                            Presentación de resultados<br>
                            <select class="check">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2" selected="selected">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </td>
                        <td class="tabCel avanzado">
                            <li>Los resultados de la práctica son iguales a las imágenes del docente</li>
                            <li>No se muestran errores en la salida</li>
                            <input type="hidden" class="entrada">
                           
                        </td>
                        <td class="tabCel inter">	
                            <li>Los resultados de la práctica son similares a las imágenes del docente</li>
                            <li>La salida muestra algunos errores</li>
                            <input type="hidden" class="entrada">
                            
                        </td>
                        <td class="tabCel basico">
                            <li>Los resultados difieren a los del docente</li>
                            <li>La salida está llena de errores</li>
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                    <tr class="tabRow2" contenteditable="true">
                        <td class="tabCel2 remarcar" contenteditable="true">
                            Actitudes<br>
                            <select class="check">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2" selected="selected">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </td>
                        <td class="tabCel avanzado" >
                            <li>Entrega en la fecha previa establecida por el docente</li>
                            <li>Muestra perseverancia al aprovechar los errores marcados en prácticas previas para mejorar su trabajo</li>
                            <li>Organiza el código de manera legible</li>    
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel inter">	
                            <li>Entrega en la fecha establecida por el docente</li>
                            <li>Muestra perseverancia al aprovechar los errores marcados en prácticas previas para mejorar su trabajo</li>
                            <li>La organización del código es legible sólo en algunas partes</li>
                            <input type="hidden" class="entrada">
                        </td>
                        <td class="tabCel basico">
                            <li>Entrega en una fecha posterior a la establecida</li>
                            <li>El código es ilegible</li> 
                            <input type="hidden" class="entrada">
                        </td>

                    </tr>
                </tbody>
            </table>
            </div>
            </div>
        </div>
        <script type="text/javascript" src="js/rubricaPractica.js"></script>
        <script>
        var modal = document.getElementById("idPopupForm");
        var btn = document.getElementById("despliegaForm");
        var span = document.getElementsByClassName("close")[0];
        var rubrica = document.getElementsByClassName("cuerpoForm")[0];

        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            
            modal.style.display = "none";
            //alert(modal.innerHTML);
                
                var rub = document.getElementById("rubrica");
                rub.value = modal.innerHTML;
                var res = rub.value.replace(/contenteditable="true"/g, "contenteditable=\"false\"");
                rub.value = res;  
                btn.placeholder = "Rúbrica editada";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target === modal) {
                
                modal.style.display = "none";
                //alert(modal.innerHTML);
                    var rub = document.getElementById("rubrica");
                    rub.value = rubrica.innerHTML;
                    var res = rub.value.replace(/contenteditable="true"/g, "contenteditable=\"false\"");
                    rub.value = res;
                    btn.placeholder = "Rúbrica con puntaje de "+document.getElementById("califTotal").value;
                    btn.style.backgroundColor = "#007700";

            }
        }
        </script>
        <script type="text/javascript" src="js/practica.js"></script>
    </body>
</html>
