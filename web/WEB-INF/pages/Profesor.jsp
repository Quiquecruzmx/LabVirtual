<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link type="text/css" rel="stylesheet" href="css/profCss.css" >
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        
        <title>Login Profesor</title>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#"><h4><s:property value = "#session.USER.username "/></h4></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="material">Material</a>
                 </li>
                 <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                    </a>
                        <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                            <form action="ProyectoNuevoProfesor" id="formInicial" class="form-horizontal" method="post">
                                Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                                Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                                <s:select class="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                                <input class="btn btn-primary" type ="submit" value="CREAR">
                            </form>

                    </div>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="GruposLista">Ver grupos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="VerPracticasProfesor">Ver prácticas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nuevapractica">Crear Práctica</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ListaCalificar">Calificar proyectos</a>
                </li>
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav> 
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">¡Profesor <s:property value = "#session.USER.nombre"/>!</h1>
            <br>
            <p class="lead">Como profesor, usted tendrá a cargo grupos. En ellos podrá <a href="nuevapractica">enviar prácticas</a>
                para los alumnos inscritos, los cuales tendrán que resolver y enviar sus resultados que podrá
                <a href="ListaCalificar">calificar y retroalimentar</a> para enriqueces su aprendizaje.
                De igual forma, tendrá la facultad de <a href="material">subir materiales de apoyo</a> para sus prácticas.
                
            </p>
            <div class="row">
                <div class="input-group">
                    <input type="text" class="form-control" id="busqueda" placeholder="Buscar proyecto"/>         
                </div>
            </div>
            <s:actionmessage/>
            <s:actionerror/>
            
        </div>                        
        
        
        <div style="margin-top: 30px;">
            <table class="tabla" id="table">
                <thead>
                <tr class="tabRow">
                    <th class="tabHeader">Proyecto</th>
                    <th class="tabHeader">Abrir</th>
                    <!--<th class="tabHeader">Para Práctica</th>-->
                    <th class="tabHeader">Borrar</th>
                </tr>
                </thead>
                <tbody>
                <s:iterator value="#session.PROYECTOS_PROF">
                    <s:url action="AbrirProyectoProfesor" var="abrir">
                        <s:param name="idP" value="%{idProyecto}"/>
                        <s:param name="nombreProyecto" value="nombreProyecto"/>
                    </s:url>
                    <%--<s:url action="EnvioProfe" var="envioP">
                        <s:param name="idP" value="%{idProyecto}"/>
                        
                    </s:url>--%>
                    <s:url action="EliminarProyecto" var="eliminarP">
                        <s:param name="idP" value="%{idProyecto}"/>
                        
                    </s:url>
                    <tr class="tabRow">
                        <td class="tabCel"><s:property value="nombreProyecto"/></td>
                        <td class="tabCel"><s:a href="%{abrir}"><img src="images/eye-icon1.png" width="25" height="25"></s:a></td>
                        <!--<td class="tabCel">-->
                           <%-- <s:a href="#">
                                Generar
                            </s:a>--%>
                            
                        <!--</td>-->
                        <td class="tabCel"><s:a href="%{eliminarP}" onclick="return confirm('¿Eliminar este proyecto?');">
                                <img src="images/basura_opt.png" width="25" height="25">
                            </s:a></td>
                    </tr>
                </s:iterator>
            </tbody>

            </table>
        </div>
        
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
        <script>
                    // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($("#table tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
             
             
       </script>  
    </body>
</html>
