<%-- 
    Document   : Prohibido
    Created on : 1/03/2018, 01:47:33 PM
    Author     : Enrique
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>¡Prohibido!</title>
    </head>
    <body>
        <h1>No tienes acceso aquí!</h1>
        <p><a href="javascript:history.back(-1);">Regresar</a></p>
    </body>
</html>
