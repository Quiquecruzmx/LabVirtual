<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/profCss.css" type="text/css" rel="stylesheet"/>
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Envio Proyectos</title>
        
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="AlumnoMain"><s:property value = "#session.USER.username "/></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
       
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                </a>
                    <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                        <form action="ProyectoNuevoAlumno" id="formInicial" class="form-horizontal" method="post">
                            Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                            Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                            <s:select cssClass="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                            <input class="btn btn-primary" type ="submit" value="CREAR">
                        </form>

                </div>
                </li>
                <!--<li class="nav-item"><a class="nav-link" href="#">Ver grupos</a></li>-->
                <li class="nav-item"><a class="nav-link" href="verpractica">Prácticas</a></li>
                
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav>
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Lista de proyectos enviados</h1>
            <br>
            <p class="lead">
                Los proyectos que haya enviado a algun profesor para evaluación se verán reflejados aquí. Una vez que su(s)
                profesor(es) haya(n) revisado su(s) trabajo(s) podrá ver el status de su calificación y las observaciones que
                este le haya hecho.
            </p>
            <div class="row">
                
                    <div class="input-group">
                        <input type="text" class="form-control" id="busqueda" placeholder="Búsqueda"/> 
                        
                    </div><!-- /input-group -->
                
            </div><!-- /.row -->
            <s:actionmessage/>
            <s:actionerror/>
        </div>                                                
        <div>    
            <table class="tabla">
                <thead>
                    <tr class="tabRow">
                        <th class="tabHeader">Nombre del Proyecto</th>
                        <th class="tabHeader">Práctica</th>
                        <th class="tabHeader">Evaluador</th>
                        <th class="tabHeader">Detalles</th>
                        <th class="tabHeader">Status</th>
                    </tr>
                </thead>
                <s:iterator value="proyectosEnviados" status="stat">

                    <tbody>
                        <tr class="tabRow">
                            <td class="tabCel"><s:property value="idProyecto.nombreProyecto"/></td>
                            <td class="tabCel"><s:property value="nombrePractica"/></td>
                            <td class="tabCel"><s:property value="idProfesor.nombre"/> <s:property value="idProfesor.apePat"/></td>
                            <s:if test="calificacion == null">
                                    <td class="tabCel"><img src="images/no-disponible.png" width="20" height="20"></td>
                            </s:if>
                            <s:else>
                                <td class="tabCel"><s:a href="#" onclick="detalles(%{calificacion}, '%{observaciones}', '%{rubricas[#stat.index]}')"><img src="images/eye-icon1.png" width="25" height="25"></s:a></td>
                            </s:else>        
                            
                            
                                <s:if test="calificacion == null">
                                    <td class="tabCel" style="color: red">SIN CALIFICAR</td>
                                </s:if>
                                <s:elseif test="calificacion == ''">
                                    <td class="tabCel" style="color: red">SIN CALIFICAR</td>
                                </s:elseif>
                                <s:else>
                                    <td class="tabCel" style="color: green">CALIFICACION: <s:property value="calificacion"/></td>
                                </s:else>
                            
                        </tr>
                    </tbody>
                
            </s:iterator>
                    
            </table>  
        </div>
        
        
        <s:div id="idPopupForm" cssClass="popupForm">   
            <s:div cssClass="contenedorForm" >
                <s:div cssClass="headerForm">
                    <span class="close">&times;</span>
                    <h2>Detalles</h2>
                </s:div>
                <s:div cssClass="cuerpoForm" >
                    
                    <s:textfield cssClass="form-control input-sm" name="calif" label="Calificacion Asignada" readonly="true"/><br>
                    <s:textarea cssClass="form-control input-sm" name="obs" label="Observaciones del proyecto" rows="5" cols="20" readonly="true" cssStyle="resize : none"/>
                    <s:div id="rubContent"></s:div>
                    
                </s:div>

            </s:div>
        </s:div>  
        <script>                    
            function detalles(calif, obs, rub){
                var modal = document.getElementById('idPopupForm');
                modal.style.display = "block";
                var span = document.getElementsByClassName("close")[0];

                document.getElementsByName("calif")[0].value = calif;
                document.getElementsByName("obs")[0].value = obs;
                document.getElementById("rubContent").innerHTML = rub;
                span.onclick = function() {
                        modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
            }
    
                        // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($(".tabla tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
    </script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap-4.0.0.js"></script>    
    </body>
</html>
