<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/admonCss.css">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <script src="js/jquery-3.2.1.js" type="text/javascript"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Usuarios</title>
    </head>
    <body> 
        <div class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="AdminMain"><h4><s:property value = "#session.USER.username"/></h4></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                     <a class="nav-link" href="Grupos">Grupos</a>
                  </li>
               </ul>
               <form class="form-inline my-2 my-lg-0">
                  <a href="Logout" class="btn btn-outline-warning my-2 my-sm-0">Salir</a>
                  <a href="AdminMain" class="btn btn-outline-success my-2 my-sm-0">Regresar</a>
               </form>
            </div>
         </div>
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Lista de Usuarios</h1>
            <p class="lead">Listado general de usuarios registrados</p>
            <div class="row">
                
                    <div class="input-group">
                        <input type="text" class="form-control" id="busqueda" placeholder="Buscar usuario"/> 
                        
                    </div><!-- /input-group -->
                
            </div><!-- /.row -->
         </div>
        <div class="container">
        <table width="200" border="1" cellspacing="2" class="tabla" id="table">
            <thead>
            <tr class="tabRow">
                <th class="tabHeader">Usuario</th>
                <th class="tabHeader">Rol</th>
                <th class="tabHeader">Eliminar</th>
                <th class="tabHeader">Editar</th>
                
            </tr>
           </thead>
            <s:iterator value="lista" >     
                <s:url action="DeleteUser" var="borra" >
                    <s:param name="toDelete" value="%{id}"/>
                                
                </s:url>
                <s:url  action="UpdateUser" var="actualiza">

                    <s:param name="idToUpdate" value="%{id}"/>
                               
                </s:url>
                <tbody>
                <tr class="tabRow">     
                    <td class="tabCel"><h6><s:property value="username"/></h6></td>
                    <td class="tabCel"><h6><s:property value="rol"/></h6></td>
                    <td class="tabCel"><s:a href = "%{borra}" onclick="return confirm('¿Estás seguro de eliminar?')"><img src="images/basura_opt.png" width="25" height="25"></s:a></td>
                    <td class="tabCel"><s:a href="%{actualiza}"  ><img src="images/lapiz_opt.png" width="25" height="25"></s:a></td>
                   
                </tr>
                </tbody>
            </s:iterator>
            
        </table>
        </div>
        <div class="col-xs-1 text-center">
            <a href = "NuevoUsuario" class="btn btn-dark">Agregar nuevo usuario</a>
        </div>
        <script>
                    // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($("#table tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
             
             
       </script>   
    </body>
</html>

