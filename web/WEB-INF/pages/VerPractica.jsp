<%@taglib  uri="/struts-tags" prefix="s"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <script src="js/jquery-3.2.1.min.js"></script>
        <link type="text/css" rel="stylesheet" href="css/profCss.css" >
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Prácticas</title>
    </head>
    <body>
        
        <div>     
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="AlumnoMain"><s:property value = "#session.USER.username "/></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                    </a>
                    <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                        <form action="ProyectoNuevoAlumno" class="form-horizontal" method="post">
                            Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                            Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                            <s:select cssClass="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                            <input class="btn btn-primary" type ="submit" value="CREAR">
                        </form>

                    </div>
                </li>
                <!--<li class="nav-item"><a class="nav-link" href="#">Ver grupos</a></li>-->
                <li class="nav-item"><a class="nav-link" href="ListaProyectosAlumno">Detalles de proyectos</a></li>
     
                
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav>                    
        </div> 
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Prácticas para el alumno <s:property value = "#session.USER.nombre"/> <s:property value = "#session.USER.apePat"/></h1>
        <p class="lead">
            Listado de prácticas que debe realizar, aquí se desplegarán en formato .pdf así como los anexos necesarios para realizar
            la actividad.
        </p>
        <div class="row">
            <div class="input-group">
                <input type="text" class="form-control" id="busqueda" placeholder="Buscar práctica"/>         
            </div>
        </div>

        </div>  
        
        <s:actionerror/>
        <div>
            <table class="tabla" id="table">
            <thead>
                <tr class="tabRow">
                    <th class="tabHeader">Descargar Práctica</th>
                    <th class="tabHeader">Ver práctica</th>
                    <th class="tabHeader">Materiales</th>
                    <th class="tabHeader">Grupo</th>
                </tr>
            </thead>
            <tbody>
                <s:iterator value="PDF" status="stat">
                    <s:url id="fileDownload" action="descargarpractica" >
                        <s:param name="nombrePractica" value="%{titulo}"></s:param>
                    </s:url>
                    <s:url id="verPracProf" action="vistaWebPractica">
                        <s:param name="nombrePractica" value="%{idPractica}"/>
                    </s:url>
                    
                    <tr class="tabRow">
                        <td class="tabCel"><s:a href="%{fileDownload}"><s:property value="titulo"/>.pdf</s:a></td>
                        <td class="tabCel"><s:a href="%{verPracProf}"><img src="images/eye-icon1.png" width="25" height="25"></s:a></td>
                        <td class="tabCel">
                            <s:iterator value="mat[#stat.index]" var="row" status="matStatus">
                            <s:url id="materialDownload" var ="materialDownload" action="descargarmaterial">
                                <s:param name="nombreMaterial" value="nombreMaterial"></s:param>
                            </s:url>
                                <s:a href="%{materialDownload}"><s:property value="#row.nombreMaterial"/></s:a><br>
                            </s:iterator>
                        </td>
                            <s:iterator value="grupos[#stat.index]">
                                <td> <s:property value="nombreGrupo"/> </td>
                            </s:iterator>
                    </tr>
                </s:iterator>
                
            </tbody>
        </table>
         
        </div>
        <div class="col-xs-1 text-center">
            <a href="AlumnoMain" class="btn btn-outline-success my-2 my-sm-0">Regresar</a>
        </div>
        
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
                 <script>
                    // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($("#table tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
             
             
       </script> 
    </body>
</html>
