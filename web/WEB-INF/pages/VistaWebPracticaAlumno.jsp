<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <link href="css/profCss.css" type="text/css" rel="stylesheet"/>
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Practica</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="AlumnoMain"><s:property value = "#session.USER.username "/></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
       
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                </a>
                    <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                        <form action="ProyectoNuevoAlumno" id="formInicial" class="form-horizontal" method="post">
                            Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                            Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                            <s:select cssClass="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                            <input class="btn btn-primary" type ="submit" value="CREAR">
                        </form>

                </div>
                </li>
                <!--<li class="nav-item"><a class="nav-link" href="#">Ver grupos</a></li>-->
                <li class="nav-item"><a class="nav-link" href="verpractica">Prácticas</a></li>
                
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav>
        <s:iterator value="practica">
            <h1><s:property value="titulo"/></h1>
            <h4>Objetivo General:</h4>
            <s:textfield readonly="true" value="%{objetivo}" cssClass="form-control input-sm"/>
            <h4>Objetivos Especificos</h4>
            <s:textarea readonly="true" value="%{objetivosEsp}" cssClass="form-control input-sm"/>
            <h4>Instrucciones: </h4>
            <s:textarea readonly="true" value="%{instrucciones}" cssClass="form-control input-sm"/>
            
        </s:iterator>
            <h4>Ver rúbrica de evaluación</h4>
            <s:a href="#" onclick="detalles('%{rubrica}')"><img src="images/eye-icon1.png" width="50" height="50"></s:a>
            <h3>Salida:</h3>
            <img src="images/<s:property value='nombreArchivo'/>" />
            
            
            <s:div id="idPopupForm" cssClass="popupForm">   
            <s:div cssClass="contenedorForm" >
                <s:div cssClass="headerForm">
                    <span class="close">&times;</span>
                    <h2>Rúbrica de evaluación</h2>
                </s:div>
                <s:div cssClass="cuerpoForm" >
  
                    <s:div id="rubContent"></s:div>
                    
                </s:div>

                </s:div>
            </s:div>  
                    
            <script>                    
            function detalles(rub){
                var modal = document.getElementById('idPopupForm');
                modal.style.display = "block";
                var span = document.getElementsByClassName("close")[0];

                document.getElementById("rubContent").innerHTML = rub;
                span.onclick = function() {
                        modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
            }

    </script>               
    </body>
</html>
