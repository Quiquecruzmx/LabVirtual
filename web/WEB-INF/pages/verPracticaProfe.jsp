<%@taglib  uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
        <link href="css/admonCss.css" type="text/css" rel="stylesheet"/>
        <script src="js/jquery-3.2.1.min.js"></script>
        <link type="text/css" rel="stylesheet" href="css/profCss.css" >
        <link rel="icon" href="images/logo_lab_cpp.png">
        <title>Prácticas</title>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="ProfesorMain"><h4><s:property value = "#session.USER.username "/></h4></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="material">Material</a>
                 </li>
                 <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Crear Proyecto Nuevo
                    </a>
                        <div class="dropdown-menu active" aria-labelledby="navbarDropdown" >
                            <form action="ProyectoNuevoProfesor" id="formInicial" class="form-horizontal" method="post">
                                Proyecto: <input autocomplete="off" class="form-control input-sm" type="text" name="nombreProyecto" id="nombreProyecto"/>
                                Archivo Principal: <input autocomplete="off" class="form-control input-sm" type="text" name="proyectos[0].nombre"/>
                                <s:select class="form-control input-sm" name = "proyectos[0].extension" list="{'.c', '.cpp', '.h', '.hpp'}" label = "Extensión"></s:select>

                                <input class="btn btn-primary" type ="submit" value="CREAR">
                            </form>

                    </div>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="GruposLista">Ver grupos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nuevapractica">Crear Práctica</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ListaCalificar">Calificar proyectos</a>
                </li>
                
            </ul>
            <a class="btn btn-outline-warning my-2 my-sm-0 float-right" href="Logout">Salir</a>
        </div>    
        </nav>
        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Prácticas del profesor <s:property value = "#session.USER.nombre"/> <s:property value = "#session.USER.apePat"/></h1>
        <p class="lead">
            Listado de sus prácticas creadas, podrá ver a que grupo fueron enviados, descargar los pdf y puede borrarlas en caso de ya no ser necesarias.
        </p>
        <div class="row">
            <div class="input-group">
                <input type="text" class="form-control" id="busqueda" placeholder="Buscar práctica"/>         
            </div>
        </div>
        </div>                        
        <s:actionmessage/>
        <table class="tabla" id="table">
            <thead>
                <tr class="tabRow">
                    <th class="tabHeader">Título</th>
                    <th class="tabHeader">PDF</th>
                    <th class="tabHeader">Ver</th>
                    <th class="tabHeader">Grupo(s)</th>
                    <th class="tabHeader">Materiales</th>
                    <th class="tabHeader">Borrar</th>
                    <th class="tabHeader">Compartir</th>
                </tr>
            </thead>
            <tbody>
                
                    <s:iterator value="practicas" status="rel">
                        <s:url id="borrarPractica" action="BorrarPractica">
                            <s:param name="idPractica" value="idPractica"/>
                        </s:url>
                        <s:url id="practicaDownload" action="descargarpractica">
                            <s:param name="nombrePractica" value="titulo"/>
                        </s:url>
                        <s:url id="verPracProf" action="vistaWebPractica">
                            <s:param name="nombrePractica" value="%{idPractica}"/>
                        </s:url>
                        <s:url id="compartirPractica" action="CompartirPractica">
                            <s:param name="titulo" value="titulo"/>
                            <s:param name="idPractica" value="idPractica"/>
                        </s:url>
                        <tr class="tabRow">
                            <td class="tabCel"><s:property value="titulo"/></td>
                            <td class="tabCel"><s:a href="%{practicaDownload}"><s:property value="titulo"/>.pdf</s:a></td>
                            <td class="tabCel"><s:a href="%{verPracProf}"><img src="images/eye-icon1.png" width="25" height="25"></s:a></td>
                            <td class="tabCel">
                                <s:iterator value="grupos[#rel.index]" var="row" status="stat">
                                        <s:property value="#row.nombreGrupo"/>
                                </s:iterator>
                            </td>
                            <td class="tabCel">
                                <s:iterator value="materiales[#rel.index]">
                                    <s:url id="materialDownload" action="descargarmaterial">
                                        <s:param name="nombreMaterial" value="nombreMaterial"/>
                                    </s:url>
                                    <s:a href="%{materialDownload}"><s:property value="nombreMaterial"/></s:a>
                                </s:iterator>
                            </td>
                            <td class="tabCel">
                                <s:a href="%{borrarPractica}">Borrar</s:a>
                            </td>
                            <td class="tabCel">
                                <s:a href="%{compartirPractica}">Compartir</s:a>
                            </td>
                        </tr>
                    </s:iterator>
                
            </tbody>
        </table>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap-4.0.0.js"></script>
      <script>
                    // Write on keyup event of keyword input element
    $("#busqueda").keyup(function(){
        var searchText = $(this).val().toLowerCase();
        // Show only matching TR, hide rest of them
        $.each($("#table tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf(searchText) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
             
             
       </script>
    </body>
</html>
