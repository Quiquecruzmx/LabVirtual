
/* global CodeMirror */

var keywords = [];
var i=0;
$(document).ready(function(){
    
    addAutocompletado("edit0");
});

function addAutocompletado(selector, numero) {
    var cppEditor = CodeMirror.fromTextArea(document.getElementById(selector), { //Crea un editor a partir de un textarea creado previamente
        dragDrop: true,
        lineNumbers: true,
        matchBrackets: true,
        mode: "text/x-c++src"
      });
      
      cppEditor.setSize(null, 380);
    var mac = CodeMirror.keyMap.default === CodeMirror.keyMap.macDefault;
    CodeMirror.keyMap.default[(mac ? "Cmd" : "Ctrl") + "-Space"] = "autocomplete";//Habilita el autocompletado
    cppEditor.on('drop', function(data, e) {
          var file;
          var files;
          // Check if files were dropped
          files = e.dataTransfer.files;

          var reader = new FileReader();
          
          if (files.length > 0) {
            e.preventDefault();
            e.stopPropagation();
                file = files[0];
            reader.readAsText(file, "UTF-8");
            reader.onload = function (evt) {
                document.getElementById(selector).value = evt.target.result;
                var texto = document.getElementById(selector).value;
                //alert('File: '+texto );
                cppEditor.getDoc().setValue(texto);
            }	


            //return false;

          }
        });
    var idEdit = document.getElementById("proyectos["+numero+"].codigo");
    if(idEdit!==null){ //Se activa a la hora de compilar y recargar la pagina, se usa para llenar los editores con el codigo que ya habia puesto el usuario antes de recargar
        cppEditor.getDoc().setValue(idEdit.value);
        idEdit.parentNode.removeChild(idEdit);
    }
}
