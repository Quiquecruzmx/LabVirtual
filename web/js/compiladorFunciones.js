contador = 0;
    
function abrirEditor(/*evt,*/ num) {
    var i, contenido,editores, link; //la variable link no funciona aun
    contenido = document.getElementsByClassName("cont");
    editores = document.getElementsByClassName("CodeMirror");
    link = document.getElementsByClassName("tablinks");
    //alert(contenido.length+" y editores "+editores.length);
    for (i = 0; i < contenido.length; i++) {
        contenido[i].style.display = "none"; //Remueve los elementos con la clase "cont"
        //editores[i].style.display = "none"; //Remueve los elementos con la clase "cont"
    }
    for (i = 0; i < editores.length; i++) {
        //contenido[i].style.display = "none"; //Remueve los elementos con la clase "cont"
        editores[i].style.display = "none"; //Remueve los elementos con la clase "cont"
    }
    
    for (i = 0; i < link.length; i++) {
        //contenido[i].style.display = "none"; //Remueve los elementos con la clase "cont"
        link[i].style.backgroundColor = "#d6d8d9";
    }
    var nombre="nombre"+num;
    var ext="ext"+num;
    var editor = "edit"+num;
    editores[(editores.length-1) - num].style.display = "block"; //Despliega el elemento que tenga el id = "editor"
    document.getElementById(nombre).style.display = "block"; //Despliega el elemento que tenga el id = "editor"
    document.getElementById(ext).style.display = "block"; //Despliega el elemento que tenga el id = "editor"
    document.getElementById(num).style.backgroundColor = "#9fcdff";
 }

function nuevoArch(){
    var cant = document.getElementsByClassName("tablinks").length;
    if(document.getElementById("nuevoArchivo").value == null || document.getElementById("nuevoArchivo").value.toString().trim() == ""){
        alert("No puede dejar su nombre vacio");
        return false;
    }
    
    else if(!compararNombres()){
        alert("Usted ya tiene un archivo con este nombre");
    }
    else if(cant>=10){ //Verificacmos que el usuario no haya creado mas de 10 archivos
        alert("No puede generar mas de 10 archivos");
    }
    else{
        agregar(); //Si no es asi, lanzamos un nuevo nodo
    }
    
}

function agregar(){
        var editor, nombre, extension, btns, editorCodeMirror;
        
        var cant = (document.getElementsByClassName("cont").length) /3; //Obtenemos la cantidad de elementos con class="cont"   

        editor = document.getElementById("edit0");//Obtenemos el textarea con id="edit1"
        editorCodeMirror  = document.getElementsByClassName("CodeMirror");
        var clonEditor = editor.cloneNode(true);//Lo clonamos, de lo contrario, obtendremos y modificaremos el elemento original
        clonEditor.id="edit"+cant; //al elemento clonado le asignamos un nuevo id
        clonEditor.name="proyectos["+cant+"].codigo"; //al elemento clonado le asignamos un nuevo nombre
        clonEditor.value = "";
        //alert(cant+" y cm:"+editorCodeMirror.length);
        clonEditor.style.display = "none"; //Remueve los elementos con la clase "cont"
        
        
        nombre = document.getElementById("nombre0");
        var clonNombre = nombre.cloneNode(true);
        clonNombre.id="nombre"+cant;
        clonNombre.name = "proyectos["+cant+"].nombre";
        clonNombre.value = document.getElementById("nuevoArchivo").value;
        clonNombre.style.display = "none"; //Remueve los elementos con la clase "cont"

        extension=document.getElementById("ext0");
        var clonExt = extension.cloneNode(true);
        clonExt.id="ext"+cant;
        clonExt.name="proyectos["+cant+"].extension";
        //alert("Nombre ext: "+document.getElementById("nuevaExtension").value);
        clonExt.value = document.getElementById("nuevaExtension").value;
        clonExt.style.display = "none"; //Remueve los elementos con la clase "cont"

        btns = document.getElementById("tabGral"); //Obtenemos el elemento donde estan los botones
        var clonBtn = btns.firstElementChild.cloneNode(true);
        var num = document.getElementsByClassName("tablinks").length; //Obtenemos la cantidad de elementos que tienen la clase "tablinks"
        clonBtn.id = num;
        clonBtn.firstElementChild.innerHTML = clonNombre.value+clonExt.value;
        
        var itmRef= document.getElementById("edit"+(cant-1));
        var itmRef2= document.getElementById("nombre"+(cant-1));
        var itmRef3= document.getElementById("ext"+(cant-1));
        var itmRef4= document.getElementById((cant-1));

        insertAfter(clonBtn, itmRef4); //Inserta el elemento antes del "+"

        insertAfter(clonEditor, itmRef);
        insertAfter(clonNombre, itmRef2);
        insertAfter(clonExt, itmRef3);
       
        addAutocompletado("edit"+cant, cant);
       
       
       clonNombre.readonly = "false";
       clonExt.readonly = "false";
        var longitud  = editorCodeMirror.length-1;
        editorCodeMirror[longitud - cant].style.display = "none";
        rellenar(clonExt, clonNombre, clonBtn.firstElementChild, cant);
        clonNombre.readonly = "true";
       clonExt.readonly = "true";

        contador++;
        sessionStorage.setItem("cuenta", contador);
        
}


function insertAfter(nuevo, referencia) { //Funcion para insetar antes un nodo
    referencia.parentNode.insertBefore(nuevo, referencia.nextSibling);
}


function borrar(nodoBorrar) { 
    
    
    var confirma = confirm("Seguro que desea borrar este archivo?");
    if(confirma){
        if(document.getElementsByClassName("tablinks").length <=1){ //Si solo hay un archivo, no lo podremos eliminar
            alert("No se pudo borrar, al menos debe existir un archivo");
        }
        else{ //si hay mas de uno, entonces se podrá proceder su elimniación

            var numeroEditores = document.getElementsByClassName("CodeMirror").length;
            var numAux = parseInt(nodoBorrar);
            var botonBorrar = document.getElementById(nodoBorrar); //Obtenemos el id del boton para eliminar
            var editorBorrar = document.getElementById("edit"+nodoBorrar); //id del editor a eliminar
            var editorCMBorrar = document.getElementsByClassName("CodeMirror")[(numeroEditores-1) - nodoBorrar]; //id del editor a eliminar
            var extBorrar = document.getElementById("ext"+nodoBorrar); //id del tipo de extension a eliminar
            var nombreBorrar = document.getElementById("nombre"+nodoBorrar); //id del nombre a eliminar
            var botonAux = botonBorrar, editAux = editorBorrar, extAux = extBorrar, nombreAux = nombreBorrar, editorCMAux = editorCMBorrar; //auxiliares que nos ayudaran a cambiar los valores de los otros nodos
            
//            
            while(nombreAux.nextSibling !== null){//Se verifica si el editor a eliminar tiene nodos consecuentes, para poder cambiar sus valores
                
                botonAux.nextSibling.id = numAux; //el id del siguiente nodo ahora tendra el id del nodo a eliminar
                
                editAux.nextSibling.id = "edit"+numAux; //el id del siguiente editor ahora tendra el id del editor a eliminar
                editAux.nextSibling.name = "proyectos["+numAux+"].codigo"; //el nombre del siguiente editor ahora tendra el nombre del editor a eliminar
                
                extAux.nextSibling.id = "ext"+numAux;//el id de la siguiente extension ahora tendra el id de la ext a eliminar
                extAux.nextSibling.name = "proyectos["+numAux+"].extension";//igual su nombre
                
                nombreAux.nextSibling.id = "nombre"+numAux;//el id del siguiente nombre ahora tendra el id del nombre a eliminar
                nombreAux.nextSibling.name = "proyectos["+numAux+"].nombre";//el nombre del atibuto "nombre" igual

                botonAux = botonAux.nextSibling; //Pasamos ahora los valores al siguiente nodo para continuar las demás iteraciones
                editAux = editAux.nextSibling;
                extAux = extAux.nextSibling;
                
                nombreAux = nombreAux.nextSibling;

               
                editorCMAux = editorCMAux.previousSibling; //Dado que estos se anexan de forma inversa, tenemos que recorrerlos de abajo hacia arriba

                numAux++;
            }

            //Eliminamos
            botonBorrar.parentNode.removeChild(botonBorrar);
            editorBorrar.parentNode.removeChild(editorBorrar);
            editorCMBorrar.parentNode.removeChild(editorCMBorrar);
            nombreBorrar.parentNode.removeChild(nombreBorrar);
            extBorrar.parentNode.removeChild(extBorrar);
            //Guardamos el valor de archivos en el sessionStorage
            contador--;
            sessionStorage.setItem("cuenta", contador);
            abrirEditor("0");
        }
    
    }
}

function rec(){ //Sirve para recuperar el numero de elementos en el navegador en caso de que se refresque la pagina
    var tam = document.getElementById("tam").value;
    if(tam !== null){
        console.log("tamaño es: " + tam);
        sessionStorage.setItem("cuenta", tam);
    }else{
        console.log("Proyectos vacios");
        tam = sessionStorage.getItem("cuenta");//Se obtiene el valor que está en sessionStorage
        
    }
    for(var i = 0; i<tam-1; i++){
            agregar();
    }
    abrirEditor("0");
}

function rellenar(extension, nombre, nombreBoton, idRellenar){//
        

    var idExt = document.getElementById("proyectos["+idRellenar+"].extension");
    var idNombre = document.getElementById("proyectos["+idRellenar+"].nombre");
    
    if(idExt!==null){
        
        
        extension.value = idExt.value;
        nombre.value = idNombre.value;
        nombreBoton.innerHTML = idNombre.value+idExt.value;
        idExt.parentNode.removeChild(idExt);
        idNombre.parentNode.removeChild(idNombre);
        //var textArea = document.getElementById('myScript')
    }
    
}

function compararNombres(){
    var cant = (document.getElementsByClassName("cont").length) /3;
    var i;
    var nombre = document.getElementById("nuevoArchivo").value + document.getElementById("nuevaExtension").value;
    for( i = 0; i<cant; i++){
        if(nombre === document.getElementById("nombre"+i).value + document.getElementById("ext"+i).value){
            return false;
            break;
        }
    }
    return true;
}

