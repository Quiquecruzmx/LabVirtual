/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
window.onload = function(){

    var formulario = document.getElementById("formulario-id");
    formulario.addEventListener("submit", function validar(evt){
        evt.preventDefault();
        var valor = document.getElementById("titulo").value;
        var ch = document.getElementsByName("listaGrupos");
        var ref = true, ref2=true;
        for(i=0;i<ch.length;i++){
            if(ch[i].checked){
                ref2=false;
            }
        }
        if(ref2){
            alert("Seleccione al menos un grupo");
            ref=false;
        }
        if(valor == ""){
            alert("ingrese el titulo");
            ref = false;
        }
        if(ref){
            this.submit();
        }
    });


    
}

var verbosObjetivos = [];
var verbosObjsEsp = [];
var i=0;
$(document).ready(function(){
    
    $.ajax({
        url: 'palabrasObjetivos.xml',
        type: 'POST',
        dataType: 'xml',
        success: function(returnedXMLResponse){
                $('verbo', returnedXMLResponse).each(function(){
                        var kw = $(this).text();//.attr("src");
                        
                        verbosObjetivos.push(kw);
                })
        }  // Fin Success
    }); // Fin AJAX
    
    $.ajax({
        url: 'palabrasObjsEspecificos.xml',
        type: 'POST',
        dataType: 'xml',
        success: function(returnedXMLResponse){
                $('verbo', returnedXMLResponse).each(function(){
                        var kw = $(this).text();//.attr("src");
                        
                        verbosObjsEsp.push(kw);
                })
        }  // Fin Success
    }); // Fin AJAX
    
    addAutocompletado("#objetivos", verbosObjetivos);
    addAutocompletado("#objsEsp", verbosObjsEsp);
})

function addAutocompletado(selector, lista) {
    $(selector).each(function(){
        $(this).textcomplete([

            { //Palabras reservadas

                words: lista,
                match: /\b(\w{1,})$/,
                search: function (term, callback) {
                    callback($.map(this.words, function (word) {
                        return word.toLowerCase().indexOf(term.toLowerCase()) === 0 ? word : null;
                    }));
                },
                index: 1,
                replace: function (word) {
                    /*item.label*/
                    return word;
                }
            }

        ], {
            onKeydown: function (e, commands) {
                if (e.ctrlKey && e.keyCode === 74) { // CTRL-J
                    return commands.KEY_ENTER;
                }
            }
        });

    })
}

