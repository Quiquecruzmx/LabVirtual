    var sum = 0; //Suma para obtener la calificacion total
    var sumAlumno = 0;
    var porcentaje;
    
    
    $(".tabCel2").children('.check').each(function(){
        sum += parseFloat($(this).val());
    });
    $("#califTotal").val(sum);
    sum=0;
    
    $(document).on("change", ".tabCel2", function() {
        sum = 0;
        $(".tabCel2").children('.check').each(function(){
            //$(this).attr("selected","selected");
            sum += parseFloat($(this).val());
        });
        //alert(sum+"SUMA");
        $("#califTotal").val(sum);

    });
    
    $(document).on("change", ".tabHeader select", function() {
        //alert($(this).val());
        if($(this).is("#lvlinter")){
            $(this).children().each(function(){
                $(this).removeAttr('selected');
            });
            
            $(this).children("option[value='"+$(this).val()+"']").attr("selected","selected");
        }
        
        if($(this).is("#lvlbasico")){
            $(this).children().each(function(){
                $(this).removeAttr('selected');
            });
            
            $(this).children("option[value='"+$(this).val()+"']").attr("selected","selected");
        }

    });
    
    $(document).on("change", ".tabCel2 .check", function() {
        
        $(this).children().each(function(){
            $(this).removeAttr('selected');
        });
        
     
        $(this).children("option[value='"+$(this).val()+"']").attr("selected","selected");

    });
    
    $('.tabCel').click(function() {
        $(this).parent().find('.tabCel').css('background-color', '#ffffff');
        $(this).css('background-color', '#ffe8a1');
        
        $(this).parent().children().children('.entrada').each(function(){
            $(this).val(0);
            //alert($(this).val());
        });
        
        
        
        
        if($(this).hasClass('avanzado')){ //Si se clickea en una celda con clase = avanzado, se toma ese porcentaje como referencia
            porcentaje = (parseInt($('#lvlavanzado').val()) * parseInt($(this).parent().children('.remarcar').children('.check').val())) / 10;
            
            $(this).children('.entrada').val(porcentaje);
        }
        
        else if($(this).hasClass('inter')){ //Si se clickea en una celda con clase = inter, se toma ese porcentaje como referencia
            porcentaje = (parseInt($('#lvlinter').val()) * parseInt($(this).parent().children('.remarcar').children('.check').val())) / 10;
            $(this).val(porcentaje);
            $(this).children('.entrada').val(porcentaje);
        }
        
        else{
            porcentaje = (parseInt($('#lvlbasico').val()) * parseInt($(this).parent().children('.remarcar').children('.check').val())) / 10;
            $(this).val(porcentaje); //Si se clickea en una celda con clase = basico, se toma ese porcentaje como referencia
            $(this).children('.entrada').val(porcentaje);
            //sumAlumno += parseFloat($(this).val());
        }
        
        $(this).parent().parent().children().children().children('.entrada').each(function(){
            
            if(!isNaN(parseFloat($(this).val()))){
            
                sumAlumno+=parseFloat($(this).val());
                //alert("Suma acum:"+ sumAlumno);
            }
            //alert("final: "+$(this).val());
        });
        
        
        $("#califUser").val(sumAlumno);
        
        sumAlumno = 0;
      });

